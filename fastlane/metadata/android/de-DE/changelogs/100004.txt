Version 0.40:

Neue Funktionen:
- Bulk-Komprimierung und Piping ist jetzt möglich
- Unterstützung für Multitouch-Gesten beim Zuschneiden des Bildes hinzugefügt

Fehlerbehebungen:
- Auflösung des App-Icons war zu niedrig
- App-Absturz beim Drehen des Geräts mit aktivierter JPEG-Qualitätsvorschau behoben
- Der Check-Button neben der Bildgröße wurde entfernt
- die Freigabe funktionierte nicht, wenn das Piping mit dem Zurück-Button abgebrochen wurde
