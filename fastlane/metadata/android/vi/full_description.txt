Ứng dụng này giảm kích cỡ hình ảnh bằng cách thay đổi độ phân giải và chất lượng hình ảnh. Đồng thời loại bỏ dữ liệu exif trước khi chia sẻ hình ảnh. Hình ảnh đã được chỉnh sửa được lưu trữ trong một thư mục riêng biệt dưới định dạng jpeg. Hình ảnh gốc vẫn được giữ nguyên.

Imagepipe sẽ nhận hình ảnh, chỉnh sửa nó và gửi hình ảnh đi. Do đó, bạn chỉ cần một lần chạm để chuyển đổi hình ảnh trước khi gửi!

Hình ảnh cũng có thể được chỉnh sửa với Imagepipe.
