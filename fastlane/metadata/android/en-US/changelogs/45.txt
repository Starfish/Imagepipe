- when loading an image via Imagepipe, the image can now be edited beforehand
- new options for setting the image size
- improved translations
- long pressing "reload" now loads the image in higher resolution
- fixes an error if an image could be successfully loaded but not decoded
- fixes a bug that caused degraded image quality