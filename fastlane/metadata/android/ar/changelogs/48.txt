- يمكن حذف بيانات الصورة الموجودة في التطبيق بعد مشاركتها
- تدوير جديد للصورة بأي زاوية
- يتم الآن ترقيم الصور باستخدام عداد داخلي
- تم الآن إلغاء تنشيط الحفظ التلقائي للصور افتراضيًا في عمليات التثبيت الجديدة
- تحسينات أخرى مختلفة
- تحسين الترجمات
