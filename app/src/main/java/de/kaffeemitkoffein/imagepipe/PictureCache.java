/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

public class PictureCache {

    final static String FILELIST_FILE = "picture_cache.txt";
    final static String CACHEFILESKELETON = "picturecache";

    /*
     * all the stuff here ist static
     */

    private PictureCache() {
    }

    /*
     * Imagepipe picture cache logic:
     *
     * cache is invalidated:
     *     - new image received (done)
     *     - image loaded (done)
     *     - image saved to disk (done)
     *     - image shared (not needed)
     */

    private static String getNewTempFileName(Context context){
        //String result = ImageWriter.determineUnusedFilename(context,null,context.getCacheDir(),false);
        int i = -1;
        File file;
        String newCacheFileName;
        File cachePathFile = context.getCacheDir();
        do {
            i++;
            newCacheFileName = CACHEFILESKELETON+"_"+i+"."+ImagepipePreferences.getCompressFormatFileExtension(context);
            file = new File(cachePathFile,newCacheFileName);
        } while (file.exists());
        return newCacheFileName;
    }

    private static File getNewPictureTempFile(Context context){
        String newFilename = getNewTempFileName(context);
        return new File(context.getCacheDir(),newFilename);
    }

    private static File getPictureTempFile(Context context, int position){
        ArrayList<String> fileList = readFileNameList(context);
        if (position<fileList.size()){
            return new File(context.getCacheDir(),fileList.get(position));
        }
        return null;
    }

    public static String getPictureTempFileName(Context context, int position){
        File file = getPictureTempFile(context,position);
        if (file!=null){
            return file.getName();
        }
        return null;
    }

    private static File getFileNameListFile(Context context){
        return new File(context.getCacheDir(),FILELIST_FILE);
    }

    private static ArrayList<String> readFileNameList(Context context){
        ArrayList<String> fileList = new ArrayList<String>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(getFileNameListFile(context))));
            String line;
            while ((line = bufferedReader.readLine()) != null){
                fileList.add(line);
            }
            bufferedReader.close();
        } catch (Exception e) {
            // do nothing
        }
        return fileList;
    }

    private static int writeFileNameList(Context context, ArrayList<String> fileList, boolean append){
        if (fileList!=null){
            try {
                File file = getFileNameListFile(context);
                if (!file.exists()){
                    append = false;
                }
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(getFileNameListFile(context),append));
                int count = 0;
                for (int i=0; i<fileList.size(); i++){
                    bufferedWriter.write(fileList.get(i));
                    bufferedWriter.newLine();
                    count++;
                }
                bufferedWriter.close();
                return count;
            } catch (Exception e){
                // do nothing
            }
        }
        return -1;
    }

    public static int addToFileNameList(Context context, File file){
        ArrayList<String> fileList = new ArrayList<String>();
        fileList.add(file.getName());
        return writeFileNameList(context,fileList,true);
    }

    public static void recreateFileNameList(Context context){
        ArrayList<String> oldFileList = readFileNameList(context);
        ArrayList<String> newFileList = new ArrayList<String>();
        for (int i=0; i<oldFileList.size(); i++){
            File oldFile = getPictureTempFile(context,i);
            File newFile;
            do {
                newFile = getNewPictureTempFile(context);
                newFileList.add(newFile.getName());
            } while (!Objects.requireNonNull(oldFile).renameTo(newFile));
        }
        writeFileNameList(context,newFileList,false);
    }

    public static void clearPictureCache(Context context, int position){
        ArrayList<String> fileList = readFileNameList(context);
        ArrayList<String> newFileList = new ArrayList<String>();
        // copy valid entires
        for (int i=0; ((i<position) && (i<fileList.size())); i++){
            newFileList.add(fileList.get(i));
        }
        // delete images not needed any more above position
        for (int i=position; i<fileList.size(); i++){
            File f = getPictureTempFile(context,i);
            boolean result = f.delete();
        }
        // update file list
        int count = writeFileNameList(context,newFileList,false);
    }

    public static boolean checkCacheIntegrity(Context context){
        ArrayList<String> fileNameList = readFileNameList(context);
        for (int i=0; i<fileNameList.size(); i++){
            File file = getPictureTempFile(context,i);
            if (file!=null){
                if (file.exists()){
                    // do nothing
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    public static void deleteRecursively(File file){
        if (file!=null){
            if (file.isDirectory()){
                File[] files = file.listFiles();
                for (int i=0; i<files.length; i++){
                        deleteRecursively(files[i]);
                }
            }
            if (file.delete()){
                // nothing to do
            }
        }
    }

    public static void deleteFilesRecursively(File file){
        if (file!=null){
            if (file.isDirectory()){
                File[] files = file.listFiles();
                for (int i=0; i<files.length; i++){
                    deleteRecursively(files[i]);
                }
            } else
            if (file.delete()){
                // nothing to do
            }
        }
    }

    public static void clearPictureCache(Context context){
        try {
            deleteRecursively(context.getCacheDir());
        } catch (Exception e){
            // do nothing, because cache likely is not available.
        }
    }

    public static void clearCacheNotShare(Context context){
        try {
            File cacheDir = context.getCacheDir();
            File[] files = cacheDir.listFiles();
            if (files!=null){
                for (int i=0; i<files.length; i++){
                    File file = files[i];
                    if (!file.isDirectory()){
                        file.delete();
                    }
                }
            }
        } catch (Exception e){
            // do nothing, because cache likely is not available.
        }
    }

    public static int saveToPictureCache(final Context context, final Bitmap bitmap, final boolean applyQualityLoss, final Runnable runnable){
        final int position = getPictureCacheSize(context);
        final File file = getNewPictureTempFile(context);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    int quality = 100;
                    if (applyQualityLoss){
                        quality = ImagepipePreferences.getQuality(context);
                    }
                    bitmap.compress(ImagepipePreferences.getCompressFormat(context,quality),quality,out);
                    out.flush();
                    out.close();
                    addToFileNameList(context,file);
                    if (runnable!=null){
                        runnable.run();
                    }
                } catch (Exception e){
                    // do nothing
                }
            }
        }).start();
        return position;
    }

    public static Bitmap restoreFromPictureCache(Context context, int position){
        File file = getPictureTempFile(context,position);
        if (file!=null){
            if (file.exists()){
                try {
                    FileInputStream inputstream = new FileInputStream(file);
                    Bitmap bitmapImmutable = BitmapFactory.decodeStream(inputstream);
                    inputstream.close();
                    return bitmapImmutable.copy(Bitmap.Config.ARGB_8888,true);
                } catch (Exception e){
                    return null;
                }
            }
        }
        return null;
    }

    public static int getPictureCacheSize(Context context){
        ArrayList<String> fileList = readFileNameList(context);
        return fileList.size();
    }

    public static int getLastUsedCachePosition(Context context){
        int cacheSize = getPictureCacheSize(context);
        long lastTime = 0;
        int resultPosition = 0;
        for (int i=0; i<cacheSize; i++){
            File file = getPictureTempFile(context,i);
            long time = file.lastModified();
            if (time>lastTime){
                lastTime = time;
                resultPosition = i;
            }
        }
        return resultPosition;
    }

    public static boolean touchPicture(Context context, int position){
        File file = getPictureTempFile(context,position);
        if (file!=null){
            if (file.exists()){
                return file.setLastModified(Calendar.getInstance().getTimeInMillis());
            }
        }
        return false;
    }

    public static long getCachedFileSize(Context context, int position){
        File file = getPictureTempFile(context, position);
        try {
            return file.length();
        } catch (NullPointerException e){
            return 0;
        }
    }

    public static Uri getFileUri(Context context, int position){
        File outfile = getPictureTempFile(context,position);
        Uri uri = FileContentProvider.getUriForFile(ImageReceiver.AUTHORITY,FileContentProvider.TAG_CACHE,outfile,null);
        return uri;
    }

    public static Uri getLastFileUri(Context context) {
        return getFileUri(context, getLastUsedCachePosition(context));
    }

}
