/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import java.io.File;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ImageWriter {

    // this is the target uri
    final static Uri mediaStoreTargetUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

    final static char[] FILENAMECHARS = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
    final static int RANDOMFILELENGTH = 16;
    final static String SHARE_TEMP_DIR = "share/";

    public static String getRandomFileNameSkeleton(){
        StringBuilder stringBuilder = new StringBuilder();
        SecureRandom secureRandom = new SecureRandom();
        for (int i=0; i<RANDOMFILELENGTH; i++){
            int r = secureRandom.nextInt(FILENAMECHARS.length);
            stringBuilder.append(FILENAMECHARS[r]);
        }
        return stringBuilder.toString();
    }

    private static String generateFileNameSkeleton(Context context, ImageContainer originalImageContainer, long i){
        // ImagepipePreferences pref = new ImagepipePreferences(context);
        // skeleton from the settings is default
        String skeleton  = ImagepipePreferences.getSkeletonFilename(context);
        String numbering = ImagepipePreferences.getNumbering(context);
        // take filename if desired and possible. We cannot assume a known filename, e.g. if image is painted
        // from scratch.
        if ((numbering.equals("5")) || (numbering.equals("6")) || (numbering.equals("7")) || (numbering.equals("8"))){
            if (originalImageContainer!=null){
                if (originalImageContainer.filename!=null){
                    skeleton=originalImageContainer.getFileNameSkeleton();
                } else {
                    skeleton=ImagepipePreferences.getOriginalName(context);
                }
            } else {
                // originalImageContainer is unknown/null. Therefore, we need
                // to get it from preferences
                skeleton=ImagepipePreferences.getOriginalName(context);
            }
        }
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
        if (numbering.equals("2") || numbering.equals("6")){
            return String.valueOf(i) + "_" + skeleton;
        }
        if (numbering.equals("3") || numbering.equals("7")){
            return date.format(calendar.getTime()) + "_" + skeleton + "_" + String.valueOf(i);
        }
        if (numbering.equals("4") || numbering.equals("8")){
            return String.valueOf(i) + "_" + skeleton + "_" + date.format(calendar.getTime());
        }
        if (numbering.equals("9")){
            return getRandomFileNameSkeleton(); // this ignores the parameter i.
        }
        // option "1" & "5"
        return skeleton + "_" + String.valueOf(i);
    }

    public static String determineUnusedFilename(Context context, ImageContainer originalImageContainer, File path){
        long i = ImagepipePreferences.getFileNameCounter(context);
        String fn;
        File check_file;
        do {
            fn = generateFileNameSkeleton(context,originalImageContainer,i) + "."+ImagepipePreferences.getCompressFormatFileExtension(context);
            check_file = new File (path,fn);
            i = i + 1;
            if (check_file.exists()){
                // do nothing
            }
        } while (check_file.exists());
        // do not increment if file names are random (option 9).
        if (!ImagepipePreferences.getNumbering(context).equals("9")){
            // increment here for legacy devices where the chooser has no callback
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                ImagepipePreferences.setFileNameCounter(context,i);
            } else {
                // write new value to cache; this will be called and written to the file counter
                // once a target app was chosen. See IntentReceiver.
                ImagepipePreferences.setFileCounterCache(context,i);
            }
        }
        return fn;
    }

    // basically deprecated, only used for versions prior to scoped storage

    public static ImageContainer toFile(Context context, ImageContainer originalImageContainer){
        // File system_picure_directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        // String target_directory_string = system_picure_directory.getAbsolutePath()+File.separator+context.getResources().getString(R.string.app_folder);
        String target_directory_string = ImagepipePreferences.getPath(context);
        File target_directory = new File(target_directory_string);
        target_directory.mkdirs();
        String targetname = determineUnusedFilename(context,originalImageContainer,target_directory);
        File file = new File(target_directory,targetname);
        Uri uri = Uri.fromFile(file);
        ImageContainer imageContainer = new ImageContainer();
        imageContainer.uri = uri;
        imageContainer.file = file;
        imageContainer.filename = targetname;
        imageContainer.filesize = 0; // not known yet
        return imageContainer;
    }

    /**
     Determines unused description name using the MediaStore.
     @return the unused name, or null if the underlying contentProvider crashed and/or returned null.
     */

    private static String determineUnusedStorename(Context context, ImageContainer originalImageContainer, boolean incrementCounter){
        long i = ImagepipePreferences.getFileNameCounter(context);
        String desiredName;
        ContentResolver contentResolver = context.getContentResolver();
        String[] projection = {MediaStore.MediaColumns.DISPLAY_NAME};
        String selection = MediaStore.MediaColumns.DISPLAY_NAME+"=?";
        Cursor cursor;
        do {
            desiredName = generateFileNameSkeleton(context,originalImageContainer,i) + "."+ImagepipePreferences.getCompressFormatFileExtension(context);
            String[] selectionArgs={desiredName};
            cursor = contentResolver.query(mediaStoreTargetUri,projection,selection,selectionArgs,null);
            i ++;
            if (cursor==null){
                break;
            }
        } while (cursor.moveToFirst());
        if (cursor!=null){
            cursor.close();
            // save the position to the settings for re-use
            if (incrementCounter){
                // do not increment if file names are random (option 9).
                if (!ImagepipePreferences.getNumbering(context).equals("9")){
                    // increment here for legacy devices where the chooser has no callback
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                        ImagepipePreferences.setFileNameCounter(context,i);
                    }
                }
            }
            return desiredName;
        }
        return null;
    }

    /**
     * Saves a bitmap to uri.
     *
     * @param context the context
     * @param targetUri the target uri to save to. May be a file or a mediastore uri.
     * @param sourceBitmap the source bitmap. Will not be modified.
     * @param compression_rate the compression rate.
     * @param exifData optional exi data of the original image. May be null.
     *
     * @return true if saved succeeded, otherwise false.
     */

    public static boolean saveBitmapToUri(final Context context, final Bitmap sourceBitmap, final Uri targetUri, final int compression_rate, final ExifData exifData){
        if ((targetUri==null)|| (sourceBitmap==null)){
            return false;
        }
        try {
            Log.v("Writing bitmap tu uri: "+targetUri.toString());
            OutputStream outputStream = context.getContentResolver().openOutputStream(targetUri);
            Bitmap bitmap_output = sourceBitmap.copy(Bitmap.Config.ARGB_8888,true);
            bitmap_output.compress(ImagepipePreferences.getCompressFormat(context,compression_rate), compression_rate, outputStream);
            outputStream.flush();
            outputStream.close();
            Log.v("written Bitmap to uri: "+targetUri.toString());
            if (ImagepipePreferences.keepSomeExifTags(context)){
                ImageWriter.addExifDataToUri(context,targetUri,exifData);
            } else {
                Log.v("No exif tags allowed.");
            }
        } catch (Exception e) {
            Log.v("ERROR saving bitmap to uri: "+e.getMessage());
            return false;
        }
        return true;
    }

    private static Uri getExternalSingleShareFileUri(Context context, File file, String displayName){
       //Uri uri = FileContentProvider.getUriForFile(ImageReceiver.AUTHORITY,FileContentProvider.TAG_CACHE+"/"+SHARE_TEMP_DIR,file,PictureCache.getPictureTempFileName(context,PictureCache.getLastUsedCachePosition(context)));
       Uri uri = FileContentProvider.getUriForFile(ImageReceiver.AUTHORITY,FileContentProvider.TAG_CACHE+"/"+SHARE_TEMP_DIR,file,displayName);
       return uri;
    }

    public static File getShareTempDir(Context context){
        File targetDirectory = new File(context.getCacheDir(),SHARE_TEMP_DIR);
        if (targetDirectory.mkdirs()) {
            // do nothing
        }
        return targetDirectory;
    }

    public static void clearShareTempDir(Context context){
        File targetDirectory = getShareTempDir(context);
        PictureCache.deleteFilesRecursively(targetDirectory);
        if (targetDirectory.mkdirs()){
            // do nothing
        }
    }

    /**
     * Copies a file from cache to the temporary share folder and adds allowed exif tags to the target.
     * Returns the new uri of the file to share.
     */

    public static Uri saveShareTempFileFromLastCachePosition(Context context,ImageContainer imageContainer, ExifData exifData){
        return saveShareTempFileFromCache(context,imageContainer,exifData,PictureCache.getLastUsedCachePosition(context));
    }

    public static Uri saveShareTempFileFromCache(Context context,ImageContainer imageContainer, ExifData exifData, int cachePosition){
        Bitmap image = PictureCache.restoreFromPictureCache(context,cachePosition);
        File targetDirectory = getShareTempDir(context);
        String targetFilename = determineUnusedFilename(context,imageContainer,targetDirectory);
        File targetFile = new File(targetDirectory,targetFilename);
        Uri targetUri =  Uri.fromFile(targetFile);
        if (saveBitmapToUri(context,image,targetUri,ImagepipePreferences.getQuality(context),exifData)){
            Uri uri = getExternalSingleShareFileUri(context,targetFile,targetFilename);
            Log.v("saveShareTempFileFromCache is: "+uri.toString());
            return uri;
        } else {
            Log.v("saveShareTempFileFromCache FAILED for: "+targetFilename);
        }
        return null;
    }

    public static String getMimeType(Context context){
        String compressFormat = ImagepipePreferences.getImageFileFormat(context);
        String mimeType= "image/png";
        if (compressFormat.equals(ImagepipePreferences.ImageFileFormat.JPEG)){
            mimeType="image/jpeg";
        }
        if (compressFormat.equals(ImagepipePreferences.ImageFileFormat.WEBP)){
            mimeType="image/webp";
        }
        if (compressFormat.equals(ImagepipePreferences.ImageFileFormat.PNG)){
            mimeType="image/png";
        }
        return mimeType;
    }

    public static ImageContainer toMediaStore(Context context, ImageContainer originalImageContainer){
        String filePath = Environment.DIRECTORY_PICTURES+ File.separator+ImagepipePreferences.getPath(context);
        String desiredName = determineUnusedStorename(context,originalImageContainer,true);
        Log.v("Adding saved image to the mediastore -> target path: "+filePath+", desired name: "+desiredName);
        final ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, desiredName);
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE,getMimeType(context));
        // means use of scoped storage
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.Q) {
            contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, filePath);
        }
        // insert the metadata
        Uri uri = context.getContentResolver().insert(mediaStoreTargetUri,contentValues);
        ImageContainer imageContainer = new ImageContainer();
        imageContainer.uri = uri;
        imageContainer.file = null; // we do not have a file here
        imageContainer.filename = desiredName;
        imageContainer.filesize = -1; // not known yet
        return imageContainer;
    }

    /**
     * Adds an ExifInterface to a file determined by uri.
     *
     * Only "allowed" (as per user setup in the settings) exif tags are written to the uri/file.
     *
     * @param context the app context
     * @param uri the uri of the target file
     * @param exifData the exif data of the original image
     *
     * @return the ExifInterface with the tags that have been written, null if writing failed for some reason (e.g. file
     * does not exist, option has been disabled, api too low, etc.)
     *
     */

    @TargetApi(Build.VERSION_CODES.N)
    public static ExifInterface addExifDataToUri(Context context, Uri uri, ExifData exifData) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            if (exifData != null) {
                try {
                    ExifInterface targetExifInterface = new ExifInterface(context.getContentResolver().openFileDescriptor(uri, "rw").getFileDescriptor());
                    exifData.fillExifInterfaceWithData(targetExifInterface);
                    targetExifInterface.saveAttributes();
                    return targetExifInterface;
                } catch (Exception e){
                     Log.v("ERROR saving exif data: "+e.getMessage());
                }
            }
        }
        return null;
    }

    public static String getDefaultSavePath(Context context){
        // means use of scoped storage
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.Q) {
            return Environment.DIRECTORY_PICTURES+ File.separator+context.getResources().getString(R.string.app_folder);
        } else {
            File system_picure_directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            return system_picure_directory.getAbsolutePath()+File.separator+context.getResources().getString(R.string.app_folder);
        }
    }

}
