/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

public class SeekBarPreference extends Preference implements SeekBar.OnSeekBarChangeListener {

    private final static String NAMESPACE = "http://schemas.android.com/apk/res/android";
    private Context context;
    private int value;
    private int min;
    private int max;
    private int[] colorThresholds;
    private int[] colors;
    private boolean displayProgressValue = false;

    private View preferenceView;
    private boolean changeOngoing = false;


    public SeekBarPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        readAttributes(context,attrs,defStyleAttr);
    }

    public SeekBarPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        readAttributes(context,attrs,0);
    }

    /**
     * Internal function to read the attributes from the xml definition of this preference. This is called by the
     * constructors to unify attribute interpretation.
     *
     * Attributes are:
     * min: defines the minimum value allowed. The seekBar cannot be moved to a position lower than this minimum.
     * max: defines the maximum value allowed. The seekBar will be cut-off at this value.
     * label: defines color labeling of values. The color of the value switches as soon as a threshold is reached.
     * Format is "threshold,color", the thresholds are separated by a pipe symbol (|). Example in xml:
     *      android:label="20,#ffffff66|60,#ff66ff66|90,#ffff6666"
     * The displayed value will be yellow when equal or above 20,
     *                             green when equal or above 60,
     *                             red when equal or above 90.
     * When below 20, the color won't be modified.
     *
     * To display a progress value, define at least one threshold color.
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     */

    private void readAttributes(Context context, AttributeSet attrs, int defStyleAttr){
        setMax(100);
        this.context = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs,R.styleable.SeekBarPreference,defStyleAttr,0);
        min = attrs.getAttributeIntValue(NAMESPACE,"min",0);
        max = attrs.getAttributeIntValue(NAMESPACE,"max",100);
        displayProgressValue = false;
        String progressLabel = attrs.getAttributeValue(NAMESPACE,"label");
        if (progressLabel!=null){
            displayProgressValue = true;
            String[] labelIitems = progressLabel.split("\\|");
            if (labelIitems.length>0){
                colorThresholds = new int[labelIitems.length]; colors = new int[labelIitems.length];
                for (int i=0; i<labelIitems.length; i++){
                    String values[] = labelIitems[i].split(",");
                    if (values.length>1){
                        colorThresholds[i] = Integer.parseInt(values[0]);
                        colors[i]          = Color.parseColor(values[1]);
                    }
                }
            }
        }
        typedArray.recycle();
    }

    @Override
    protected View onCreateView(ViewGroup parent) {
        setLayoutResource(R.layout.seekbarpreference);
        View view = super.onCreateView(parent);
        preferenceView = view;
        TextView textViewValue = (TextView) view.findViewById(R.id.value);
        if (!isEnabled()){
            TextView textView1 = (TextView) view.findViewById(android.R.id.title);
            textView1.setTextColor(Color.GRAY);
            TextView textView2 = (TextView) view.findViewById(android.R.id.summary);
            textView2.setTextColor(Color.GRAY);
            textViewValue.setTextColor(Color.GRAY);
        }
        if (displayProgressValue){
            textViewValue.setVisibility(View.VISIBLE);
        } else {
            textViewValue.setVisibility(View.GONE);
        }
        return view;
    }

    public void setValue(int v){
        if (v!=value) {
            if (v < min) {
                value = min;
            } else if (v > max) {
                value = max;
            } else {
                value = v;
            }
            persistInt(value);
            notifyChanged();
        }
    }

    public int getValue(){
        return value;
    }

    public void setMax(int v){
        max = v;
        notifyChanged();
    }

    private void setProgress(SeekBar seekBar){
        int p = seekBar.getProgress();
        if (callChangeListener(p)){
            setValue(p);
        } else {
            seekBar.setProgress(value);
        }
    }

    private void displayValueNumber(int i){
        if (displayProgressValue){
            TextView textViewValue = (TextView) preferenceView.findViewById(R.id.value);
            if ((colorThresholds!=null) && (colors!=null)){
                if ((colorThresholds.length>0) && (colors.length>0)) {
                    int colorPosition=-1;
                    for (int pos=0; pos<colorThresholds.length; pos++){
                        if (i>=colorThresholds[pos]){
                            colorPosition = pos;
                        }
                    }
                    if (colorPosition!=-1){
                        textViewValue.setTextColor(colors[colorPosition]);
                    }
                }
            }
            textViewValue.setText(Integer.toString(i));
        }
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        SeekBar seekBar = view.findViewById(R.id.seekbar);
        TextView textViewValue = view.findViewById(R.id.value);
        seekBar.setOnSeekBarChangeListener(this);
        seekBar.setMax(max);
        seekBar.setProgress(value);
        displayValueNumber(seekBar.getProgress());
        seekBar.setEnabled(isEnabled());
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue){
        if (restoreValue){
            if (defaultValue!=null){
                value = getPersistedInt((int) defaultValue);
            } else {
                value = getPersistedInt(0);
            }
        } else {
            value = (int) defaultValue;
        }
       setValue(value);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {
        if (fromUser && !changeOngoing){
            setProgress(seekBar);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        changeOngoing = true;
        displayValueNumber(seekBar.getProgress());
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        changeOngoing = false;
        setProgress(seekBar);
    }
}

