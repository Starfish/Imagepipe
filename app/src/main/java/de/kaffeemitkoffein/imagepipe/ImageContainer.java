/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;

import java.io.File;

/**
 * This is a tiny class to combine an image uri and a file for easier handling in lists and arrays.
 * There is no easy and reliable way to convert a file to an uri and vice versa.
 *
 * Rule: each child of this class always holds an URI and sometimes also the file and a filesize.
 */

public class ImageContainer {
    public Uri uri;
    public File file;
    public long filesize=0;
    public String filename;
    public int originalWidth=0;
    public int originalHeight=0;

    public ImageContainer(Context context, Uri u){
        setValues(context,u,true);
    }

    public ImageContainer(Context context, Uri u, long filesize){
        setValues(context,u,false);
        this.filesize = filesize;
    }

    public ImageContainer(ImageContainer ic){
        this.uri = ic.uri;
        this.file = ic.file;
        this.filesize = ic.filesize;
        this.filename = ic.filename;
    }

    public ImageContainer(){
    }

    private void setValues(Context context, Uri u, boolean determineFilesize){
        if (u!=null){
            this.uri = u;
            // try to get filename from gallery
            // this requires the READ_EXTERNAL_STORAGE permission and might fail if this has not been granted
            if (context!=null){
                try {
                    ContentResolver contentResolver = context.getContentResolver();
                    String[] projection = {OpenableColumns.DISPLAY_NAME,OpenableColumns.SIZE};
                    Cursor cursor = contentResolver.query(u, projection, null, null, null);
                    if (cursor != null) {
                        try {
                            if (cursor.moveToFirst()){
                                this.filename = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                            }
                        } catch (Exception e){
                            // do nothing
                        }
                        if (determineFilesize){
                            // try to determine file size from uri.
                            try {
                                // try to determine file size using contentResolver, this might fail when data is
                                // presented as a stream and not a file.
                                ParcelFileDescriptor parcelFileDescriptor = context.getApplicationContext().getContentResolver().openFileDescriptor(u,"r");
                                this.filesize = parcelFileDescriptor.getStatSize();
                            } catch (Exception e){
                                try {
                                    // try to determine file size using the FileProvider of the gallery app, this result
                                    // might be slightly inaccurate, is therefore the second choice
                                    this.filesize = cursor.getLong(cursor.getColumnIndex(OpenableColumns.SIZE));
                                } catch (Exception e2){
                                    // unable to determine file size by both methods
                                    this.filesize = 0;
                                }
                            }
                        }
                        cursor.close();
                    }
                } catch (Exception e){
                    // do nothing
                }
            }
        }
    }

    public void setDimensions(int x, int y){
        this.originalWidth = x;
        this.originalHeight = y;
    }

    public String getFileNameSkeleton(){
        if (filename==null){
            return null;
        } else {
            String result;
            String seperator = System.getProperty("file.separator");
            int separatorIndex = filename.lastIndexOf(seperator);
            // remove any path before the filename
            if (separatorIndex==-1){
                // file name has no path
                result = filename;
            } else {
                try {
                    result = filename.substring(separatorIndex+1);
                } catch (IndexOutOfBoundsException e){
                    // filename is actually a path only, so we take an empty string
                    result = "";
                }
            }
            // remove extension
            int extIndex = result.lastIndexOf(".");
            if (extIndex==-1){
                return result;
            } else {
                return result.substring(0,extIndex);
            }
        }
    }

}
