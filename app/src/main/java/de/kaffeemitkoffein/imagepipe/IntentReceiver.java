/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

public class IntentReceiver extends BroadcastReceiver {

    public final static String IMAGE_COUNT_EXTRA = "IMAGECOUNT";
    private final static int IMAGE_COUNT_DEFAULT = 1;
    // empiric: 500 ms delay sufficient for 1 image, 700 ms sufficient for 10 images
    private static long BASEDELAY_MILLIS = 1500L; // empiric testing shows that 500ms delay are sufficient for a single image
    private static long DELAY_PER_IMAGE_MILLIS = 30L; // empiric testing shows that 20ms delay per image seem sufficient

    @Override
    public void onReceive(Context context, Intent intent) {
        int count = intent.getIntExtra(IMAGE_COUNT_EXTRA,IMAGE_COUNT_DEFAULT);
        long delay = BASEDELAY_MILLIS + count * DELAY_PER_IMAGE_MILLIS;
        Log.v("image count: "+count+", clearing data is delayed by "+delay+" ms to allow target app to handle data.");
        if (ImagepipePreferences.clearAfterShare(context)){
            final Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    clearPrivateData(context);
                    Log.v("Cleared private data after sharing.");
                    // this receiver should never be called on android sdk below 22, but we check anyway
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
                        // set the cache value to the file name counter once at least a target component was chosen
                        ImagepipePreferences.setFileNameCounter(context,ImagepipePreferences.getFileCounterCache(context));
                    }
                }
            },delay);
        }
    }

    private static void clearPrivateData(Context context){
        // clear image cache
        PictureCache.clearPictureCache(context);
        // clear cache from image rotation
        ImageReceiver.deleteCacheFile(context);
        // clear image origin data
        ImagepipePreferences.resetSavedImageContainer(context);
        // clear exif container
        ImagepipePreferences.resetExifData(context);
        // notify main app to remove volatile data
        Intent mainAppCall = new Intent(ImageReceiver.ACTION_CLEAR_DATA);
        context.sendBroadcast(mainAppCall);
    }

}
