/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ColorPicker {

    public interface OnColorPickedListener{
        void onColorSelected(int color);
        void onNoColorSelected();
    }
    public final static int FLAG_NONE = 0;
    public final static int FLAG_TRANSPARENT = 1;

    private OnColorPickedListener onColorPickedListener;
    private final Context context;
    private final int oldColor;
    private int color;
    private String title;
    private int iconID = 0;
    private int displayParameters = FLAG_NONE;

    private final static int BUTTON_HEIGHT=100;
    private final static int BUTTON_WIDTH=200;
    private final static int BUTTON_RADIUS=10;
    private final static int BUTTON_STROKEWIDTH=2;

    public static final int[] COLORPALETTE =
            {0xff000000,0xff0000AB,0xff00AB00,0xff00ABAB,0xffAB0000,0xffAB00AB,0xffAB5600,0xffBBBBBB,
            0xff666666,0xff6666FF,0xff66FF66,0xff66FFFF,0xffFF6666,0xffFF66FF,0xffFFFF55,0xffFFFFFF,
            0xfffcf309,0xffd2cc1b};

    public static int[] COLORPALETTE_OLD = {0xff000000,0xff0000AB,0xff00AB00,0xff00ABAB,0xffAB0000,0xffAB00AB,0xffAB5600,0xffBBBBBB,
            0xff666666,0xff6666FF,0xff66FF66,0xff66FFFF,0xffFF6666,0xffFF66FF,0xffFFFF55,0xffFFFFFF,
            0xfffcf309,0xffd2cc1b,0xffe2ad20,0xfff6c266,0xff896c1e,0xffed7a14,0xffaa6121,0xff57381c};

    public ColorPicker(Context context, int oldColor, String title, int iconID, int displayParameters){
        this.context = context;
        this.oldColor = oldColor;
        this.color = oldColor;
        this.title = title;
        this.iconID = iconID;
        this.displayParameters = displayParameters;
    }

    public void setOnColorPickedListener(OnColorPickedListener onColorPickedListener){
        this.onColorPickedListener = onColorPickedListener;
    }

    private void setChosenColor(int color){
        this.color = color;
        ImagepipePreferences.addColorHistory(context,color);
        if (onColorPickedListener!=null){
            onColorPickedListener.onColorSelected(color);
        }
    }

    private void setNoColorSelected(){
        if (onColorPickedListener!=null){
            onColorPickedListener.onNoColorSelected();
        }
    }

    private final static int PALETTESIZE = 255;

    private int getBarHeight(){
        if (ImagepipePreferences.isDeviceLandscape(context)){
            return 255;
        }
        return 60;
    }

    private Bitmap drawPaletteBitmap(ImageView imageView){
        Bitmap paletteBitmap = Bitmap.createBitmap(PALETTESIZE+1,PALETTESIZE+1, Bitmap.Config.ARGB_8888);
        Canvas paletteCanvas = new Canvas(paletteBitmap);
        Paint palettePaint = new Paint();
        palettePaint.setColor(Color.WHITE);
        palettePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        int red = Color.red(color); int green = Color.green(color); int blue = Color.blue(color);
        int circleX=0; int circleY=0;
        for (int y=0; y<=255; y++){
            for (int x=0; x<=255; x++){
                paletteBitmap.setPixel(x,y,Color.rgb(x,y,255-y));
                if ((red==x) && (green==y) && (blue==255-y)){
                    circleX=x; circleY=y;
                }
            }
        }
        paletteCanvas.drawCircle(circleX,circleY,5,palettePaint);
        imageView.setImageDrawable(new BitmapDrawable(paletteBitmap));
        return paletteBitmap;
    }

    private Bitmap drawBrightnessBar(ImageView imageView){
        Bitmap barBitmap = Bitmap.createBitmap(255,getBarHeight(), Bitmap.Config.ARGB_8888);
        float[] hsv = new float[3]; Color.colorToHSV(color,hsv);
        for (int x=1; x<255; x++){
            for (int y=getBarHeight()-1; y>0; y--){
                hsv[1]=(float) y/getBarHeight();
                hsv[2]=(float) x/255f;
                int c = Color.HSVToColor(hsv);
                barBitmap.setPixel(x,y,c);
            }
        }
        imageView.setImageDrawable(new BitmapDrawable(barBitmap));
        return barBitmap;
    }

    public static Bitmap drawButton(Context context, ImageView imageView, TextView textView, int color){
        RectF rectF = new RectF(BUTTON_STROKEWIDTH,BUTTON_STROKEWIDTH,BUTTON_WIDTH-BUTTON_STROKEWIDTH,BUTTON_HEIGHT-BUTTON_STROKEWIDTH);
        Bitmap buttonBitmap = Bitmap.createBitmap(BUTTON_WIDTH,BUTTON_HEIGHT, Bitmap.Config.ARGB_8888);
        Canvas buttonCanvas = new Canvas(buttonBitmap);
        Paint buttonPaint = new Paint();
        buttonPaint.setColor(color);
        buttonPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        buttonCanvas.drawRoundRect(rectF,BUTTON_RADIUS,BUTTON_RADIUS,buttonPaint);
        if (color==Color.TRANSPARENT){
            buttonPaint.setStyle(Paint.Style.FILL);
            buttonPaint.setColor(Color.WHITE);
            Bitmap trbitmap = getTransparentColorBitmap(context,BUTTON_WIDTH-BUTTON_STROKEWIDTH*2-BUTTON_RADIUS*2,BUTTON_HEIGHT-BUTTON_STROKEWIDTH*2-BUTTON_RADIUS*2);
            buttonCanvas.drawBitmap(trbitmap,BUTTON_STROKEWIDTH+BUTTON_RADIUS,BUTTON_STROKEWIDTH+BUTTON_RADIUS,buttonPaint);
        }
        buttonPaint.setStyle(Paint.Style.STROKE);
        buttonPaint.setStrokeWidth(3);
        buttonPaint.setColor(Color.WHITE);
        buttonCanvas.drawRoundRect(rectF,BUTTON_RADIUS,BUTTON_RADIUS,buttonPaint);
        imageView.setImageDrawable(new BitmapDrawable(buttonBitmap));
        if (textView!=null){
            textView.setText("#"+Integer.toHexString(color));
        }
        return buttonBitmap;
    }

    public static Bitmap getTransparentColorBitmap(Context context, int width, int height){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds=false;
        Bitmap tbpattern = BitmapFactory.decodeResource(context.getResources(), R.drawable.transparent_symbol,options);
        tbpattern = Bitmap.createScaledBitmap(tbpattern,50,50,false);
        tbpattern.isMutable();
        Bitmap tpbatternTarget = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Paint patternPaint = new Paint();
        patternPaint.setStyle(Paint.Style.FILL);
        //patternPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        Canvas pc = new Canvas(tpbatternTarget);
        for (int x = 0; x < width; x=x+50){
            for (int y = 0; y < height; y=y+50) {
                pc.drawBitmap(tbpattern,x,y,patternPaint);
            }
        }
        return tpbatternTarget;
    }

    @SuppressLint("ClickableViewAccessibility")
    public void show() {
        AlertDialog.Builder adBuilder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View cpDialogView = layoutInflater.inflate(R.layout.colorpicker, null);
        final ImageView paletteView = (ImageView) cpDialogView.findViewById(R.id.colorpicker_palette);
        final ImageView barView = (ImageView) cpDialogView.findViewById(R.id.colorpicker_brightness);
        final ImageView colorOld = (ImageView) cpDialogView.findViewById(R.id.colorpicker_colorOld);
        final TextView colorOldText = (TextView) cpDialogView.findViewById(R.id.colorpicker_colorOldHexText);
        final ImageView colorNew = (ImageView) cpDialogView.findViewById(R.id.colorpicker_colorNew);
        final TextView colorNewText = (TextView) cpDialogView.findViewById(R.id.colorpicker_colorNewHexText);
        final ImageButton transparentButton = cpDialogView.findViewById(R.id.brushview_button18);
        drawPaletteBitmap(paletteView); drawBrightnessBar(barView); drawButton(context,colorOld,colorOldText,oldColor); drawButton(context,colorNew,colorNewText,color);
        paletteView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction()==MotionEvent.ACTION_UP){
                    int x = Math.round(motionEvent.getX()*255/paletteView.getWidth());
                    int y = Math.round(motionEvent.getY()*255/paletteView.getHeight());
                    int c = Color.rgb(x,y,255-y);
                    if (c!=color){
                        color = c;
                        drawPaletteBitmap(paletteView); drawButton(context,colorNew,colorNewText,color); drawBrightnessBar(barView);;
                    }
                    return true;
                }
                return true; // consume all touch events to speed up the selection.
            }
        });
        barView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int x = Math.round(motionEvent.getX()*255/barView.getWidth());
                int y = Math.round(motionEvent.getY()*getBarHeight()/barView.getHeight());
                float[] hsv = new float[3]; Color.colorToHSV(color,hsv);
                hsv[1]=(float) y/getBarHeight();
                hsv[2]=(float) x/255f;
                int c = Color.HSVToColor(hsv);
                if (c!=color){
                    color = c;
                    drawButton(context,colorNew,colorNewText,color);
                }
                return true;
            }
        });
        colorOld.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                color = oldColor;
                drawButton(context,colorNew,colorNewText,color);
                drawPaletteBitmap(paletteView);
                drawBrightnessBar(barView);
                return true;
            }
        });
        adBuilder.setView(cpDialogView);
        if (title!=null){
            adBuilder.setTitle(title);
        }
        if (iconID!=0){
            adBuilder.setIcon(iconID);
        }
        adBuilder.setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setChosenColor(color);
                dialogInterface.dismiss();
            }
        });
        adBuilder.setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setNoColorSelected();
                dialogInterface.dismiss();
            }
        });
        final AlertDialog alertDialog = adBuilder.create();
        alertDialog.show();
        colorNew.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                setChosenColor(color);
                alertDialog.dismiss();
                return true;
            }
        });
        int colorHistory1=ImagepipePreferences.getColorHistory1(context);
        int colorHistory2=ImagepipePreferences.getColorHistory2(context);
        for (int i=0; i<COLORPALETTE.length; i++){
            int id = context.getResources().getIdentifier("brushview_button"+i,"id",context.getPackageName());
            ImageButton imageButton = (ImageButton) cpDialogView.findViewById(id);
            Bitmap bitmap = Bitmap.createBitmap(150,150, Bitmap.Config.ARGB_8888);
            if (i==16){
                bitmap.eraseColor(colorHistory1);
            } else
            if (i==17){
                if ((displayParameters & FLAG_TRANSPARENT) == FLAG_TRANSPARENT){
                    bitmap = getTransparentColorBitmap(context,150,150);
                } else
                {
                    bitmap.eraseColor(colorHistory2);
                }
            } else {
                bitmap.eraseColor(COLORPALETTE[i]);
            }
            imageButton.setImageDrawable(new BitmapDrawable(bitmap));
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String name = context.getResources().getResourceName(view.getId());
                    int charPos = name.indexOf("button");
                    String numberString = name.substring(charPos+6);
                    int position = Integer.parseInt(numberString);
                    int c = COLORPALETTE[position];
                    if ((position==17) && ((displayParameters & FLAG_TRANSPARENT) == FLAG_TRANSPARENT)){
                        c = Color.TRANSPARENT;
                    }
                    setChosenColor(c);
                    alertDialog.dismiss();
                }
            });
        }
        if (ImagepipePreferences.isDeviceLandscape(context)){
            WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
            alertDialog.getWindow().setLayout((int) Math.round(displayMetrics.widthPixels*0.85), (int) Math.round(displayMetrics.heightPixels*0.9));
        }
    }

    public static class Builder{

    }

}
