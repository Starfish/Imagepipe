/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.*;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.preference.*;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import java.util.Locale;

public class Settings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener{

    private Context context;
    private final static int PICKPATH_REQUESTCODE = 987;

    @Override
    @SuppressWarnings("deprecation")
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.preferences);
        context = this;
        updateValuesDisplay();
    }

    @Override
    @SuppressWarnings("deprecation")
    protected void onResume(){
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    @SuppressWarnings("deprecation")
    protected void onPause(){
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @SuppressWarnings("deprecation")
    public void disableAllowedTags(){
        EditTextPreference editTextPreference = (EditTextPreference) findPreference(ImagepipePreferences.PREF_ALLOWED_TAGS);
        editTextPreference.setEnabled(false);
        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(this);
        Preference p= findPreference(ImagepipePreferences.PREF_ALLOWED_TAGS);
        String summary = getResources().getString(R.string.preference_allowed_tags_summary)+" "+ sp.getString(ImagepipePreferences.PREF_ALLOWED_TAGS,"");
        Spannable spannable = new SpannableString(summary);
        spannable.setSpan(new ForegroundColorSpan(Color.GRAY),0,spannable.length(),Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        String title = getResources().getString(R.string.preference_allowed_tags_title);
        Spannable spannable2 = new SpannableString(title);
        spannable2.setSpan(new ForegroundColorSpan(Color.GRAY),0,spannable2.length(),Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        p.setSummary(spannable);
        p.setTitle(spannable2);
    }

    @SuppressWarnings("deprecation")
    private void updateValuesDisplay(){
        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(this);
        PreferenceCategory preferenceCategoryPrivacy = (PreferenceCategory) findPreference("PREF_category_privacy");
        PreferenceCategory preferenceCategoryShare = (PreferenceCategory) findPreference("PREF_category_share");
        String file_ext;
        String[] file_ext_text = getResources().getStringArray(R.array.fileextension_text);
        String s=sp.getString(ImagepipePreferences.PREF_FILENAME,"");
        Preference p = findPreference(ImagepipePreferences.PREF_FILENAME);
        p.setSummary(getResources().getString(R.string.preference_filename_summary)+" "+s);
        s=sp.getString(ImagepipePreferences.PREF_NUMBERING,"");
        try {
            int i = Integer.valueOf(s);
            file_ext = file_ext_text[i-1];
        } catch (NumberFormatException e){
            file_ext = "";
        }
        p = findPreference(ImagepipePreferences.PREF_NUMBERING);
        p.setSummary(getResources().getString(R.string.preference_numbering_summary)+" "+file_ext);
        p = findPreference(ImagepipePreferences.PREF_MAXQUALITY);
        /*
        p.setSummary(getResources().getString(R.string.preference_maxquality_summary)+" "+String.valueOf(sp.getString(ImagepipePreferences.PREF_MAXQUALITY,"80")));
         */
        p = findPreference(ImagepipePreferences.PREF_COMPRESSFORMAT);
        p.setSummary(getResources().getString(R.string.preference_compressformat_summary)+" "+ImagepipePreferences.getCompressFormatFileExtension(this).toString().toUpperCase(Locale.ROOT));
        p= findPreference(ImagepipePreferences.PREF_ALLOWED_TAGS);
        p.setSummary(getResources().getString(R.string.preference_allowed_tags_summary)+" "+ sp.getString(ImagepipePreferences.PREF_ALLOWED_TAGS,""));
        // feature is only available in sdk version 24 or above
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.N){
            disableAllowedTags();
        }
        ColorPreference colorPreference = (ColorPreference) findPreference("PREF_replacecolor");
        if (ImagepipePreferences.getCompressFormat(context)== Bitmap.CompressFormat.JPEG){
            colorPreference.setShouldDisableView(true);
            colorPreference.setEnabled(false);
        } else {
            colorPreference.setEnabled(true);
            colorPreference.setShouldDisableView(false);
            colorPreference.setColor(ImagepipePreferences.getReplaceColor(context));
            colorPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ColorPicker colorPicker = new ColorPicker(context,ImagepipePreferences.getReplaceColor(context),getResources().getString(R.string.preference_remove_transparency_dialogtitle),R.mipmap.ic_palette_white_24dp,ColorPicker.FLAG_TRANSPARENT);
                    colorPicker.setOnColorPickedListener(new ColorPicker.OnColorPickedListener() {
                        @Override
                        public void onColorSelected(int color) {
                            ImagepipePreferences.setReplaceColor(context,color);
                        }

                        @Override
                        public void onNoColorSelected() {

                        }
                    });
                    colorPicker.show();
                    return true;
                }
            });
        }
        CheckBoxPreference clearAfterSharePreference = (CheckBoxPreference) findPreference(ImagepipePreferences.PREF_CLEARAFTERSHARE);
        if (clearAfterSharePreference!=null){
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                preferenceCategoryPrivacy.removePreference(clearAfterSharePreference);
            }
        }
        Preference pathPreference = (Preference) findPreference(ImagepipePreferences.PREF_PATH);
        if (pathPreference!=null){
            pathPreference.setSummary(context.getResources().getString(R.string.preference_path_summary)+" "+ImagepipePreferences.getPath(context));
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sp, String id){
        updateValuesDisplay();
        if ((id.equals(ImagepipePreferences.PREF_NUMBERING))){
            PictureCache.recreateFileNameList(context);
            ImagepipePreferences.setImageNeedsToBeSavedFlag(context,true);
        }
        // reset internal file name counter when file name changes.
        if (id.equals(ImagepipePreferences.PREF_FILENAME)){
            ImagepipePreferences.setFileNameCounter(context,ImagepipePreferences.PREF_FILENAMECOUNTER_DEFAULT);
            ImagepipePreferences.setImageNeedsToBeSavedFlag(context,true);
        }
        if ((id.equals(ImagepipePreferences.PREF_SCALEMAX)) ||
                (id.equals(ImagepipePreferences.PREF_COMPRESSFORMAT))){
            ImagepipePreferences.setImageNeedsToBeSavedFlag(context,true);
        }
        if (id.equals(ImagepipePreferences.PREF_MAXQUALITY2)){
            ImagepipePreferences.setQualitymaxvalueChangedFlag(context);
        }
    }
}