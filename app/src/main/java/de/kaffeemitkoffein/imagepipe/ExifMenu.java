/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.View;
import android.widget.*;

public class ExifMenu extends Activity {

    private Context context;
    private ExifData exifData;
    private String[] allowedTags;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private boolean isAllowedTag(String tag){
        for (int i=0; i<allowedTags.length; i++){
            if (allowedTags[i].equalsIgnoreCase(tag)){
                return true;
            }
        }
        return false;
    }

    private int getTagPosition(String tag){
        for (int i=0; i<exifData.exifItems.size(); i++){
            if (exifData.exifItems.get(i).tag.equalsIgnoreCase(tag)){
                return i;
            }
        }
        return -1;
    }

    private void setTagValue(String tag, boolean state){
        int pos = getTagPosition(tag);
        if (pos>=0){
            exifData.exifItems.get(pos).isAllowed = state;
        }
    }

    private String generateAllowedString(){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i=0; i<exifData.exifItems.size(); i++){
            ExifData.ExifItem exifItem = exifData.exifItems.get(i);
            if (exifItem.isAllowed){
                stringBuilder.append(exifItem.tag);
                stringBuilder.append(ImagepipePreferences.TAG_SEPERATOR);
            }
        }
        // remove last # if string not empty
        if (stringBuilder.length()>0){
            stringBuilder.setLength(stringBuilder.length()-1);
        }
        return stringBuilder.toString();
    }

    private void addCheckBox(LinearLayout parent, ExifData.ExifItem exifItem){
        CheckBox checkBox = new CheckBox(this);
        String s = exifItem.description;
        if (exifItem.present){
            int spanStart = s.length();
            s = s + " (" + exifItem.value+")";
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(s);
            int color = getResources().getColor(R.color.primaryLightColor);
            spannableStringBuilder.setSpan(new ForegroundColorSpan(color),spanStart,s.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            checkBox.setText(spannableStringBuilder);
        } else {
            checkBox.setText(s);
        }
        //checkBox.setText(exifItem.description);
        checkBox.setChecked(isAllowedTag(exifItem.tag));
        checkBox.setTag(exifItem);
        checkBox.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean state) {
                ExifData.ExifItem exifItem = (ExifData.ExifItem) compoundButton.getTag();
                setTagValue(exifItem.tag,state);
                ImagepipePreferences.setPrefAllowedTags(context,generateAllowedString());
                ImagepipePreferences.setAllowedTagsModified(context);
                // disable temporary "do not show again" if new tags were added
                if (state){
                    ImagepipePreferences.setTemporarilyDisableWarning(context,false);
                }
                askForClearCache();
            }
        });
        int color = context.getResources().getColor(R.color.secondaryColor);
        if (exifItem.present){
            color = context.getResources().getColor(R.color.secondaryLightColor);
        }
        if (isAllowedTag(exifItem.tag)){
            checkBox.setChecked(true);
            exifItem.isAllowed = true;
        } else {
            checkBox.setChecked(false);
            exifItem.isAllowed = false;
        }
        checkBox.setTextColor(color);
        parent.addView(checkBox);
        /* EditText editText = new EditText(this);
        editText.setText(exifItem.value);
        editText.setTag(exifItem);
        editText.setSingleLine(true);
        editText.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
        parent.addView(editText);
        */
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        context = getApplicationContext();
        setContentView(R.layout.activity_exifmenu);
        //exifData = new ExifData(context,null);
        exifData = ImagepipePreferences.readExifData(context);
        allowedTags = ImagepipePreferences.getAllowedTags(context);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.exifmenu_linearlayout);
        for (int i=0; i<exifData.exifItems.size(); i++){
            addCheckBox(linearLayout,exifData.exifItems.get(i));
        }
        Button resetButton = (Button) findViewById(R.id.exifmenu_resetbutton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setPrefAllowedTags(context,"");
                ImagepipePreferences.setAllowedTagsModified(context);
                recreate();
            }
        });
    }

    public void askForClearCache(){

    }
}
