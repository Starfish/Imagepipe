/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

public class Log {

    private Log() {
    }

    private static boolean isDebugBuild(){
        return BuildConfig.VERSION_NAME.contains("-debug_");
    }

    public static void v(String text){
        if (isDebugBuild()){
            // this log can be filtered in logcat using "V/imagepipe"
            android.util.Log.v("imagepipe",text);
        }
    }

    public static void printStackTrace(Exception e){
        StackTraceElement[] stackTraceElements = e.getStackTrace();
        for (int j=0; j<stackTraceElements.length; j++){
            Log.v(j+" -> "+stackTraceElements[j].getLineNumber()+" "+stackTraceElements[j].getClassName()+" "+stackTraceElements[j].getMethodName()+" "+stackTraceElements[j].getFileName());
        }
    }
}
