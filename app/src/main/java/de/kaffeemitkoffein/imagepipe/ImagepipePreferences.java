/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.ExifInterface;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.Surface;
import android.view.WindowManager;

public class ImagepipePreferences {

    public static final String PREF_SCALEMAX = "PREF_scale_max_diameter";
    public static final String PREF_SCALEMODE = "PREF_scalemode";
    public static final String PREF_PATH = "PREF_path";
    public static final String PREF_FILENAME = "PREF_filename";
    public static final String PREF_QUALITY = "PREF_quality";
    public static final String PREF_MAXQUALITY = "PREF_maxquality";
    public static final String PREF_MAXQUALITY2 = "PREF_maxquality2";
    public static final String PREF_MAXQUALITY_CHANGED_FLAG = "PREF_maxquality2_changed";
    public static final String PREF_COMPRESSFORMAT = "PREF_compressformat";
    private static final String PREF_AUTOPIPE = "PREF_autopipe";
    public static final String PREF_NUMBERING = "PREF_numbering";
    private static final String PREF_AUTOSCALE = "PREF_autoscale";
    private static final String PREF_AUTOROTATE = "PREF_autorotate";
    private static final String PREF_PREVIEWQUALITY = "PREF_previewquality";
    private static final String PREF_FORCEDOWNSCALE = "PREF_forcedownscale";
    private static final String PREF_BUBLLECOUNTER = "PREF_bubblecounter";
    private static final String PREF_SHOWHINTS = "PREF_showhints";
    private static final String PREF_SAVEDIMAGE_URI = "PREF_savedimage_uri";
    private static final String PREF_SAVEDIMAGE_FILESIZE = "PREF_savedimage_filesize";
    private static final String PREF_ORIGINALIMAGE_URI = "PREF_originalimage_uri";
    private static final String PREF_ORIGINALIMAGE_FILESIZE = "PREF_originalimage_filesize";
    private static final String PREF_ORIGINALIMAGE_FILENAME = "PREF_originalimage_filename";
    private static final String PREF_ORIGINALIMAGE_WIDTH = "PREF_originalimage_width";
    private static final String PREF_ORIGINALIMAGE_HEIGHT = "PREF_originalimage_height";
    public static final String PREF_ALLOWED_TAGS="PREF_allowed_tags";
    public static final String PREF_ALLOWED_TAGS_MODIFIED="PREF_allowed_tags_modified";
    private static final String PREF_BILINEAR_FILTERING="PREF_bilinear_filtering";
    private static final String PREF_AUTOSAVE="PREF_autosave";
    private static final String PREF_ASPECT_RATIO="PREF_aspect_ratio";
    public static final String PREF_LAST_VERSION_CODE = "PREF_last_version_code";
    public static final String PREF_LAST_COLOR = "PREF_last_color";
    public static final String PREF_FILTER = "PREF_filter";
    public static final String PREF_AUTOAPPLY_FILTER = "PREF_autoapply_filter";
    public static final String PREF_REMOVE_TRANSPARENCY = "PREF_remove_transparency";
    public static final String PREF_REPLACECOLOR = "PREF_replacecolor";
    public static final String PREF_COLORHISTORY1 = "PREF_colorhistory1";
    public static final String PREF_COLORHISTORY2 = "PREF_colorhistory2";
    public static final String PREF_FILENAMECOUNTER = "PREF_filenamecounter";
    public static final String PREF_FILECOUNTERCACHE = "PREF_filecountercache";
    public static final String PREF_CLEARAFTERSHARE = "PREF_clearaftershare";
    public static final String PREF_CROP_AFTER_ROTATION = "PREF_crop_after_rotation";
    public static final String PREF_EXIFDATA = "PREF_exifinterface";
    public static final String PREF_EXIF_TEMPLATE = "PREF_exif_template";
    public static final String PREF_NO_WARN = "PREF_no_warn";
    public static final String PREF_WARN_TEMPORARILY_DISABLED = "PREF_no_warn_temporarily";
    public static final String PREF_IMAGE_NEEDS_TO_BE_SAVED_FLAG = "PREF_need_to_save_flag";

    public static final String PREF_SCALEMAX_DEFAULT = "1024";
    private static final int PREF_SCALEMODE_DEFAULT = ImageReceiver.ResizeOptions.MAX;
    private static final String PREF_FILENAME_DEFAULT = "Imagepipe";
    private static final boolean PREF_KEEP_ORIGINAL_NAME_DEFAULT = false;
    private static final int PREF_QUALITY_DEFAULT = 70;
    private static final int PREF_MAXQUALITY_DEFAULT = 80;
    private static final String PREF_COMPRESSFORMAT_DEFAULT = ImageFileFormat.JPEG;
    private static final boolean PREF_AUTOPIPE_DEFAULT = true;
    private static final String PREF_NUMBERING_DEFAULT = "1";
    private static final boolean PREF_AUTOSCALE_DEFAULT = true;
    private static final boolean PREF_AUTOROTATE_DEFAULT = true;
    private static final boolean PREF_PREVIEWQUALITY_DEFAULT = true;
    private static final boolean PREF_FORCEDOWNSCALE_DEFAULT = true;
    private static final boolean PREF_SMARTCROP_DEFAULT=true;
    private static final int PREF_BUBLLECOUNTER_DEFAULT = 0;
    private static final boolean PREF_SHOWHINTS_DEFAULT = true;
    private static final String PREF_URI_DEFAULT = "";
    private static final long PREF_FILESIZE_DEFAULT = 0;
    private static final String PREF_ALLOWED_TAGS_DEFAULT="";
    private static final boolean PREF_BILINEAR_FILTERING_DEFAULT=true;
    private static final boolean PREF_AUTOSAVE_DEFAULT = false;
    private static final String PREF_ASPECT_RATIO_DEFAULT= "";
    public static final int PREF_LAST_VERSION_CODE_DEFAULT = 0;
    public static final int PREF_LAST_COLOR_DEFAULT = Color.WHITE;
    public static final int PREF_FILTER_DEFAULT = ImageFilters.FilerType.GREYSCALE;
    public static final boolean PREF_AUTOAPPLY_FILTER_DEFAULT = false;
    public static final boolean PREF_REMOVE_TRANSPARENCY_DEFAULT = false;
    public static final int PREF_REPLACECOLOR_DEFAULT = Color.TRANSPARENT;
    public static final int PREF_COLORHISTORY1_DEFAULT = 0xfffcf309;
    public static final int PREF_COLORHISTORY2_DEFAULT = 0xffd2cc1b;
    public static final long PREF_FILENAMECOUNTER_DEFAULT = 0;
    public static final long PREF_FILECOUNTERCACHE_DEFAULT = 0;
    public static final boolean PREF_CLEARAFTERSHARE_DEFAULT = false;
    public static final boolean PREF_CROP_AFTER_ROTATION_DEFAULT = false;
    public static final boolean PREF_NO_WARN_DEFAULT = false;
    public static final boolean PREF_WARN_TEMPORARILY_DISABLED_DEFAULT = false;

    public static int getQualitymaxvalue_Legacy(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String quality_maxvalue = sharedPreferences.getString(PREF_MAXQUALITY,Integer.toString(PREF_MAXQUALITY_DEFAULT));
        try {
            int i = Integer.valueOf(quality_maxvalue);
            return i;
        } catch (NumberFormatException e) {
            SharedPreferences.Editor pref_editor = sharedPreferences.edit();
            pref_editor.putString(PREF_MAXQUALITY,Integer.toString(PREF_MAXQUALITY_DEFAULT));
            pref_editor.apply();
            return Integer.valueOf(PREF_MAXQUALITY_DEFAULT);
        }
    }

    public static void setQualitymaxvalue(Context context, int value){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(PREF_MAXQUALITY2,value);
        pref_editor.apply();
    }

    public static int getQualitymaxvalue(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int value = sharedPreferences.getInt(PREF_MAXQUALITY2,PREF_MAXQUALITY_DEFAULT);
        return value;
    }

    public static void setQualitymaxvalueChangedFlag(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(PREF_MAXQUALITY_CHANGED_FLAG,true);
        pref_editor.apply();
    }

    public static void resetQualitymaxvalueChangedFlag(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(PREF_MAXQUALITY_CHANGED_FLAG,false);
        pref_editor.apply();
    }

    public static boolean maxQualityChanged(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_MAXQUALITY_CHANGED_FLAG,false);
    }

    public static int getResizeMode(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(PREF_SCALEMODE,PREF_SCALEMODE_DEFAULT);
    }

    public static void setResizeMode(Context context, int scaleMode){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(PREF_SCALEMODE,scaleMode);
        pref_editor.apply();
    }

    public static boolean forceDownscale(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_FORCEDOWNSCALE,PREF_FORCEDOWNSCALE_DEFAULT);
    }

    static class ImageFileFormat {
        public final static String JPEG = "0";
        public final static String PNG = "1";
        public final static String WEBP = "2";
    }

    public static String getImageFileFormatValue(Bitmap.CompressFormat compressFormat){
        switch (compressFormat) {
            case PNG: return ImageFileFormat.PNG;
            case WEBP: return ImageFileFormat.WEBP;
        }
        return ImageFileFormat.JPEG;
    }

    public static String getImageFileFormat(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(PREF_COMPRESSFORMAT,PREF_COMPRESSFORMAT_DEFAULT);
    }


    public static Bitmap.CompressFormat getImageFileCompressFormat(String s){
        if (s.equals(ImageFileFormat.PNG)){
            return Bitmap.CompressFormat.PNG;
        }
        if (s.equals(ImageFileFormat.WEBP)){
            return Bitmap.CompressFormat.WEBP;
        }
        return Bitmap.CompressFormat.JPEG;
    }

    public static Bitmap.CompressFormat getCompressFormat(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return getImageFileCompressFormat(sharedPreferences.getString(PREF_COMPRESSFORMAT,PREF_COMPRESSFORMAT_DEFAULT));
    }

    public static Bitmap.CompressFormat getCompressFormat(Context context, int compressValue){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Bitmap.CompressFormat compressFormat = getImageFileCompressFormat(sharedPreferences.getString(PREF_COMPRESSFORMAT,PREF_COMPRESSFORMAT_DEFAULT));
        if (compressFormat== Bitmap.CompressFormat.WEBP){
            if (android.os.Build.VERSION.SDK_INT >= 30){
                if (compressValue==100){
                    return Bitmap.CompressFormat.WEBP_LOSSLESS;
                } else {
                    return Bitmap.CompressFormat.WEBP_LOSSY;
                }
            } else {
                return Bitmap.CompressFormat.WEBP;
            }
        }
        return compressFormat;
    }

    public static CharSequence getCompressFormatFileExtension(Context context){
        String compressFormat = getImageFileFormatValue(getCompressFormat(context));
        if (compressFormat.equals(ImageFileFormat.PNG)){
            return "png";
        }
        if (compressFormat.equals(ImageFileFormat.WEBP)){
            return "webp";
        }
        return "jpg";
    }

    public static boolean autopipe(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_AUTOPIPE,PREF_AUTOPIPE_DEFAULT);
    }

    public static String getNumbering(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(PREF_NUMBERING,PREF_NUMBERING_DEFAULT);
    }

    public static boolean shouldShowHints(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_SHOWHINTS,PREF_SHOWHINTS_DEFAULT);
    }

    public static void setShouldShowHints(Context context, Boolean b){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(PREF_SHOWHINTS,b);
        pref_editor.apply();
    }

    public static ImageContainer getSavedImageContainer(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String uriString = sharedPreferences.getString(PREF_SAVEDIMAGE_URI,PREF_URI_DEFAULT);
        if (uriString.equals("")){
            return null;
        }
        Uri uri = Uri.parse(uriString);
        long filesize = sharedPreferences.getLong(PREF_SAVEDIMAGE_FILESIZE,PREF_FILESIZE_DEFAULT);
        ImageContainer imageContainer = new ImageContainer(context,uri,filesize);
        return imageContainer;
    }

    public static void setSavedImageContainer(Context context, ImageContainer imageContainer){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        if (imageContainer==null){
            resetSavedImageContainer(context);
        } else {
            pref_editor.putString(PREF_SAVEDIMAGE_URI,imageContainer.uri.toString());
            pref_editor.putLong(PREF_SAVEDIMAGE_FILESIZE,imageContainer.filesize);
            pref_editor.apply();
        }
    }

    public static void resetSavedImageContainer(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(PREF_SAVEDIMAGE_URI,PREF_URI_DEFAULT);
        pref_editor.putLong(PREF_SAVEDIMAGE_FILESIZE,PREF_FILESIZE_DEFAULT);
        pref_editor.apply();
    }

    public static ImageContainer getOriginalImageContainer(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String uriString = sharedPreferences.getString(PREF_ORIGINALIMAGE_URI,PREF_URI_DEFAULT);
        if (uriString.equals("")){
            return null;
        }
        try {
            Uri uri = Uri.parse(uriString);
            long filesize = sharedPreferences.getLong(PREF_ORIGINALIMAGE_FILESIZE,PREF_FILESIZE_DEFAULT);
            ImageContainer imageContainer = new ImageContainer(context,uri,filesize);
            imageContainer.filename = sharedPreferences.getString(PREF_ORIGINALIMAGE_FILENAME,PREF_URI_DEFAULT);
            imageContainer.originalHeight = sharedPreferences.getInt(PREF_ORIGINALIMAGE_HEIGHT,0);
            imageContainer.originalWidth = sharedPreferences.getInt(PREF_ORIGINALIMAGE_WIDTH,0);
            return imageContainer;
        } catch (Exception e){
            // nothing to do
        }
        return null;
    }

    public static void setOriginalImageContainer(Context context, ImageContainer imageContainer){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        if (imageContainer==null){
            resetOriginalImageContainer(context);
        } else {
            pref_editor.putString(PREF_ORIGINALIMAGE_URI,imageContainer.uri.toString());
            pref_editor.putLong(PREF_ORIGINALIMAGE_FILESIZE,imageContainer.filesize);
            pref_editor.putInt(PREF_ORIGINALIMAGE_WIDTH,imageContainer.originalWidth);
            pref_editor.putInt(PREF_ORIGINALIMAGE_HEIGHT, imageContainer.originalHeight);
            pref_editor.putString(PREF_ORIGINALIMAGE_FILENAME,imageContainer.filename);
            pref_editor.apply();
        }
    }

    public static void resetOriginalImageContainer(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(PREF_ORIGINALIMAGE_URI,PREF_URI_DEFAULT);
        pref_editor.putLong(PREF_ORIGINALIMAGE_FILESIZE,PREF_FILESIZE_DEFAULT);
        pref_editor.putString(PREF_ORIGINALIMAGE_FILENAME,PREF_URI_DEFAULT);
        pref_editor.apply();
    }

    public static final String TAG_SEPERATOR="#";

    public static String[] getAllowedTags(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String s = sharedPreferences.getString(PREF_ALLOWED_TAGS,PREF_ALLOWED_TAGS_DEFAULT);
        if (s.length()>0){
            String[] result = s.split(TAG_SEPERATOR);
            return result;
        }
        return new String[]{};
    }

    public static boolean keepSomeExifTags(Context context){
        String[] allowedTags = getAllowedTags(context);
        return (allowedTags.length>0);
    }

    public static void setPrefAllowedTags(Context context, String s){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(PREF_ALLOWED_TAGS,s);
        pref_editor.apply();
    }

    public static void setAllowedTagsModified(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(PREF_ALLOWED_TAGS_MODIFIED,true);
        pref_editor.apply();
    }

    public static void resetAllowedTagsModified(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(PREF_ALLOWED_TAGS_MODIFIED,false);
        pref_editor.apply();
    }

    public static boolean allowedTagsModified(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_ALLOWED_TAGS_MODIFIED, false);
    }

    public static void setScaleMax(Context context, int i){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(PREF_SCALEMAX,String.valueOf(i));
        pref_editor.apply();
    }

    public static int getQuality(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(PREF_QUALITY,PREF_QUALITY_DEFAULT);
    }

    public static void setQuality(Context context, int i){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(PREF_QUALITY,i);
        pref_editor.apply();
    }

    public static String getScaleMax(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(PREF_SCALEMAX,PREF_SCALEMAX_DEFAULT);
    }

    public static boolean isBilinearFilteringEnabled(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_BILINEAR_FILTERING,PREF_BILINEAR_FILTERING_DEFAULT);
    }

    public static boolean autosave(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_AUTOSAVE,PREF_AUTOSAVE_DEFAULT);
    }

    public static boolean validateAspectRatioString(String s){
        String[] result = s.split(":");
        if (result.length<2){
            return false;
        }
        try {
            float a = Float.valueOf(result[0]);
            float b = Float.valueOf(result[1]);
        } catch (NumberFormatException e){
            return false;
        }
        return true;
    }

    public static boolean applyAspectRatioString(Context context, String s) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        if (validateAspectRatioString(s)) {
            pref_editor.putString(PREF_ASPECT_RATIO, s);
            pref_editor.apply();
            return true;
        }
        pref_editor.putString(PREF_ASPECT_RATIO,PREF_ASPECT_RATIO_DEFAULT);
        pref_editor.apply();
        return false;
    }

    public static Float getAspectRatioFloat(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String aspectRatio = sharedPreferences.getString(PREF_ASPECT_RATIO, PREF_ASPECT_RATIO_DEFAULT);
        if (validateAspectRatioString(aspectRatio)) {
            String[] result = aspectRatio.split(":");
            try {
                Float f = (Float.valueOf(result[1]) / Float.valueOf(result[0]));
                return f;
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    public static String getAspectRatioString(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(PREF_ASPECT_RATIO, PREF_ASPECT_RATIO_DEFAULT);
    }

    public static void setCurrentAppVersionFlag(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(PREF_LAST_VERSION_CODE, BuildConfig.VERSION_CODE);
        pref_editor.apply();
    }

    public static int getLastAppVersion(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(PREF_LAST_VERSION_CODE, PREF_LAST_VERSION_CODE_DEFAULT);
    }

    public static String getOriginalName(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String result = sharedPreferences.getString(PREF_ORIGINALIMAGE_FILENAME,"");
        if (result.equals("")){
            return sharedPreferences.getString(PREF_FILENAME,PREF_FILENAME_DEFAULT);
        }
        result = result.substring(0,result.lastIndexOf("."));
        return result;
    }

    public static String getSkeletonFilename(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(PREF_FILENAME,PREF_FILENAME_DEFAULT);
    }

    public static void setLastColor(Context context, int lastColor){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(PREF_LAST_COLOR, lastColor);
        pref_editor.apply();
    }

    public static int getLastColor(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(PREF_LAST_COLOR, PREF_LAST_COLOR_DEFAULT);
    }

    public static int getFilter(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(PREF_FILTER, PREF_FILTER_DEFAULT);
    }

    public static void setFilter(Context context, int filter){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(PREF_FILTER, filter);
        pref_editor.apply();
    }

    public static boolean autoApplyFilter(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_AUTOAPPLY_FILTER, PREF_AUTOAPPLY_FILTER_DEFAULT);
    }

    public static boolean removeTransparency(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_REMOVE_TRANSPARENCY, PREF_REMOVE_TRANSPARENCY_DEFAULT);
    }

    public static int getReplaceColor(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(PREF_REPLACECOLOR, PREF_REPLACECOLOR_DEFAULT);
    }

    public static void setReplaceColor(Context context, int color){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(PREF_REPLACECOLOR, color);
        pref_editor.apply();
    }

    public static int getColorHistory1(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(PREF_COLORHISTORY1, PREF_COLORHISTORY1_DEFAULT);
    }

    public static int getColorHistory2(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(PREF_COLORHISTORY2, PREF_COLORHISTORY2_DEFAULT);
    }

    public static void addColorHistory(Context context, int color){
        int hColor1 = getColorHistory1(context);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(PREF_COLORHISTORY1, color);
        pref_editor.putInt(PREF_COLORHISTORY2, hColor1);
        pref_editor.apply();
    }

    public static boolean isDeviceLandscape(Context context){
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (windowManager!=null){
            if ((windowManager.getDefaultDisplay().getRotation()==Surface.ROTATION_90) || (windowManager.getDefaultDisplay().getRotation()==Surface.ROTATION_180)){
                return true;
            }
        }
        return false;
    }

    public static void setFileNameCounter(Context context, long fileNameCounter){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putLong(PREF_FILENAMECOUNTER, fileNameCounter);
        pref_editor.apply();
    }

    public static long getFileNameCounter(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getLong(PREF_FILENAMECOUNTER, PREF_FILENAMECOUNTER_DEFAULT);
    }

    public static void setFileCounterCache(Context context, long fileCounterCache){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putLong(PREF_FILECOUNTERCACHE, fileCounterCache);
        pref_editor.commit();
    }

    public static long getFileCounterCache(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getLong(PREF_FILECOUNTERCACHE, PREF_FILECOUNTERCACHE_DEFAULT);
    }


    public static boolean clearAfterShare(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_CLEARAFTERSHARE, PREF_CLEARAFTERSHARE_DEFAULT);
    }

    public static boolean cropAfterRotation(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_CROP_AFTER_ROTATION, PREF_CROP_AFTER_ROTATION_DEFAULT);
    }

    public static void setCropAfterRotation(Context context, boolean crop){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(PREF_CROP_AFTER_ROTATION, crop);
        pref_editor.apply();
    }

    public static String getPath(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String path = sharedPreferences.getString(PREF_PATH, ImageWriter.getDefaultSavePath(context));
        return path;
    }

    public static Uri getPathUri(Context context){
        Uri uri = Uri.parse(getPath(context));
        return uri;
    }

    public static void setPath(Context context, Uri uri){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(PREF_PATH, uri.toString());
        pref_editor.apply();
    }

    public static void saveExifData(Context context, ExifData exifData){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(PREF_EXIFDATA, exifData.getString());
        pref_editor.apply();
    }

    public static void resetExifData(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.remove(PREF_EXIFDATA);
        pref_editor.apply();
    }


    public static ExifData saveExifData(Context context, ExifInterface exifInterface){
        ExifData exifData = new ExifData(context,exifInterface);
        saveExifData(context,exifData);
        return exifData;
    }

    public static ExifData readExifData(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String result = sharedPreferences.getString(PREF_EXIFDATA,null);
        if (result==null){
            return new ExifData(context,null);
        }
        ExifData exifData = new ExifData(context,result,getAllowedTags(context));
        return exifData;
    }

    public static void saveExifTemplate(Context context, ExifData exifData){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(PREF_EXIF_TEMPLATE, exifData.getString());
        pref_editor.apply();
    }

    public static boolean warningPermanentlyDisabled(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_NO_WARN,PREF_NO_WARN_DEFAULT);
    }

    public static void setTemporarilyDisableWarning(Context context, boolean value){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(PREF_WARN_TEMPORARILY_DISABLED, value);
        pref_editor.apply();
    }

    public static boolean warningTemporarilyDisabled(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_WARN_TEMPORARILY_DISABLED,PREF_WARN_TEMPORARILY_DISABLED_DEFAULT);
    }

    public static boolean autoScale(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_AUTOSCALE,PREF_AUTOSCALE_DEFAULT);
    }

    public static boolean autoRotate(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_AUTOROTATE,PREF_AUTOROTATE_DEFAULT);
    }

    public static void setBubbleCounter(Context context, int value){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(PREF_BUBLLECOUNTER, value);
        pref_editor.apply();
    }

    public static int getBubbleCounter(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(PREF_BUBLLECOUNTER,PREF_BUBLLECOUNTER_DEFAULT);
    }

    public static void incrementBubbleCounter(Context context){
        int bubbleCounter = getBubbleCounter(context);
        bubbleCounter++;
        setBubbleCounter(context,bubbleCounter);
    }

    public static boolean previewQuality(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_PREVIEWQUALITY,PREF_PREVIEWQUALITY_DEFAULT);
    }

    public static void setImageNeedsToBeSavedFlag(Context context, boolean value){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(PREF_IMAGE_NEEDS_TO_BE_SAVED_FLAG, value);
        pref_editor.apply();
    }

    public static boolean imageNeedsToBeSavedFlag(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_IMAGE_NEEDS_TO_BE_SAVED_FLAG,false);
    }

}
