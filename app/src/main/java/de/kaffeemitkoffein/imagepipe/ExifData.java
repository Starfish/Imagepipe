/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.Context;
import android.graphics.Color;
import android.media.ExifInterface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import java.util.ArrayList;
import java.util.Comparator;

public class ExifData {

    private final static String SEPERATOR ="|";

    private Context context;
    public ArrayList<ExifItem> exifItems;
    private String[] allowedTags;

    public static class ExifItem implements Comparator<ExifItem> {
        private final static String SEPERATOR ="$";
        int minApi;
        String description;
        String tag;
        String value;
        boolean present;
        boolean isAllowed;

        public ExifItem(){
        }

        @Override
        public int compare(ExifItem exifItem, ExifItem t1) {
            if (exifItem.tag.equalsIgnoreCase(t1.tag)){
                return 0;
            }
            return -1;
        }

        public String getString(){
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(minApi); stringBuilder.append(SEPERATOR);
            stringBuilder.append(description); stringBuilder.append(SEPERATOR);
            stringBuilder.append(tag); stringBuilder.append(SEPERATOR);
            stringBuilder.append(value); stringBuilder.append(SEPERATOR);
            stringBuilder.append(present); stringBuilder.append(SEPERATOR);
            return stringBuilder.toString();
        }

        public ExifItem(String source){
            String[] items = source.split("\\"+SEPERATOR);
            this.minApi = Integer.parseInt(items[0]);
            this.description = items[1];
            this.tag = items[2];
            this.value = items[3];
            this.present = Boolean.parseBoolean(items[4]);
        }
    }

    public ExifData(Context context, ExifInterface exifInterface){
        this.context = context;
        allowedTags = ImagepipePreferences.getAllowedTags(context);
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (android.os.Build.VERSION.SDK_INT>=24){
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_aperture),ExifInterface.TAG_APERTURE_VALUE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_maxapterture),ExifInterface.TAG_MAX_APERTURE_VALUE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_artist),ExifInterface.TAG_ARTIST);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_bps),ExifInterface.TAG_BITS_PER_SAMPLE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_brightness),ExifInterface.TAG_BRIGHTNESS_VALUE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_cfa),ExifInterface.TAG_CFA_PATTERN);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_cspace),ExifInterface.TAG_COLOR_SPACE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_compcfg),ExifInterface.TAG_COMPONENTS_CONFIGURATION);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_cbpp),ExifInterface.TAG_COMPRESSED_BITS_PER_PIXEL);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_compression),ExifInterface.TAG_COMPRESSION);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_contrast),ExifInterface.TAG_CONTRAST);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_copyright),ExifInterface.TAG_COPYRIGHT);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_cr),ExifInterface.TAG_CUSTOM_RENDERED);
        }
        if (android.os.Build.VERSION.SDK_INT>=14){
            addTagData(exifInterface,14,context.getResources().getString(R.string.exiflabel_datetime),ExifInterface.TAG_DATETIME);
        }
        if (android.os.Build.VERSION.SDK_INT>=24){
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_datetimed),ExifInterface.TAG_DATETIME_DIGITIZED);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_datetimeo),ExifInterface.TAG_DATETIME_ORIGINAL);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_dcs),ExifInterface.TAG_DEFAULT_CROP_SIZE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_ds),ExifInterface.TAG_DEVICE_SETTING_DESCRIPTION);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_digzoom),ExifInterface.TAG_DIGITAL_ZOOM_RATIO);
        }
        if (android.os.Build.VERSION.SDK_INT>=26){
            addTagData(exifInterface,26,context.getResources().getString(R.string.exiflabel_dng),ExifInterface.TAG_DNG_VERSION);
        }
        if (android.os.Build.VERSION.SDK_INT>=24){
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_exifv),ExifInterface.TAG_EXIF_VERSION);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_ebv),ExifInterface.TAG_EXPOSURE_BIAS_VALUE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_expi),ExifInterface.TAG_EXPOSURE_INDEX);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_expm),ExifInterface.TAG_EXPOSURE_MODE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_expp),ExifInterface.TAG_EXPOSURE_PROGRAM);
        }
        addTagData(exifInterface,11,context.getResources().getString(R.string.exiflabel_expt),ExifInterface.TAG_EXPOSURE_TIME);
        if (android.os.Build.VERSION.SDK_INT>=24){
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_fs),ExifInterface.TAG_FILE_SOURCE);
        }
        addTagData(exifInterface,5,context.getResources().getString(R.string.exiflabel_flash),ExifInterface.TAG_FLASH);
        if (android.os.Build.VERSION.SDK_INT>=24){
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_flashe),ExifInterface.TAG_FLASH_ENERGY);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_fpv),ExifInterface.TAG_FLASHPIX_VERSION);
        }
        addTagData(exifInterface,8,context.getResources().getString(R.string.exiflabel_focallength),ExifInterface.TAG_FOCAL_LENGTH);
        if (android.os.Build.VERSION.SDK_INT>=24) {
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_focallength35), ExifInterface.TAG_FOCAL_LENGTH_IN_35MM_FILM);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_fpru), ExifInterface.TAG_FOCAL_PLANE_RESOLUTION_UNIT);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_fpx), ExifInterface.TAG_FOCAL_PLANE_X_RESOLUTION);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_fpy), ExifInterface.TAG_FOCAL_PLANE_Y_RESOLUTION);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_fnumber), ExifInterface.TAG_F_NUMBER);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gaincontrol), ExifInterface.TAG_GAIN_CONTROL);
        }
        addTagData(exifInterface,9,context.getResources().getString(R.string.exiflabel_gps_altitude),ExifInterface.TAG_GPS_ALTITUDE);
        addTagData(exifInterface,5,context.getResources().getString(R.string.exiflabel_gps_longitude),ExifInterface.TAG_GPS_LONGITUDE);
        addTagData(exifInterface,5,context.getResources().getString(R.string.exiflabel_gps_latitude),ExifInterface.TAG_GPS_LATITUDE);
        addTagData(exifInterface,9,context.getResources().getString(R.string.exiflabel_gps_latitude_r),ExifInterface.TAG_GPS_DEST_LATITUDE_REF);
        addTagData(exifInterface,9,context.getResources().getString(R.string.exiflabel_gps_altitude_r),ExifInterface.TAG_GPS_ALTITUDE_REF);
        if (android.os.Build.VERSION.SDK_INT>=24) {
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_areainfo), ExifInterface.TAG_GPS_AREA_INFORMATION);
        }
        addTagData(exifInterface,8,context.getResources().getString(R.string.exiflabel_gps_datestamp),ExifInterface.TAG_GPS_DATESTAMP);
        if (android.os.Build.VERSION.SDK_INT>=24) {
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_bearing), ExifInterface.TAG_GPS_DEST_BEARING);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_bearingref), ExifInterface.TAG_GPS_DEST_BEARING_REF);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_distance), ExifInterface.TAG_GPS_DEST_DISTANCE);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_distanceref), ExifInterface.TAG_GPS_DEST_DISTANCE_REF);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_destlat), ExifInterface.TAG_GPS_DEST_LATITUDE);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_destlatref), ExifInterface.TAG_GPS_DEST_LATITUDE_REF);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_destlong), ExifInterface.TAG_GPS_DEST_LONGITUDE);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_destlongref), ExifInterface.TAG_GPS_DEST_LONGITUDE_REF);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_differential), ExifInterface.TAG_GPS_DIFFERENTIAL);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_dop), ExifInterface.TAG_GPS_DOP);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_img), ExifInterface.TAG_GPS_IMG_DIRECTION);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_imgdir), ExifInterface.TAG_GPS_IMG_DIRECTION_REF);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_mapdatum), ExifInterface.TAG_GPS_MAP_DATUM);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_measuremode), ExifInterface.TAG_GPS_MEASURE_MODE);
        }
        addTagData(exifInterface,8,context.getResources().getString(R.string.exiflabel_gps_processingmethod),ExifInterface.TAG_GPS_PROCESSING_METHOD);
        if (android.os.Build.VERSION.SDK_INT>=24) {
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_satellites), ExifInterface.TAG_GPS_SATELLITES);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_speed), ExifInterface.TAG_GPS_SPEED);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_speedref), ExifInterface.TAG_GPS_SPEED_REF);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_gps_status), ExifInterface.TAG_GPS_STATUS);
        }
        addTagData(exifInterface,8,context.getResources().getString(R.string.exiflabel_gps_timestamp),ExifInterface.TAG_GPS_TIMESTAMP);
        if (android.os.Build.VERSION.SDK_INT>=24){
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_gps_track),ExifInterface.TAG_GPS_TRACK);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_gps_trackref),ExifInterface.TAG_GPS_TRACK_REF);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_gps_versionid),ExifInterface.TAG_GPS_VERSION_ID);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_image_description),ExifInterface.TAG_IMAGE_DESCRIPTION);
        }
        addTagData(exifInterface,5,context.getResources().getString(R.string.exiflabel_image_length),ExifInterface.TAG_IMAGE_LENGTH);
        if (android.os.Build.VERSION.SDK_INT>=24) {
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_image_unique_id), ExifInterface.TAG_IMAGE_UNIQUE_ID);
        }
        addTagData(exifInterface,5,context.getResources().getString(R.string.exiflabel_image_width),ExifInterface.TAG_IMAGE_WIDTH);
        if (android.os.Build.VERSION.SDK_INT>=24) {
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_interop_index), ExifInterface.TAG_INTEROPERABILITY_INDEX);
            //addTagData(exifInterface, 11, context.getResources().getString(R.string.exiflabel_iso), ExifInterface.TAG_ISO);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_iso), ExifInterface.TAG_ISO_SPEED_RATINGS);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_jpegformat), ExifInterface.TAG_JPEG_INTERCHANGE_FORMAT);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_jpegformat_length), ExifInterface.TAG_JPEG_INTERCHANGE_FORMAT_LENGTH);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_ls), ExifInterface.TAG_LIGHT_SOURCE);
        }
        addTagData(exifInterface,5,context.getResources().getString(R.string.exiflabel_make),ExifInterface.TAG_MAKE);
        if (android.os.Build.VERSION.SDK_INT>=24) {
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_makernote), ExifInterface.TAG_MAKER_NOTE);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_max_aperture_value), ExifInterface.TAG_MAX_APERTURE_VALUE);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_metering_mode), ExifInterface.TAG_METERING_MODE);
        }
        addTagData(exifInterface,5,context.getResources().getString(R.string.exiflabel_model),ExifInterface.TAG_MODEL);
        if (android.os.Build.VERSION.SDK_INT>=26) {
            addTagData(exifInterface, 26, context.getResources().getString(R.string.exiflabel_new_subfile_type), ExifInterface.TAG_NEW_SUBFILE_TYPE);
        }
        if (android.os.Build.VERSION.SDK_INT>=24) {
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_oecf), ExifInterface.TAG_OECF);
        }
        addTagData(exifInterface,5,context.getResources().getString(R.string.exiflabel_orientation),ExifInterface.TAG_ORIENTATION);
        if (android.os.Build.VERSION.SDK_INT>=24){
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_pi),ExifInterface.TAG_PHOTOMETRIC_INTERPRETATION);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_pixel_x_dim),ExifInterface.TAG_PIXEL_X_DIMENSION);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_pixel_y_dim),ExifInterface.TAG_PIXEL_Y_DIMENSION);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_planarconf),ExifInterface.TAG_PLANAR_CONFIGURATION);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_prim_chromat),ExifInterface.TAG_PRIMARY_CHROMATICITIES);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_bw),ExifInterface.TAG_REFERENCE_BLACK_WHITE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_related_soundfile),ExifInterface.TAG_RELATED_SOUND_FILE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_resolution_unit),ExifInterface.TAG_RESOLUTION_UNIT);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_rps),ExifInterface.TAG_ROWS_PER_STRIP);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_samples_per_pixel),ExifInterface.TAG_SAMPLES_PER_PIXEL);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_saturation),ExifInterface.TAG_SATURATION);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_scene_capture_type),ExifInterface.TAG_SCENE_CAPTURE_TYPE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_scene_type),ExifInterface.TAG_SCENE_TYPE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_sensing_method),ExifInterface.TAG_SENSING_METHOD);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_sharpness),ExifInterface.TAG_SHARPNESS);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_shutterspeedvalue),ExifInterface.TAG_SHUTTER_SPEED_VALUE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_software),ExifInterface.TAG_SOFTWARE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_spatial_frequency_response),ExifInterface.TAG_SPATIAL_FREQUENCY_RESPONSE);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_spectral_sensitivity),ExifInterface.TAG_SPECTRAL_SENSITIVITY);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_strip_byte_counts),ExifInterface.TAG_STRIP_BYTE_COUNTS);
            addTagData(exifInterface,24,context.getResources().getString(R.string.exiflabel_strip_offsets),ExifInterface.TAG_STRIP_OFFSETS);
        }
        if (android.os.Build.VERSION.SDK_INT>=24){
            addTagData(exifInterface,26,context.getResources().getString(R.string.exiflabel_subfile_type),ExifInterface.TAG_SUBFILE_TYPE);
        }
        if (android.os.Build.VERSION.SDK_INT>=24) {
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_subject_area), ExifInterface.TAG_SUBJECT_AREA);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_subject_dist), ExifInterface.TAG_SUBJECT_DISTANCE);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_subject_distance_range), ExifInterface.TAG_SUBJECT_DISTANCE_RANGE);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_subject_loc), ExifInterface.TAG_SUBJECT_LOCATION);
        }
        if (android.os.Build.VERSION.SDK_INT>=23) {
            addTagData(exifInterface, 23, context.getResources().getString(R.string.exiflabel_subsec_time), ExifInterface.TAG_SUBSEC_TIME);
        }
        if (android.os.Build.VERSION.SDK_INT>=24) {
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_subsec_time_dig), ExifInterface.TAG_SUBSEC_TIME_DIGITIZED);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_subsec_time_orig), ExifInterface.TAG_SUBSEC_TIME_ORIGINAL);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_transfer), ExifInterface.TAG_TRANSFER_FUNCTION);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_user_comment), ExifInterface.TAG_USER_COMMENT);
        }
        addTagData(exifInterface,5,context.getResources().getString(R.string.exiflabel_white_balance),ExifInterface.TAG_WHITE_BALANCE);
        if (android.os.Build.VERSION.SDK_INT>=24) {
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_white_point), ExifInterface.TAG_WHITE_POINT);
        }
        if (android.os.Build.VERSION.SDK_INT>=24) {
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_x_resolution), ExifInterface.TAG_X_RESOLUTION);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_ycbcr_co), ExifInterface.TAG_Y_CB_CR_COEFFICIENTS);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_ycbcr_pos), ExifInterface.TAG_Y_CB_CR_POSITIONING);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_ycbcr_sub), ExifInterface.TAG_Y_CB_CR_SUB_SAMPLING);
            addTagData(exifInterface, 24, context.getResources().getString(R.string.exiflabel_y_resolution), ExifInterface.TAG_Y_RESOLUTION);
        }
        if (android.os.Build.VERSION.SDK_INT>=29){
            addTagData(exifInterface,29,context.getResources().getString(R.string.exiflabel_xmp),ExifInterface.TAG_XMP);
        }
        if (android.os.Build.VERSION.SDK_INT>=30){
            addTagData(exifInterface,30,context.getResources().getString(R.string.exiflabel_xmp),ExifInterface.TAG_XMP);
            addTagData(exifInterface,30,context.getResources().getString(R.string.exiflabel_thumbnail_orientation),ExifInterface.TAG_THUMBNAIL_ORIENTATION);
            addTagData(exifInterface,30,context.getResources().getString(R.string.exiflabel_offset_time_original),ExifInterface.TAG_OFFSET_TIME_ORIGINAL);
            addTagData(exifInterface,30,context.getResources().getString(R.string.exiflabel_offset_time_digitized),ExifInterface.TAG_OFFSET_TIME_DIGITIZED);
            addTagData(exifInterface,30,context.getResources().getString(R.string.exiflabel_offset_time),ExifInterface.TAG_OFFSET_TIME);
        }
    }

    public ExifData(Context context, String source, String[] allowedTags){
        this.context = context;
        this.allowedTags = allowedTags;
        this.exifItems = new ArrayList<ExifItem>();
        String[] exifItemsStrings = source.split("\\"+SEPERATOR);
        for (int i=0; i<exifItemsStrings.length; i++){
            ExifItem exifItem = new ExifItem(exifItemsStrings[i]);
            if (isAllowedTag(exifItem.tag)){
                exifItem.isAllowed = true;
            } else {
                exifItem.isAllowed = false;
            }
            this.exifItems.add(exifItem);
        }
    }

    private void addTagData(ExifInterface exifInterface, int minApi, String description, String tag){
        ExifItem exifItem = new ExifItem();
        exifItem.tag = tag;
        String attribute = null;
        if (exifInterface!=null){
            attribute= exifInterface.getAttribute(tag);
            exifItem.present = attribute != null;
        }
        exifItem.value = attribute;
        exifItem.minApi = minApi;
        exifItem.description = description;
        if (exifItems==null){
            exifItems = new ArrayList<ExifItem>();
        }
        exifItem.isAllowed=isAllowedTag(tag);
        exifItems.add(exifItem);
    }

    private boolean isAllowedTag(String tag){
        for (int i=0; i<allowedTags.length; i++){
            if (tag.equalsIgnoreCase(allowedTags[i])){
                return true;
            }
        }
        return false;
    }

    public SpannableStringBuilder getListString(){
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        for (int i=0; i<exifItems.size(); i++){
            ExifItem data = exifItems.get(i);
            if (data.present){
                int startPosition = spannableStringBuilder.length();
                if (android.os.Build.VERSION.SDK_INT >= 19){
                    spannableStringBuilder.append(data.description + ": "+ data.value + System.lineSeparator());
                } else {
                    spannableStringBuilder.append(data.description + ": "+ data.value + "\n");
                }
                if (data.isAllowed){
                    spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.CYAN),startPosition,spannableStringBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else {
                    spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.WHITE),startPosition,spannableStringBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }
        return spannableStringBuilder;
    }

    public String getAllowedListString(){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i=0; i<exifItems.size(); i++){
            ExifItem data = exifItems.get(i);
            if (data.isAllowed){
                if (android.os.Build.VERSION.SDK_INT >= 19){
                    stringBuilder.append(data.description + System.lineSeparator());
                } else {
                    stringBuilder.append(data.description + data.value + "\n");
                }
            }
        }
        return stringBuilder.toString();
    }


    public String getString(){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i=0; i<exifItems.size(); i++){
            ExifItem exifItem = exifItems.get(i);
            stringBuilder.append(exifItem.getString()); stringBuilder.append("\\"+SEPERATOR);
        }
        return stringBuilder.toString();
    }

    public void updateAllowedTags(String[] allowedTags){
        if (allowedTags==null){
            allowedTags = ImagepipePreferences.getAllowedTags(context);
        }
        this.allowedTags = allowedTags;
        for (int i=0; i<exifItems.size(); i++){
            exifItems.get(i).isAllowed = isAllowedTag(exifItems.get(i).tag);
        }
    }

    public void fillExifInterfaceWithData(ExifInterface exifInterface){
        if (exifInterface!=null){
            // update the allowed tags from settings before any insert, since they may have changed
            updateAllowedTags(null);
            Log.v("Adding exif tags:");
            for (int i=0; i<exifItems.size(); i++){
                ExifItem exifItem = exifItems.get(i);
                if (exifItem.isAllowed) {
                    // do not add empty values
                    if (!exifItem.value.equalsIgnoreCase("null")){
                        Log.v("-> TAG: "+exifItem.tag+", VALUE: "+exifItem.value);
                        exifInterface.setAttribute(exifItem.tag,exifItem.value);
                    }
                }
            }
        }
    }

}
