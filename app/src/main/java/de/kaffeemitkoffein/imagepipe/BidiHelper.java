/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.Context;
import android.view.View;

import java.util.Locale;

public class BidiHelper {

    private Context context;
    private boolean isRTLLanguage;

    public BidiHelper(Context context){
        this.context = context;
        isRTLLanguage = checkIsRTL();
    }

    public boolean isRTL(){
        return this.isRTLLanguage;
    }

    private boolean checkIsRTL(){
        if (android.os.Build.VERSION.SDK_INT>16){
            int direction = context.getResources().getConfiguration().getLayoutDirection();
            if (direction == View.LAYOUT_DIRECTION_RTL){
                return true;
            } else {
                return false;
            }
        } else {
            /*
            Only sane way for api below 17 is a hard-coded RTL language list:

            Most common RTL languages
            Language    ISO 639-1   ISO 639-2
            =================================
            Arabic 	    ar
            Aramaic     -	        sam, syr, arc
            Azeri 	    az
            Maldivian   dv
            Fula	    ff
            Hebrew	    he
            Kurdish	    ku
            N'Ko	    -	        nqo
            Persian	    fa
            Syriac	    -	        syc
            Urdu	    ur
            Hausa       ha
            Kashmiri    ks
            Pashto      ps
            Yiddish     yi
             */
            String[] RTL2Codes = new String[]{"ar", "vz", "dv", "ff", "he", "ku", "fa", "ur", "ha", "ks", "ps", "yi"};
            String[] RTL3Codes = new String[]{"sam","syr","arc","nqo","syc"};
            Locale locale = Locale.getDefault();
            if (!locale.getLanguage().equals("")){
                for (int i=0; i<RTL2Codes.length; i++){
                    if (locale.getLanguage().equals(new Locale(RTL2Codes[i]).getLanguage())){
                        return true;
                    }
                }
            } else {
                for (int i=0; i<RTL3Codes.length; i++){
                    if (locale.getISO3Language().equals(new Locale(RTL3Codes[i]).getLanguage())){
                        return true;
                    }
                }
            }
            return false;
        }
    }

    public String removeColon(String s){
        return s.replace(":","");
    }

    public  String getPair(String label, String value){
        if (isRTLLanguage){
            return value + " :" + removeColon(label);
        } else {
            return removeColon(label) + ": " + value;
        }
    }

}
