/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.Context;
import android.content.Intent;
import android.graphics.*;
import java.util.*;

public class ImageFilters {

    private ImageFilters(){

    }

    public static Bitmap applyColorMatrix(Bitmap source, ColorMatrix colorMatrix){
        Bitmap target = Bitmap.createBitmap(source.getWidth(),source.getHeight(), Bitmap.Config.ARGB_8888);
        ColorMatrixColorFilter colorMatrixColorFilter = new ColorMatrixColorFilter(colorMatrix);
        Canvas targetCanvas = new Canvas(target);
        Paint paint = new Paint();
        paint.setColorFilter(colorMatrixColorFilter);
        targetCanvas.drawBitmap(source,0,0,paint);
        return target;
    }


    public static final class FilerType{
        public final static int NONE = 0;
        public final static int GREYSCALE = 1;
        public final static int SEPIA = 2;
        public final static int SEPIALIGHT = 3;
        public final static int SEPIA1960 = 4;
        public final static int SEPIACOLD = 5;
        public final static int INVERT = 6;
        public final static int MYSTIFY = 7;
        public final static int DAWN = 8;
        public final static int DITHER1 = 9;
        public final static int DITHER2 = 10;
        public final static int DITHER3 = 11;
        public final static int DITHER4 = 12;
        public final static int DITHER5 = 13;
        public final static int HALFTONE = 14;
        public final static int RETRO1 = 15;
        public final static int RETRO2 = 16;
        public final static int RETRO3 = 17;
    }

    public static Bitmap grayScaleBitmap(Bitmap source){
        if (source!=null){
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.setSaturation(0); // to grayscale
            return applyColorMatrix(source,colorMatrix);
        }
        return null;
    }

    /*
                        R   G   B   A
             Matrix:    1   0   0   0   0       red
                        0   1   0   0   0       green
                        0   0   1   0   0       blue
                        0   0   0   1   0       alpha
    */

    final static float[] sepiaMatrix =     {0.500f, 0.650f, 0.150f, 0, 0,
                                            0.380f, 0.570f, 0.040f, 0, 0,
                                            0.250f, 0.500f, 0.002f, 0, 0,
                                            0.000f, 0.000f, 0.000f,1f, 0};

    final static float[] sepiaColdMatrix = {0.300f, 0.570f, 0.050f, 0, 0,
                                            0.450f, 0.700f, 0.030f, 0, 0,
                                            0.500f, 0.800f, 0.020f, 0, 0,
                                            0.000f, 0.000f, 0.000f,1f, 0};


    final static float[] sepiaLightMatrix= {1f, 0, 0    , 0, 0,
                                             0 ,1f, 0    , 0, 0,
                                             0 , 0, 0.83f, 0, 0,
                                             0,  0,     0,1f, 0};

    final static float[] sepia1960Matrix=  {0.65f, 0.3f, 0.3f, 0, 0,
                                            0 ,0.6f, 0.6f, 0, 0,
                                            0 , 0, 0.80f, 0, 0,
                                            0,  0,     0,1f, 0};

    final static float[] mystifyMatrix =   {1.000f, 0.000f, -0.50f, 0, 0,
                                            0.000f, 1.000f, -0.60f, 0, 0,
                                            0.000f, 0.000f, 1.000f, 0, 0,
                                            0.000f, 0.000f, 0.000f,1f, 0};

    final static float[] dawnMatrix =      {0.500f, 0.600f, 0.100f, 0, 0,
                                            0.000f, 1.000f, 0.000f, 0, 0,
                                            0.000f, 0.200f, 1.000f, 0, 0,
                                            0.000f, 0.000f, 0.000f,1f, 0};

    final static float[] nullMatrix =      {0.000f, 0.000f, 0.000f, 0, 0,
                                            0.000f, 0.000f, 0.000f, 0, 0,
                                            0.000f, 0.000f, 0.000f, 0, 0,
                                            0.000f, 0.000f, 0.000f,1f, 0};

    final static float[] invertColorMatrix =
                                           {-1f,0 , 0 , 0, 255f,
                                            0  ,-1f, 0, 0, 255f,
                                            0, 0, -1f,0, 255f,
                                            0, 0, 0,1f, 0};

    public class Subcolor{
        final static int RED = 0;
        final static int GREEN = 1;
        final static int BLUE = 2;
    }

    public static int getMin(int[] values){
        int result = values[0];
        for (int i=0; i<values.length; i++){
            result = Math.min(result,values[i]);
        }
        return result;
    }

    public static int getMax(int[] values){
        int result = values[0];
        for (int i=0; i<values.length; i++){
            result = Math.max(result,values[i]);
        }
        return result;
    }

    public static void swap(int[] array, int pos1, int pos2){
        int t = array[pos1]; array[pos1] = array[pos2]; array[pos2] = t;
    }

    public static int[] getBitmapArray(Bitmap bitmap){
        int[] resultArray = new int[bitmap.getWidth()*bitmap.getHeight()];
        bitmap.getPixels(resultArray,0,bitmap.getWidth(),0,0,bitmap.getWidth(),bitmap.getHeight());
        return resultArray;
    }

    public static Bitmap putBitmapArray(int[] pixels, int width, int height){
        Bitmap newBitmap = Bitmap.createBitmap(pixels,width,height, Bitmap.Config.ARGB_8888);
        Bitmap bitmapMutable = newBitmap.copy(Bitmap.Config.ARGB_8888,true);
        return bitmapMutable;

    }

    public static int[] getRedValues(int[] pixels, int start, int end){
        int[] subcolor = new int[end-start];
        for (int i=0; i<end-start; i++){
            subcolor[i] = Color.red(pixels[start+i]);
        }
        return subcolor;
    }

    public static int[] getGreenValues(int[] pixels, int start, int end){
        int[] subcolor = new int[end-start];
        for (int i=0; i<end-start; i++){
            subcolor[i] = Color.green(pixels[start+i]);
        }
        return subcolor;
    }

    public static int[] getBlueValues(int[] pixels, int start, int end){
        int[] subcolor = new int[end-start];
        for (int i=0; i<end-start; i++){
            subcolor[i] = Color.blue(pixels[start+i]);
        }
        return subcolor;
    }

    public static int averageSubColor(int[] subcolor){
        long sum = 0;
        for (int i=0; i<subcolor.length; i++){
            sum = sum + subcolor[i];
        }
        int result = (int)  (sum / subcolor.length);
        return result;
    }

    public static int averageColor(int[] pixels, int start, int end){
        long sumRed=0; long sumGreen=0; long sumBlue=0;
        for (int i=0; i<end-start; i++){
            sumRed = sumRed + Color.red(pixels[start+i]);
            sumGreen = sumGreen + Color.green(pixels[start+i]);
            sumBlue = sumBlue + Color.blue(pixels[start+i]);
        }
        int red = (int) sumRed/(end-start);
        int green = (int) sumGreen/(end-start);
        int blue = (int) sumBlue/(end-start);
        int averageColor = Color.rgb(red,green,blue);
        return averageColor;
    }

    public static int partitionBySubColor(int[] pixels, int subcolor, int start, int end){
        int pivot = getSubcolor(pixels[(end-start)/2 + start],subcolor);
        int leftIndex = start - 1;
        int rightIndex = end + 1;
        while (true) {
            do {
                leftIndex++;
            } while (getSubcolor(pixels[leftIndex],subcolor)<pivot);
            do {
                rightIndex--;
            } while (getSubcolor(pixels[rightIndex],subcolor)>pivot);
            if (leftIndex>=rightIndex){
                return rightIndex;
            }
            int i=pixels[leftIndex]; pixels[leftIndex] = pixels[rightIndex]; pixels[rightIndex] = i;
        }
    }

    public static void quicksortBySubColor(int[] pixels, int subcolor, int start, int end){
        if (start<end){
            int pivot = partitionBySubColor(pixels,subcolor,start,end);
            quicksortBySubColor(pixels,subcolor,start,pivot);
            quicksortBySubColor(pixels,subcolor,pivot+1,end);
        }
    }

    public static int getSubcolor(int color, int subcolorKey){
        if (subcolorKey==Subcolor.RED){
            return Color.red(color);
        } else if (subcolorKey==Subcolor.GREEN) {
            return Color.green(color);
        }
        return Color.blue(color);
    }

    public static int partition(int[] values, int start, int end){
        int pivot = values[(end-start)/2 + start];
        int leftIndex = start-1;
        int rightIndex = end+1;
        while (true) {
            do {
                leftIndex++;
            } while (values[leftIndex]<pivot);
            do {
                rightIndex--;
            } while (values[rightIndex]>pivot);
            if (leftIndex>=rightIndex){
                return rightIndex;
            }
            int i=values[leftIndex]; values[leftIndex] = values[rightIndex]; values[rightIndex] = i;
        }
    }

    public static void quicksort(int[] values, int start, int end){
        if (start<end){
            int p = partition(values,start,end);
            quicksort(values,start,p);
            quicksort(values,p+1,end);
        }
    }

    public static void medianCut(int[] pixels, int start, int end, int depth, int targetDepth, ArrayList<Integer> resultPalette, Context context){
        updateProgressBar(context,Math.round((float) depth/targetDepth*100f));
        if (depth>=targetDepth){
            // as target depth is reached, average the pixels in the bucket
            int averageColor = averageColor(pixels,start,end);
            // add the color to the palette
            resultPalette.add(averageColor);
        } else {
            // split into buckets
            // first, find the subcolor with the highest range
            int[] subcolorRed   = getRedValues(pixels,start,end);
            int[] subcolorGreen = getGreenValues(pixels,start,end);
            int[] subcolorBlue  = getBlueValues(pixels,start,end);
            int rangeRed   = getMax(subcolorRed) - getMin(subcolorRed);
            int rangeGreen = getMax(subcolorGreen) - getMin(subcolorGreen);
            int rangeBlue  = getMax(subcolorBlue) - getMin(subcolorBlue);
            int largestRangeSpace = Subcolor.RED; // red has largest range by default
            // sort pixels by the subcolor with the highest range
            if ((rangeGreen >= rangeRed) && (rangeGreen >= rangeBlue)){
                // green has largest range
                largestRangeSpace = Subcolor.GREEN;
                quicksortBySubColor(pixels,largestRangeSpace,start,end);
            } else {
                if ((rangeBlue >= rangeRed) && (rangeBlue >= rangeGreen)){
                    // blue has largest range
                    largestRangeSpace = Subcolor.BLUE;
                    quicksortBySubColor(pixels,largestRangeSpace,start,end);
                } else {
                    // red has the largest range
                    quicksortBySubColor(pixels,largestRangeSpace,start,end);
                }
            }
            // divide into two buckets by the median (=center position)
            int median = start+(end-start)/2;
            medianCut(pixels,start,median,depth+1,targetDepth,resultPalette,context);
            medianCut(pixels,median+1,end,depth+1,targetDepth,resultPalette,context);
        }
    }

    public static int colorDistance(int color1, int color2){
        return Math.abs(Color.red(color1) - Color.red(color2)) +
                Math.abs(Color.green(color1) - Color.green(color2)) +
                Math.abs(Color.blue(color1) - Math.abs(Color.blue(color2)));
    }

    public static int colorDistance3D(int color1, int color2){
        return (int) Math.round(Math.sqrt(Math.pow(Color.red(color1) - Color.red(color2),2) +
                Math.pow(Color.green(color1) - Color.green(color2),2) +
                Math.pow(Color.blue(color1) - Color.blue(color2),2)));
    }

    public static int findClosestColor(ArrayList<Integer> palette, int color){
        int r = palette.get(0);
        int d = colorDistance(r,color);
        for (int i=1; i<palette.size(); i++){
            int c = palette.get(i);
            int distance = colorDistance(c,color);
            if (distance<d){
                d=distance; r=c;
            }
        }
        return r;
    }

    public static Bitmap reduceColors(Bitmap source, ArrayList<Integer> palette){
      int[] pixels = getBitmapArray(source);
      for (int i=0; i<pixels.length; i++){
          pixels[i] = findClosestColor(palette,pixels[i]);
      }
      return putBitmapArray(pixels,source.getWidth(),source.getHeight());
    };

    public static Bitmap getReducedPaletteBitmap(Bitmap source, int depth, Context context){
        updateProgressBar(context,0);
        ArrayList<Integer> resultPalette = new ArrayList<Integer>();
        int[] pixels = getBitmapArray(source);
        medianCut(pixels,0,pixels.length-1,0,depth,resultPalette,context);
        Bitmap resultBitmap = dither(source,resultPalette,context);
        if (context==null){
            drawPalette(resultBitmap,resultPalette);
        }
        return resultBitmap;
    }

    public static void drawPalette(Bitmap bitmap, final ArrayList<Integer> palette){
        final int paletteWidth = bitmap.getWidth()/2;
        final int paletteHeight = bitmap.getHeight()/10;
        float step = (float) paletteWidth/palette.size();
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(1f);
        canvas.drawRect(0, bitmap.getHeight(), paletteWidth+1, bitmap.getHeight()-paletteHeight-1, paint);
        paint.setStyle(Paint.Style.FILL);
        for (int i=0; i<palette.size(); i++) {
            paint.setColor(palette.get(i));
            //canvas.drawRect(i * step, 0, (i + 1) * step, paletteHeight, paint);
            canvas.drawRect(i * step, bitmap.getHeight(), (i + 1) * step, bitmap.getHeight()-paletteHeight, paint);
        }
    }

    public static int[] PALETTE16 = {0xff000000, 0xff0000aa, 0xff00aa00, 0xff00aaaa, 0xffaa0000, 0xffaa00aa, 0xffaa5500,
            0xaaaaaa, 0xff555555, 0xff5555ff, 0xff55ff55, 0xff55ffff, 0xffff5555, 0xffff55ff, 0xffffff55, 0xffffffff};

    public static int[] PALETTE_RETRO1 = {0xff000000, 0xff00aa00, 0xffaa0000, 0xffaa5500};
    public static int[] PALETTE_RETRO2 = {0xff000000, 0xff55ff55, 0xffff5555, 0xffffff55};
    public static int[] PALETTE_RETRO3 = {0xff000000, 0xff55ffff, 0xffff55ff, 0xffffffff};

    public static Bitmap getFixedPaletteBitmap(Context context, Bitmap source, int[] palette){
        ArrayList<Integer> resultPalette = new ArrayList<Integer>();
        for (int i=0; i<palette.length; i++){
            resultPalette.add(palette[i]);
        }
        Bitmap resultBitmap = dither(source,resultPalette,context);
        if (context==null){
            drawPalette(resultBitmap,resultPalette);
        }
        return resultBitmap;
    }

    private static void applyError(Bitmap source, int x, int y, int errorR, int errorG, int errorB){
        if ((x>=0) && (x<source.getWidth()) && (y>=0) && (y<source.getHeight())){
            int p = source.getPixel(x,y);
            int r = Color.red(p); int g = Color.green(p); int b = Color.blue(p);
            int tR = r+errorR; int tG = g+errorG; int tB = b+errorB;
            if (tR>255){
                tR=255;
            }
            if (tR<0){
                tR=0;
            }
            if (tG>255){
                tG=255;
            }
            if (tG<0){
                tG=0;
            }
            if (tB>255){
                tB=255;
            }
            if (tB<0){
                tB=0;
            }
            //source.setPixel(x,y,Color.rgb(r+errorR,g+errorG,b+errorB));
            source.setPixel(x,y,Color.rgb(tR,tG,tB));
        }
    }

    public static Bitmap dither(Bitmap source, ArrayList<Integer> palette, Context context){
        int height = source.getHeight();
        for (int y=0; y<source.getHeight(); y++){
            for (int x=0; x<source.getWidth(); x++){
                int pixel = source.getPixel(x,y);
                int closestColor = findClosestColor(palette,pixel);
                source.setPixel(x,y,closestColor);
                int r  = Color.red(pixel); int g = Color.green(pixel); int b = Color.blue(pixel);
                int r2 = Color.red(closestColor); int g2 = Color.green(closestColor); int b2 = Color.blue(closestColor);
                int errorRed = r-r2; int errorGreen = g-g2; int errorBlue = b-b2;
                applyError(source,x + 1,y + 0, Math.round(7f/16*errorRed),Math.round(7f/16*errorGreen),Math.round(7f/16*errorBlue));
                applyError(source,x - 1,y + 1, Math.round(3f/16*errorRed),Math.round(3f/16*errorGreen),Math.round(3f/16*errorBlue));
                applyError(source,x + 0,y + 1, Math.round(5f/16*errorRed),Math.round(5f/16*errorGreen),Math.round(5f/16*errorBlue));
                applyError(source,x + 1,y + 1, Math.round(1f/16*errorRed),Math.round(1f/16*errorGreen),Math.round(1f/16*errorBlue));
            }
            //updateProgressBar(context,Math.round(height/50f*y)+50);
            updateProgressBar(context,Math.round(100f/height*y));
        }
        return source;
    }

    private static int getColorAverage(int color){
        return (Color.red(color) + Color.green(color) + Color.blue(color))/3;
    }

    public static Bitmap halftone(Bitmap source, Context context){
        //Bitmap target = source.copy(Bitmap.Config.ARGB_8888,true);
        Bitmap target = Bitmap.createBitmap(source.getWidth(),source.getHeight(), Bitmap.Config.ARGB_8888);
        int height = source.getHeight();
        int targetColor = Color.WHITE;
        int bitmapSize = source.getWidth()*source.getHeight();
        double average=0;
        for (int y=0; y<target.getHeight()-1; y++) {
            for (int x=0; x<target.getWidth()-1; x++){
                average = average + ((double) getColorAverage(source.getPixel(x,y)))/bitmapSize;
            }
        }
        int threshold=(int) Math.round(average/2);
        for (int y=0; y<target.getHeight()-1; y=y+2){
            for (int x=0; x<target.getWidth()-1; x=x+2){
                int sum = (getColorAverage(source.getPixel(x,y))+
                        getColorAverage(source.getPixel(x+1,y)) +
                        getColorAverage(source.getPixel(x,y+1)) +
                        getColorAverage(source.getPixel(x+1,y+1)))/4;
                int error = sum;
                if (sum>=threshold){
                    target.setPixel(x,y+1,targetColor);
                    error = error - threshold;
                }
                if (sum>=threshold*2){
                    target.setPixel(x+1,y,targetColor);
                    error = error - threshold;
                }
                if (sum>=threshold*3){
                    target.setPixel(x+1,y+1,targetColor);
                    error = error - threshold;
                }
                if (sum>=threshold*4){
                    target.setPixel(x,y+1,targetColor);
                    error = error - threshold;
                }
            }
            updateProgressBar(context,Math.round(height/100f*y));
        }
        return target;
    }

    public static void applyHalftoneError(Bitmap bitmap, int x, int y, int error){
        if ((x>=0) && (y>=0) && (x<bitmap.getWidth()) && (y<bitmap.getHeight())){
            int p = bitmap.getPixel(x,y);
            int r = Color.red(p); int g = Color.green(p); int b = Color.blue(p);
            int newValueRed   = r + error;
            int newValueGreen = g + error;
            int newValueBlue  = b + error;
            if (newValueRed>255){
                newValueRed=255;
            }
            if (newValueGreen>255){
                newValueGreen=255;
            }
            if (newValueBlue>255){
                newValueBlue=255;
            }
            bitmap.setPixel(x,y,Color.rgb(newValueRed,newValueGreen,newValueBlue));
        }
    }

    public static void applyHalftoneError4(Bitmap bitmap, int x, int y, int error){
        applyHalftoneError(bitmap,x,y,error);
        applyHalftoneError(bitmap,x+1,y,error);
        applyHalftoneError(bitmap,x,y+1,error);
        applyHalftoneError(bitmap,x+1,y+1,error);
    }

    public static void updateProgressBar(Context context, int progress){
        if (context!=null){
            Intent intent = new Intent();
            intent.setAction(ImageReceiver.ACTION_UPDATE_PROGRESS);
            intent.putExtra(ImageReceiver.EXTRA_PROGRESS,progress);
            context.sendBroadcast(intent);
        }
    }

    public static Bitmap applyColorFilter(Context context, Bitmap source, int filter){
        ColorMatrix colorMatrix = null;
        if (filter==FilerType.NONE){
            return source.copy(Bitmap.Config.ARGB_8888,true);
        }
        if (filter==FilerType.GREYSCALE){
            return grayScaleBitmap(source);
        }
        if (filter==FilerType.SEPIA){
            colorMatrix = new ColorMatrix(sepiaMatrix);
        }
        if (filter==FilerType.SEPIALIGHT){
            colorMatrix = new ColorMatrix(sepiaLightMatrix);
        }
        if (filter==FilerType.SEPIA1960){
            colorMatrix = new ColorMatrix(sepia1960Matrix);
        }
        if (filter==FilerType.SEPIACOLD){
            colorMatrix = new ColorMatrix(sepiaColdMatrix);
        }
        if (filter==FilerType.INVERT){
            colorMatrix = new ColorMatrix(invertColorMatrix);
        }
        if (filter==FilerType.MYSTIFY){
            colorMatrix = new ColorMatrix(mystifyMatrix);
        }
        if (filter==FilerType.DAWN){
            colorMatrix = new ColorMatrix(dawnMatrix);
        }
        if (filter==FilerType.DITHER1){
            return getReducedPaletteBitmap(source,1,context);
        }
        if (filter==FilerType.DITHER2){
            return getReducedPaletteBitmap(source,4,context);
        }
        if (filter==FilerType.DITHER3){
            return getReducedPaletteBitmap(source,6,context);
        }
        if (filter==FilerType.DITHER4){
            return getReducedPaletteBitmap(source,8,context);
        }
        if (filter==FilerType.DITHER5){
            return getFixedPaletteBitmap(context,source,PALETTE16);
        }
        if (filter==FilerType.RETRO1){
            return getFixedPaletteBitmap(context,source,PALETTE_RETRO1);
        }
        if (filter==FilerType.RETRO2){
            return getFixedPaletteBitmap(context,source,PALETTE_RETRO2);
        }
        if (filter==FilerType.RETRO3){
            return getFixedPaletteBitmap(context,source,PALETTE_RETRO3);
        }
        if (filter==FilerType.HALFTONE){
            return halftone(source,context);
        }
        if (colorMatrix!=null){
            return applyColorMatrix(source,colorMatrix);
        }
        return null;
    }

}
