/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.Context;
import android.graphics.Color;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ColorPreference extends Preference {

    private Context context;

    public ColorPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    public ColorPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    private int prefColor = ImagepipePreferences.getReplaceColor(getContext());

    @Override
    protected View onCreateView(ViewGroup parent) {
        setLayoutResource(R.layout.colorpreference);
        View view = super.onCreateView(parent);
        ImageView imageView1 = (ImageView) view.findViewById(R.id.preferenceColor);
        TextView featureDisabledTextview = view.findViewById(R.id.currentColorTextDisabled);
        ColorPicker.drawButton(context,imageView1,null,prefColor);
        if (!isEnabled()){
            TextView textView1 = (TextView) view.findViewById(android.R.id.title);
            textView1.setTextColor(Color.GRAY);
            TextView textView2 = (TextView) view.findViewById(android.R.id.summary);
            textView2.setTextColor(Color.GRAY);
            TextView textView3 = (TextView) view.findViewById(R.id.currentColorText);
            textView3.setTextColor(Color.GRAY);
            imageView1.setAlpha(0.5f);
        }
        if (prefColor==Color.TRANSPARENT){
            featureDisabledTextview.setVisibility(View.VISIBLE);
        } else {
            featureDisabledTextview.setVisibility(View.INVISIBLE);
        }
        return view;
    }

    public void setColor(int color){
        prefColor = color;
        notifyChanged();
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
    }

}
