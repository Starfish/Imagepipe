/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.kaffeemitkoffein.imagepipe;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.*;
import android.content.*;
import android.graphics.*;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.os.*;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import java.io.*;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import android.Manifest;
import android.content.pm.PackageManager;

/*
 * This class provides all the relevant functions and interaction for Imagepipe.
 */

public class ImageReceiver extends Activity{

    /*
     * Constants for callbacks after permissions were granted.
     * The two functions need the read/write to SD storage permission:
     * - select image from gallery
     * - pipe the image
     * - save image to disk
     *
     * missing_permissions_task holds the call type that has to be made once
     * permissions are granted.
     * */

    private static final int PERMISSION_CALLBACK = 98;

    private static final int MISSING_PERMISSIONS_TASK_sendImageUriIntent = 1;
    private static final int MISSING_PERMISSIONS_TASK_pipeTheImage = 2;
    private static final int MISSING_PERMISSIONS_TASK_bulkpipe = 4;
    private static final int MISSING_PERMISSIONS_TASK_saveImage = 3;

    private int missing_permissions_task;

    /*
     * Callback constant to identify the select from gallery task.
     */

    public static final int SELECT_FROM_GALLERY_CALLBACK=99;
    public static final int SELECT_FROM_GALLERY_GET_CONTENT_CALLBACK=100;

    private static final int BUBBLE_RUN_DELAY = 3500;

    private static final int HINT_COUNT = 2;

    /*
     Clear data callback identifier
     */

    public static String ACTION_CLEAR_DATA = "ACTION_CLEAR_DATA";
    public static String ACTION_UPDATE_PROGRESS = "ACTION_UPDATE_PROGRESS";
    public static String ACTION_SHOW_PROGRESS = "ACTION_HIDE_PROGRESS";
    public static String ACTION_HIDE_PROGRESS = "ACTION_HIDE_PROGRESS";
    public static String EXTRA_PROGRESS = "EXTRA_PROGRESS";

    /*
     * Saved instance-state constants.
     */

    private static final String SIS_CUT_LINE_TOP="SIS_CUT_LINE_TOP";
    private static final String SIS_CUT_LINE_BOTTOM="SIS_CUT_LINE_BOTTOM";
    private static final String SIS_CUT_LINE_LEFT="SIS_CUT_LINE_LEFT";
    private static final String SIS_CUT_LINE_RIGHT="SIS_CUT_LINE_RIGHT";
    private static final String SIS_SCALE_RATIO="SIS_SCALE_RATIO";
    private static final String SIS_IMAGE_NEEDS_TO_BE_SAVED ="SIS_IMAGE_NEEDS_TO_BE_SAVED";
    private static final String SIS_SAVEDIMAGE_URI="SIS_SAVEDIMAGE_URI";
    private static final String SIS_ORIGINALIMAGE_URI="SIS_ORIGINALIMAGE_URI";
    private static final String SIS_NO_IMAGE_LOADED="SIS_NO_IMAGE_LOADED";
    private static final String SIS_JPEG_FILESIZE="SIS_JPEG_FILESIZE";
    private static final String SIS_ORIGINAL_FILESIZE="SIS_ORIGINAL_FILESIZE";
    private static final String SIS_PIPING_ALREADY_LAUNCHED="SIS_PIPING_ALREADY_LAUNCHED";
    private static final String SIS_CACHE_POSITION="SIS_CACHE_POSITION";
    private static final String SIS_PAINTTOOL="SIS_PAINTTOOL";
    private static final String SIS_BRUSHSIZE="SIS_BRUSHSIZE";
    private static final String SIS_SELECTEDCOLOR="SIS_SELECTEDCOLOR";
    private static final String SIS_CHARSEQUENCE="SIS_CHARSEQUENCE";
    private static final String SIS_TEXTSTYLE_BOLD="SIS_TEXTSTYLE_BOLD";
    private static final String SIS_TEXTSTYLE_ITALIC="SIS_TEXTSTYLE_ITALIC";
    private static final String SIS_TEXTSTYLE_UNDERLINE="SIS_TEXTSTYLE_UNDERLINE";
    private static final String SIS_TEXTSTYLE_STRIKETHROUGH="SIS_TEXTSTYLE_STRIKETHROUGH";
    private static final String SIS_PREF_NUMBERING="SIS_NUMBERING";

    /*
     * File provider authority name. This must be identical as stated in AndroidManifest.xml.
     */

    public final static String AUTHORITY="imageprovider";

    /*
     * Error codes.
     */

    private static final int ERROR_NO_IMAGE_APP = 1;
    private static final int ERROR_NO_BULK_IMAGE_APP = 2;

    private static final String DATA_TITLE="DATA_TITLE";
    private static final String DATA_TEXTRESOURCE="DATA_TEXTRESOURCE";
    private static final String DATA_BUTTONTEXT="DATA_BUTTONTEXT";

    /*
     * Debug string that is displayed above the image info in runtime.
     */

    private String debug = "";

    /*
     * Static filename for file in cache. This file is used to save a stream
     * into a file to get exif data.
     */

    private static final String TEMP_FILENAME="tempimage.jpg";

    /*
     * Global exif instance for loaded image. When loading an image, a copy of the original exif data
     * is stored in the preferences and restored from there when the original uri is not available any longer.
     * This instance if ExifInterface always holds the *whole* exif data of the original.
     */

    // ExifInterface exifInterface;

    ExifData exifData;

    /*
     * Variables that say if permissons are available.
     */

    private Boolean hasReadStoragePermission = false;
    private Boolean hasWriteStoragePermission = false;

    /*
     * The key bitmaps the program works with. Concept is:
     * Bitmap image : the visible bitmap with quality preview
     * Bitmap scaled: non-visible backup of the scaled bitmap, but without quality loss. This is
     *                used to calculate "image".
     * Bitmap original: this is a backup of the loaded image. It is "autoscaled" and
     *                  "autorotated".
     */

    /*
     * bitmap_visible  = the visible bitmap with applied quality preview
     * bitmap_image    = the image to be modified / painted on
     * bitmap_original = optinal bitmap holding original image, autoscaled and autorotated. May
     *                   be null in low memory conditions.
     */

    private Bitmap bitmap_visible = null;
    private Bitmap bitmap_image = null;
    private Bitmap bitmap_original = null;

    /*
     * Temporary holds the received intent. This needs to be defined global
     * because it is used in async task.
     */

    private Intent call_intent;

    /*
     * Variable that holds the (main) imageview where the image is displayed.
     */

    private ImageView image_display;
    private EditText edit_size;

    /*
     * Is set true if info bubble is visible.
     */

    private final Boolean bubble_visible = false;

    /*
     * Is true if no image was loaded.
     */

    private Boolean no_image_loaded = true;

    /*
     * Indicates if there is a gallery call pending. This is needed to catch
     * simultaneous gallery calls just after permissions were granted.
     */

    private Boolean gallery_already_called = false;

    /*
     * Floats holding the cut-line boundaries globally.
     *
     * "offset_"    : offset coordinates of a gesture to calculate frame changes.
     * scale_ratio  : scale factor of the bitmap inside the imageview (image_display).
     */

    private float cut_line_top;
    private float cut_line_bottom;
    private float cut_line_left;
    private float cut_line_right;
    private float scale_ratio;

    /*
     * x_pos and x_pos hold the last touch position of the 1st touch pointer.
     * x_pos and x_pos hold the last touch position of the 2st touch pointer.
     *
     * Both are used in the cropping feature. They are global to identify movement
     * directions easier.
     */

    float x_pos;
    float y_pos;
    float x1_pos;
    float y1_pos;

    float xPaintCache;
    float yPaintCache;

    /*
     * Constants for the cropping borders.
     */

    private final static int LEFT_CUTLINE = 0;
    private final static int RIGHT_CUTLINE = 1;
    private final static int TOP_CUTLINE = 2;
    private final static int BOTTOM_CUTLINE = 3;

    /*
     * Coordinates of the "+" button visible when no image is loaded. This is populated
     * on runtime.
     */

    private float x1_choose_button;
    private float x2_choose_button;
    private float y1_choose_button;
    private float y2_choose_button;

    /*
     * Indicates if there is a change that can be saved. Prevents multiple saving of the
     * same image into a file.
     */

    private boolean image_needs_to_be_saved = true;

    /*
     * Holds the uri of the latest saved file/image.
     */

    private ImageContainer savedImageContainer;

    /*
     * Holds the uri, filesize (and optionally file) of the ORIGINAL image
     */

    private ImageContainer originalImageContainer;

    /*
     *
     */

    private ArrayList<ImageContainer> multiplePipeList;

    /*
     * Global context constant for async tasks to refer the app context.
     */

    private Context context = this;

    /*
     * Global reference to the ActionBar.
     */

    private ActionBar actionBar;

    /*
     * Filesize (preview) of the encoded jpeg file in bytes.
     * 0: no preview available / preview calculation failed.
     */

    private long jpeg_ecoded_filesize = 0;

    /*
     * Time in millis from when on the next jpeg preview is allowed to be calculated.
     * Uses millis since 1970. "0" is a failsave init that allows the next calculation
     * to be made immediately.
     *
     * On runtime, this is counted from time now + NEXT_UPDATE_TIME_DELAY
     */

    private long next_quality_update_time = 0;

    /*
     * Mandatory delay between jpeg calculations in RAM to safe resources.
     * See above.
     */

    private static final int NEXT_UPDATE_TIME_DELAY = 2000;

    private Boolean quality_update_is_dispatched = false;

    /*
     * Is true if there is a quality preview calculation already pending.
     * Prevents multiple calls.
     */
    private Boolean piping_was_already_launched = false;

    private boolean useScopedStorage = false;

    /*
     * Class to determine the selected painting tool
     */

    static class Tool {
        final static int NONE = 0;
        final static int CROP = 1;
        final static int BLUR = 2;
        final static int TEXT = 3;
        final static int PAINT = 4;
        final static int COLORPICKER = 5;
    }

    /*
     * selected tool
     */

    private int paintTool = Tool.NONE;

    /*
     * history which was the previous tool. Used for the color picker to jump back to the last tool.
     */

    private int lastPaintTool = Tool.NONE;
    private int brushSize = -1;
    private int selectedColor = Color.WHITE;
    private int cachePosition = 0;
    private CharSequence charSequence = "";
    private boolean textStyle_bold = false;
    private boolean textStyle_italic = false;
    private boolean textStyle_underline = false;
    private boolean textStyle_strikethrough = false;

    float imageScaleFactor = 1;

    /*
     * static button hooks
     */

    private ImageButton buttonCrop;
    private ImageButton buttonBlur;
    private ImageButton buttonText;
    private ImageButton buttonPaint;
    private ImageButton buttonColor;
    private ImageButton buttonColorPicker;
    private ImageButton buttonUnDo;
    private ImageButton buttonReDo;
    private ImageButton newCanvas;

    /*
     * static button frame hooks
     */

    private FrameLayout buttonCropFrame;
    private FrameLayout buttonBlurFrame;
    private FrameLayout buttonTextFrame;
    private FrameLayout buttonPaintFrame;
    private FrameLayout buttonColorFrame;
    private FrameLayout buttonColorPickerFrame;

    private boolean loadingImageIsRunning = false;

    private SeekBar seekBar;

    private ProgressBar progressBar;

    private Menu menu;

    final class Workmode {
        final static int SHARE = 0;
        final static int GET_CONTENT = 1;
        final static int EDIT = 2;
    }

    private int workmode = Workmode.SHARE;
    private int workmodeVisible = Workmode.SHARE;

    public void setWorkmodeIcon(){
        if (workmode!=workmodeVisible){
            if (menu!=null){
                try {
                    MenuItem menuItem = menu.getItem(1);
                    menuItem.setIcon(R.mipmap.ic_check_white_24dp);
                    invalidateOptionsMenu();
                } catch (IndexOutOfBoundsException e){
                    // do nothing;
                }
            }
            workmodeVisible = workmode;
        }
    }

    final static class ResizeOptions{

        final static int WIDTH = 0;
        final static int HEIGHT = 1;
        final static int MAX = 2;
        final static int MIN = 3;
        final static int PERCENT = 4;

        public static ArrayList<String> getResizeOptions(Context context){
            ArrayList<String> result = new ArrayList<String>();
            result.add(context.getResources().getString(R.string.resizeoption_0));
            result.add(context.getResources().getString(R.string.resizeoption_1));
            result.add(context.getResources().getString(R.string.resizeoption_2));
            result.add(context.getResources().getString(R.string.resizeoption_3));
            result.add(context.getResources().getString(R.string.resizeoption_4));
            return result;
        }
    }

    final AdapterView.OnItemSelectedListener scaleOptionsListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            ImagepipePreferences.setResizeMode(context,i);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            // nothing to do
        }
    };

    /*
     * This is a runnable that finishes the GET_CONTENT action by setting the result intent and terminating
     * the app. It is called at two positions:
     * - onOptionsItemSelected:
     *      when autopipe is DISABLED, ticking the check symbol finishes the GET_CONTENT action
     * - sendGetResultIntentSingle:
     *      when autopipe is ENABLED, this immediately finishes the GET_CONTENT action.
     */

    public Runnable launchAfterImageArrivedInCache = new Runnable() {
        @Override
        public void run() {
            piping_was_already_launched = true;
            // construct the result intent, send it & exit app
            Intent resultIntent = new Intent();
            Uri resultUri = ImageWriter.saveShareTempFileFromLastCachePosition(context,null,exifData);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                String[] mimeTypes = new String[1];
                mimeTypes[0]=ImageWriter.getMimeType(context);
                // this adds the uri from the FileProvider to the clipdata
                ClipData.Item clipItem = new ClipData.Item(resultUri);
                ClipData clipData = new ClipData("clipdata",mimeTypes,clipItem);
                resultIntent.setClipData(clipData);
            }
            resultIntent.setAction(Intent.ACTION_SEND);
            resultIntent.setDataAndType(resultUri,ImageWriter.getMimeType(context));
            resultIntent.putExtra(Intent.EXTRA_STREAM, resultUri);
            resultIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            setResult(Activity.RESULT_OK,resultIntent);
            finish();
        }
    };


    /*
     * static listeners
     */

    final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (action!=null){
                    if (action.equals(ACTION_CLEAR_DATA)){
                        clearImageData(true);
                    }
                }
                if (action.equals(ACTION_UPDATE_PROGRESS)){
                    int progress = intent.getIntExtra(EXTRA_PROGRESS,0);
                    int oldProgress = progressBar.getProgress();
                    if ((progress>oldProgress) || (progress<=0)){
                        progressBar.setMax(100);
                        progressBar.setProgress(progress);
                    }
                }
                if (action.equals(ACTION_HIDE_PROGRESS)){
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }
        }
    };

    private final View.OnLongClickListener cropRatioListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            showCropSelector();
            return true;
        }
    };

    private final View.OnLongClickListener brushSizeListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            showBrushSelector();
            return true;
        }
    };

    private final View.OnLongClickListener charSequenceListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            showTextSelector();
            return true;
        }
    };

    private final View.OnClickListener paletteListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showPalette();
        }
    };

    private final View.OnLongClickListener colorPickerListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            showColorPicker();
            return true;
        }
    };

    private final View.OnClickListener colorButtonListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            colorButtonPressed(view);
        }
    };


    private final View.OnClickListener unDoButtonListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            unDoLastAction();
        }
    };

    private final View.OnClickListener reDoButtonListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            reDoAction();
        }
    };

    private final View.OnClickListener newCanvasListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            createNewCanvas(false);
        }
    };

    private final View.OnLongClickListener newCanvasLongListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            createNewCanvas(true);
            return true;
        }
    };

    private final View.OnClickListener rotateRightListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!no_image_loaded){
                rotateRight();
            }
        }
    };

    private final View.OnClickListener rotateLeftListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!no_image_loaded){
                rotateLeft();
            }
        }
    };

    private final View.OnLongClickListener rotateDialogListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            rotateImageDialog();
            return true;
        }
    };

    private final View.OnClickListener flipHorizontalListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            flipImage(FLIP_HORIZONTALLY);
        }
    };

    private final View.OnLongClickListener flipVerticallyListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            flipImage(FLIP_VERTICALLY);
            return true;
        }
    };

    private final View.OnClickListener filterListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            filterImage();
        }
    };

    private final View.OnLongClickListener filterselectorListener = new View.OnLongClickListener(){

        @Override
        public boolean onLongClick(View view) {
            showFilterSelector();
            return true;
        }
    };

    private final View.OnClickListener reloadListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            reloadImage();
            updateImageDisplay();
        }
    };

    private final View.OnLongClickListener reloadNoShrinkListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            reloadNoShrink();
            return true;
        }
    };

    /*
     * Timestamp when the last modification of the image occurred
     */

    long imageLastModified = Calendar.getInstance().getTimeInMillis();

    @Override
    public void onBackPressed() {
        if ((cropToolVisible) || (textToolVisible) || (brushToolVisible) || (paletteToolVisible) || (hintBubbleVisible) || (filterToolVisible)) {
            hideToolsIfVisible();
        } else {
            super.onBackPressed();
        }
    }

    public void handleIntentOrRestoreFromInstanceState(Bundle savedInstanceState){
        call_intent = getIntent();
        String i_action = call_intent.getAction();
        String i_type = call_intent.getType();
        if (savedInstanceState != null) {
            Log.v("Restoring data from savedInstanceState");
            image_needs_to_be_saved = savedInstanceState.getBoolean(SIS_IMAGE_NEEDS_TO_BE_SAVED, true);
            // When the file name pattern has changed (user returns from preferences) we may need to save the image again.
            // This is implemented here for the case that the ImageReceiver activity is destroyed and onCreate is called
            // with an existent savedInstanceState. Other cases are handled inside the onPreferenceChangedListener,
            // and do basically the same.
            String prefNumbering = savedInstanceState.getString(SIS_PREF_NUMBERING);
            if (prefNumbering!=null){
                if (!prefNumbering.equals(ImagepipePreferences.getNumbering(context))){
                    image_needs_to_be_saved = true;
                }
            }
            no_image_loaded = savedInstanceState.getBoolean(SIS_NO_IMAGE_LOADED, true);
            Uri uriSaved = savedInstanceState.getParcelable(SIS_SAVEDIMAGE_URI);
            savedImageContainer = new ImageContainer(getApplicationContext(),uriSaved);
            jpeg_ecoded_filesize = savedInstanceState.getLong(SIS_JPEG_FILESIZE);
            Uri uriOriginal = savedInstanceState.getParcelable(SIS_ORIGINALIMAGE_URI);
            long original_image_filesize = savedInstanceState.getLong(SIS_ORIGINAL_FILESIZE);
            originalImageContainer = new ImageContainer(getApplicationContext(),uriOriginal,original_image_filesize);
            paintTool = savedInstanceState.getInt(SIS_PAINTTOOL);
            cachePosition = savedInstanceState.getInt(SIS_CACHE_POSITION);
            brushSize = savedInstanceState.getInt(SIS_BRUSHSIZE);
            selectedColor = savedInstanceState.getInt(SIS_SELECTEDCOLOR);
            charSequence = savedInstanceState.getCharSequence(SIS_CHARSEQUENCE);
            textStyle_bold = savedInstanceState.getBoolean(SIS_TEXTSTYLE_BOLD);
            textStyle_italic = savedInstanceState.getBoolean(SIS_TEXTSTYLE_ITALIC);
            textStyle_underline = savedInstanceState.getBoolean(SIS_TEXTSTYLE_UNDERLINE);
            textStyle_strikethrough = savedInstanceState.getBoolean(SIS_TEXTSTYLE_STRIKETHROUGH);
            // restore cut frame after rotation of device
            cut_line_top = savedInstanceState.getFloat(SIS_CUT_LINE_TOP);
            cut_line_bottom = savedInstanceState.getFloat(SIS_CUT_LINE_BOTTOM);
            cut_line_left = savedInstanceState.getFloat(SIS_CUT_LINE_LEFT);
            cut_line_right = savedInstanceState.getFloat(SIS_CUT_LINE_RIGHT);
            scale_ratio = savedInstanceState.getFloat(SIS_SCALE_RATIO);
            if (paintTool==Tool.CROP){
                scale_ratio = getScaleRatio();
                drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
            }
            buttonColor.setColorFilter(selectedColor,PorterDuff.Mode.SRC_ATOP);
            // restore image from cache
            if (!readImageBitmapFromCache(cachePosition)){
                // re-load if restore from cache fails
                if (savedImageContainer.uri != null) {
                    if (!readImageBitmapFromUri(savedImageContainer.uri,false)){
                        showLoadingFailedErrorToast();
                    }
                } else {
                    Log.v("Restored image from uri");
                }
                getSavedInstanceStateCutLine(savedInstanceState);
            } else {
                Log.v("Restored image from cache");
            }
            if (bitmap_image != null) {
                exifData = ImagepipePreferences.readExifData(context);
                initOriginalBitmap();
            } else {
                no_image_loaded = true;
            }
            if (no_image_loaded) {
                setEmptyDefaultScreen();
            } else {
                updateImageDisplay();
                updateImagePropertiesText();
            }
            // when the last tool is known from a savedInstanceState, the paint tool
            // needs to be set after an image loaded, otherwise cache will be wiped by updateImageDisplay
            setPaintTool(paintTool);
        } else {
            Log.v("no savedInstanceState");
            if (i_action.equals(Intent.ACTION_EDIT)){
                Log.v("Workmode is EDIT");
                workmode=Workmode.EDIT;
                // clear picture cache because new image received
                clearCache();
                // Uri source_uri = call_intent.getParcelableExtra(Intent.EXTRA_STREAM);
                Uri source_uri = call_intent.getData();
                if (source_uri!=null){
                    if (readImageBitmapFromUri(source_uri,false)){
                        applyChangesOnLoadedImage(source_uri,true,true,true,null);
                    } else {
                        Toast.makeText(context,getResources().getString(R.string.toast_error_image_not_loaded) +" ("+source_uri+")",Toast.LENGTH_LONG).show();
                        Log.v("Error: image could not be loaded, probably broken uri or missing access to uri: "+source_uri);
                    }
                } else {
                    Toast.makeText(context,getResources().getString(R.string.toast_error_image_not_loaded),Toast.LENGTH_LONG).show();
                    Log.v("Error: image could not be loaded because no uri was provided.");
                }
            } else
            if (i_action.equals(Intent.ACTION_GET_CONTENT)){
                workmode=Workmode.GET_CONTENT;
                pickImageFromGallery(SELECT_FROM_GALLERY_GET_CONTENT_CALLBACK);
            } else
            if ((Intent.ACTION_SEND.equals(i_action)) && (i_type != null)) {
                // clear picture cache because new image received
                clearCache();
                pipeTheImage(call_intent);
            } else {
                if ((Intent.ACTION_SEND_MULTIPLE.equals(i_action)) && (i_type != null)) {
                    // clear picture cache & resources because new image received
                    clearImageData(false);
                    // receive multiple images
                    pipeMultipleImages(call_intent);
                } else {
                    // app seems to be lauched natively: no savedInstanceState, no send, no send_multiple
                    // try to load someting from cache
                    // may result in bitmaps == null
                    // =>
                    Log.v("no savedInstanceState & no action, app seems to be launched natively.");
                    Log.v("-> trying to restore image from cache");
                    readLastImageFromCacheAndRestoreCache();
                }
            }
        }
        updateImageDisplay();
        if (no_image_loaded){
            checkForHintBubbleDisplay();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("onCreate");
        context = getApplicationContext();
        setContentView(R.layout.activity_image_receiver);
        // debug
        // testing
        // int[] values = new int[]{8,5,3,9,6,4,77,8,5,33,4,5,66,22,1,0};
        // ImageFilters.quicksort(values,0,values.length-1);
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.Q){
            useScopedStorage = true;
        } else {
            useScopedStorage = false;
        }
        // this is a hack for devices with a buggy media store
        // useScopedStorage = false;
        // In versionCode 39 and below the picture cache file suffix was .tmp, above it is the suffix of the image type.
        // Therefore, old .tmp files need to be removed if an upgrade from below versionCode 40 occurs.
        if (ImagepipePreferences.getLastAppVersion(context)<39){
            PictureCache.clearPictureCache(context);
            ImagepipePreferences.setCurrentAppVersionFlag(context);
        }
        // since version 51 this setting moved from a String (list) to an Integer (seekBar), we need to map
        // the old string value to the new integer value.
        if (ImagepipePreferences.getLastAppVersion(context)<51){
            ImagepipePreferences.setQualitymaxvalue(context,ImagepipePreferences.getQualitymaxvalue_Legacy(context));
            ImagepipePreferences.setCurrentAppVersionFlag(context);
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_CLEAR_DATA);
        intentFilter.addAction(ACTION_SHOW_PROGRESS);
        intentFilter.addAction(ACTION_UPDATE_PROGRESS);
        intentFilter.addAction(ACTION_HIDE_PROGRESS);
        registerReceiver(broadcastReceiver,intentFilter);
        imageEditor();
        // check if app was launched by intent request one of the three actions: SEND, SEND_MULTIPLE OR GET_CONTENT
        String intentAction = null;
        Intent intent = getIntent();
        boolean validActionRequested = false;
        if (intent!=null){
            intentAction = intent.getAction();
            if ((intentAction.equals(Intent.ACTION_SEND)) ||
                    (intentAction.equals(Intent.ACTION_SEND_MULTIPLE)) ||
                    (intentAction.equals(Intent.ACTION_GET_CONTENT)) ||
                    (intentAction.equals(Intent.ACTION_EDIT))
            ) {
                validActionRequested = true;
            }
        }
        // displays a warning that imagepipe is configured to keep some exif tags, if appropriate
        if ( (validActionRequested) &&
                (!ImagepipePreferences.warningPermanentlyDisabled(context)) &&
                (!ImagepipePreferences.warningTemporarilyDisabled(context)) &&
                (ImagepipePreferences.keepSomeExifTags(context))){
            showExifWarning(savedInstanceState,
                    getResources().getString(R.string.quit),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    },
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            handleIntentOrRestoreFromInstanceState(savedInstanceState);
                            dialogInterface.dismiss();
                        }
                    },
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ImagepipePreferences.setPrefAllowedTags(context,"");
                            displayExifTagsIndicator();
                            handleIntentOrRestoreFromInstanceState(savedInstanceState);
                            dialogInterface.dismiss();
                        }
                    });
        } else {
            handleIntentOrRestoreFromInstanceState(savedInstanceState);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("onResume");
        // re-load exif data
        if (!no_image_loaded){
            exifData = ImagepipePreferences.readExifData(context);
            // if exif tags were modified, already sent file cannot be re-used
            if (ImagepipePreferences.allowedTagsModified(context)){
                image_needs_to_be_saved = true;
            }
        }
        // check if some preferences were changed that trigger the need to save the image again
        if (ImagepipePreferences.imageNeedsToBeSavedFlag(context)){
            image_needs_to_be_saved = true;
            ImagepipePreferences.setImageNeedsToBeSavedFlag(context,false);
        }
        // set all the listeners
        displayExifTagsIndicator();
        // landscape
        Button button_rotateleft = (Button) findViewById(R.id.rotateleft_button);
        Button button_rotateright = (Button) findViewById(R.id.rotateright_button);
        Button button_flip = (Button) findViewById(R.id.flip_button);
        Button button_dither = (Button) findViewById(R.id.filter_button);
        if (button_rotateleft!=null){
            button_rotateleft.setOnClickListener(rotateLeftListener);
            button_rotateleft.setOnLongClickListener(rotateDialogListener);
        }
        if (button_rotateright!=null){
            button_rotateright.setOnClickListener(rotateRightListener);
            button_rotateright.setOnLongClickListener(rotateDialogListener);
        }
        if (button_flip!=null){
            button_flip.setOnClickListener(flipHorizontalListener);
            button_flip.setOnLongClickListener(flipVerticallyListener);
        }
        if (button_dither!=null){
            button_dither.setOnClickListener(filterListener);
            button_dither.setOnLongClickListener(filterselectorListener);
        }
        // portrait
        ImageButton imageButton_rotateleft = (ImageButton) findViewById(R.id.rotateleft_imagebutton);
        ImageButton imageButton_rotateright = (ImageButton) findViewById(R.id.rotateright_imagebutton);
        ImageButton imageButton_flip = (ImageButton) findViewById(R.id.flip_imagebutton);
        ImageButton imageButton_dither = (ImageButton) findViewById(R.id.filter_imagebutton);
        if (imageButton_rotateleft!=null){
            imageButton_rotateleft.setOnClickListener(rotateLeftListener);
            imageButton_rotateleft.setOnLongClickListener(rotateDialogListener);
        }
        if (imageButton_rotateright!=null){
            imageButton_rotateright.setOnClickListener(rotateRightListener);
            imageButton_rotateright.setOnLongClickListener(rotateDialogListener);
        }
        if (imageButton_flip!=null){
            imageButton_flip.setOnClickListener(flipHorizontalListener);
            imageButton_flip.setOnLongClickListener(flipVerticallyListener);
        }
        if (imageButton_dither!=null){
            imageButton_dither.setOnClickListener(filterListener);
            imageButton_dither.setOnLongClickListener(filterselectorListener);
        }
        // landscape & portrait
        Button button_processimage = (Button) findViewById(R.id.process_button);
        Button button_reloadimage = (Button) findViewById(R.id.reload_button);
        button_processimage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!no_image_loaded){
                    scaleImage(bitmap_image.getWidth(),bitmap_image.getHeight(),false);
                    putImageBitmapToCache(null,false);
                    updateImageDisplay();
                }
            }
        });
        button_reloadimage.setOnClickListener(reloadListener);
        button_reloadimage.setOnLongClickListener(reloadNoShrinkListener);
        // update quality bar, quality display & quality preview since this might have changed when returning from
        // the settings.
        if (ImagepipePreferences.maxQualityChanged(context)){
            int maxQualityValue = ImagepipePreferences.getQualitymaxvalue(context);
            seekBar.setMax(maxQualityValue);
            ImagepipePreferences.setQuality(context,seekBar.getProgress());
            displayImageQuality();
            ImagepipePreferences.resetQualitymaxvalueChangedFlag(context);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy(){
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    @SuppressWarnings("deprecation")
    private void setOverflowMenuItemColor(Menu menu, int id, int string_id,int color_id){
        String s = getApplicationContext().getResources().getString(string_id);
        MenuItem menuItem = menu.findItem(id);
        SpannableString spannableString = new SpannableString(s);
        if (Build.VERSION.SDK_INT>=23){
            spannableString.setSpan(new ForegroundColorSpan(getApplicationContext().getResources().getColor(color_id,getTheme())),0,s.length(),0);
        } else {
            spannableString.setSpan(new ForegroundColorSpan(getApplicationContext().getResources().getColor(color_id)),0,s.length(),0);
        }
        menuItem.setTitle(spannableString);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.image_receiver,menu);
        if (Build.VERSION.SDK_INT>17){
            // try to show icons in drop-down menu
            if (menu.getClass().getSimpleName().equals("MenuBuilder")){
                try {
                    Method method = menu.getClass().getDeclaredMethod("setOptionalIconsVisible",Boolean.TYPE);
                    method.setAccessible(true);
                    method.invoke(menu,true);
                } catch (Exception e){
                    // do nothing
                }
            }
            setOverflowMenuItemColor(menu,R.id.menu_add,R.string.gallery_button, R.color.primaryTextColor);
            setOverflowMenuItemColor(menu,R.id.menu_share,R.string.share_button, R.color.primaryTextColor);
            setOverflowMenuItemColor(menu,R.id.menu_save,R.string.save_button, R.color.primaryTextColor);
            setOverflowMenuItemColor(menu,R.id.menu_cleardata,R.string.cleardata_button, R.color.primaryTextColor);
            setOverflowMenuItemColor(menu,R.id.menu_settings,R.string.settings_button, R.color.primaryTextColor);
            setOverflowMenuItemColor(menu,R.id.menu_exifdialog,R.string.exif_tags, R.color.primaryTextColor);
            setOverflowMenuItemColor(menu,R.id.menu_about,R.string.about_button, R.color.primaryTextColor);
            setOverflowMenuItemColor(menu,R.id.menu_licence,R.string.licence_button, R.color.primaryTextColor);
        }
        this.menu = menu;
        setWorkmodeIcon();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem mi){
       int item_id = mi.getItemId();
        if (item_id == R.id.menu_add) {
            pickImageFromGallery(SELECT_FROM_GALLERY_CALLBACK);
        }
        if ((item_id == R.id.menu_share) && (workmode==Workmode.GET_CONTENT)){
            // return the result for the GET_CONTENT intent and terminate imagepipe
            launchAfterImageArrivedInCache.run();
            finish();
            return true;
        }
        if ((item_id == R.id.menu_share) && (workmode == Workmode.SHARE)){
            // default action, share as usual
            if ( (!ImagepipePreferences.warningPermanentlyDisabled(context)) &&
                    (!ImagepipePreferences.warningTemporarilyDisabled(context)) &&
                    (ImagepipePreferences.keepSomeExifTags(context))){
                showExifWarning(null,
                        getResources().getString(R.string.info_back),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                sendImageUriIntent();
                                dialogInterface.dismiss();
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ImagepipePreferences.setPrefAllowedTags(context,"");
                                image_needs_to_be_saved = true; // need to save again without the exif tags!
                                updateImageDisplay();
                                sendImageUriIntent();
                                dialogInterface.dismiss();
                            }
                        });
            } else {
                sendImageUriIntent();
            }
            return true;
        }
        // in workmode EDIT, both menu options actually do the same. However, in workmode EDIT the app
        // finishes after saving.
        if (item_id == R.id.menu_save){
            saveAndScanImage(true);
            return true;
        }
        if ((item_id == R.id.menu_share) && (workmode == Workmode.EDIT)){
            saveAndScanImage(true);
            finish();
            return true;
        }
        if (item_id == R.id.menu_cleardata){
            clearImageData(true);
        }
        if (item_id == R.id.menu_settings) {
            Intent i = new Intent(this, Settings.class);
            startActivity(i);
            return true;
        }
        if (item_id==R.id.menu_licence) {
            Intent i = new Intent(this, ImagePipeInfo.class);
            i.putExtra(DATA_TITLE, getResources().getString(R.string.license_title));
            i.putExtra(DATA_TEXTRESOURCE, "license");
            i.putExtra(DATA_BUTTONTEXT,getResources().getString(R.string.intro_button_text));
            startActivity(i);
            return true;
        }
        if (item_id == R.id.menu_exifdialog) {
            showExifDialog();
            return true;
        }
        if (item_id == R.id.menu_about) {
            showIntroDialog();
            return true;
        }
        return super.onOptionsItemSelected(mi);
    }

    private int getPixelColor(float x, float y){
        if (bitmap_visible!=null){
            if ((x<bitmap_visible.getWidth()) && (y<bitmap_visible.getHeight()) && (x>0) && (y>0)){
                int color = bitmap_visible.getPixel(Math.round(x),Math.round(y));
                return color;
            }
        }
        return Color.TRANSPARENT;
    }

    /**
     * Draws the cut-frame inside the imageview. Coordinates are absolute values on the bitmap.
     * @param x1 x-coord
     * @param y1 y-coord
     * @param x2 x-coord
     * @param y2 y-coord
     * @param scale_ratio Scale ratio of the bitmap inside the imageview.
     */

    private void drawCutFrame(float x1,float y1, float x2, float y2, float scale_ratio){
        if ((!no_image_loaded) && (paintTool==Tool.CROP) && (bitmap_visible!=null)){
            Bitmap draw_bitmap;
           try {
                draw_bitmap = bitmap_visible.copy(bitmap_visible.getConfig(),true);
            } catch (OutOfMemoryError e){
                // nothing to do; this frame drawing may fail due to low memory.
                return;
            }
            Trim.drawCutFrame(context,draw_bitmap,x1,y1,x2,y2,scale_ratio);
            Trim.applyAlpha(draw_bitmap,cut_line_left,cut_line_top,cut_line_right,cut_line_bottom);
            if ((image_display.getWidth() !=0) && (image_display.getHeight() != 0)) {
                image_display.setImageBitmap(draw_bitmap);
                image_display.invalidate();
            }
            updateImagePropertiesText();
         }
    }

    /**
     * Draws the cut-frame inside the imageview. Coordinates are absolute values on the bitmap.
     * Debug version for also displaying touch-event coordinates inside the imageview. Not used in
     * the productive code.
     *
     * @param x1 x-coord
     * @param y1 y-coord
     * @param x2 x-coord
     * @param y2 y-coord
     * @param x3 x-coord of touch-event to display
     * @param y3 y-coord of touch-event to display
     * @param scale_ratio Scale ratio of the bitmap inside the imageview.
     */

    private void drawCutFrame(float x1,float y1, float x2, float y2, float scale_ratio, float x3, float y3){
        if ((!no_image_loaded) && (paintTool==Tool.CROP)){
            Bitmap draw_bitmap;
            try {
                draw_bitmap = bitmap_visible.copy(bitmap_visible.getConfig(),true);
            } catch (OutOfMemoryError e){
                // nothing to do; this frame drawing may fail due to low memory.
                return;
            }
            Canvas canvas = new Canvas();
            canvas.setBitmap(draw_bitmap);
            Paint paint = new Paint();
            paint.setColor(Color.CYAN);
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(5 * scale_ratio);
            canvas.drawCircle(x3, y3, 10, paint);
            canvas.drawRect(x1, y1, x2, y2, paint);
            if ((image_display.getWidth() !=0) && (image_display.getHeight() != 0)) {
                image_display.setImageBitmap(draw_bitmap);
                image_display.invalidate();
            }
        }
        }

    /**
     * Calculates the new cut-frame-borders from the touch-event.
     * All coordinates are BITMAP coordinates. They were corrected from the view-coordinates by taking the
     * scale ratio into consideration!
     * @param x_delta_image delta-x from the touch-event (swipe)
     * @param y_delta_image delta-y from the touch-event (swipe)
     */

    private void calculateCutFramePosition(float x, float y, float x_delta_image, float y_delta_image){
        float a = Math.abs(cut_line_right-cut_line_left); float b = Math.abs(cut_line_bottom-cut_line_top);
        float toleranceX = a/6f;
        float toleranceY = b/6f;
        boolean touch_is_inside_frame = false;
        if (    (x > cut_line_left+toleranceX) &&
                (x < cut_line_right-toleranceX) &&
                (y > cut_line_top+toleranceY) &&
                (y < cut_line_bottom-toleranceY)){
            touch_is_inside_frame = true;
        }
        if (touch_is_inside_frame) {
            if (x_delta_image < 0) {
                if (cut_line_left-Math.abs(x_delta_image)>0){
                    cut_line_right = cut_line_right - Math.abs(x_delta_image);
                    cut_line_left = cut_line_left - Math.abs(x_delta_image);
                }
            }
            if (x_delta_image > 0) {
                if (cut_line_right+ Math.abs(x_delta_image)<bitmap_visible.getWidth()){
                    cut_line_right = cut_line_right + Math.abs(x_delta_image);
                    cut_line_left  = cut_line_left + Math.abs(x_delta_image);
                }
            }
            if (y_delta_image < 0) {
                if (cut_line_top- Math.abs(y_delta_image)>0){
                    cut_line_top    = cut_line_top - Math.abs(y_delta_image);
                    cut_line_bottom = cut_line_bottom - Math.abs(y_delta_image);
                }
            }
            if (y_delta_image > 0) {
                if (cut_line_bottom+ Math.abs(y_delta_image)<bitmap_visible.getHeight()){
                    cut_line_top = cut_line_top + Math.abs(y_delta_image);
                    cut_line_bottom = cut_line_bottom + Math.abs(y_delta_image);
                }
            }
        } else {
            // change left border
            if (x<cut_line_left+toleranceX){
                // swipe to the left
                if (x_delta_image<0){
                    cut_line_left = cut_line_left - Math.abs(x_delta_image);
                }
                // swipe to the right
                if (x_delta_image>0){
                    // prevent switch of cutlines
                    if (cut_line_left+Math.abs(x_delta_image)<cut_line_right){
                        cut_line_left = cut_line_left + Math.abs(x_delta_image);
                    }
                }
            }
            // change right border
            if (x>cut_line_right-toleranceX){
                if (x_delta_image<0){
                    if (cut_line_right - Math.abs(x_delta_image)>cut_line_left){
                        cut_line_right = cut_line_right - Math.abs(x_delta_image);
                    }
                }
                if (x_delta_image>0){
                    cut_line_right = cut_line_right + Math.abs(x_delta_image);
                }
            }
            // change top border
            if (y<cut_line_top+toleranceY){
                if (y_delta_image<0){
                    cut_line_top = cut_line_top - Math.abs(y_delta_image);
                }
                // prevent switch of cutlines
                if (y_delta_image>0){
                    if (cut_line_top + Math.abs(y_delta_image)<cut_line_bottom){
                        cut_line_top = cut_line_top + Math.abs(y_delta_image);
                    }
                }
            }
            // change bottom border
            if (y>cut_line_bottom-toleranceY){
                if (y_delta_image<0){
                    if (cut_line_bottom - Math.abs(y_delta_image)>cut_line_top){
                        cut_line_bottom = cut_line_bottom - Math.abs(y_delta_image);
                    }
                }
                if (y_delta_image>0){
                    cut_line_bottom = cut_line_bottom + Math.abs(y_delta_image);
                }
            }
        }
        fixCropOutOfBounds();
        applyFixedAspectRatio(x_delta_image,y_delta_image);
    }

    private void fixCropOutOfBounds(){
        if (cut_line_left<0)
            cut_line_left = 0;
        if (cut_line_right>bitmap_visible.getWidth())
            cut_line_right = bitmap_visible.getWidth();
        if (cut_line_top<0)
            cut_line_top = 0;
        if (cut_line_bottom>bitmap_visible.getHeight())
            cut_line_bottom = bitmap_visible.getHeight();
    }

    private void applyFixedAspectRatio(float x_delta, float y_delta){
        // fixed aspect ratio
        Float aspectRatio = ImagepipePreferences.getAspectRatioFloat(context);
        if (aspectRatio!=null){
            if (Math.abs(x_delta)>Math.abs(y_delta)){
                float width = cut_line_right-cut_line_left;
                float height = width * aspectRatio;
                cut_line_bottom = cut_line_top + height;
                if (cut_line_bottom>bitmap_visible.getHeight()){
                    cut_line_top = bitmap_visible.getHeight()-height;
                    cut_line_bottom = bitmap_visible.getHeight();
                }
            } else {
                float height = cut_line_bottom - cut_line_top;
                float width = height / aspectRatio;
                cut_line_right = cut_line_left + width;
                if (cut_line_right>bitmap_visible.getWidth()){
                    cut_line_left = bitmap_visible.getWidth()-width;
                    cut_line_right = bitmap_visible.getWidth();
                }
            }
        }
    }

    private void applyFixedAspectRatioOld(){
        // fixed aspect ratio
        Float aspectRatio = ImagepipePreferences.getAspectRatioFloat(context);
        if (aspectRatio!=null){
            if (aspectRatio<=1){
                float width = cut_line_right-cut_line_left;
                float height = width * aspectRatio;
                cut_line_bottom = cut_line_top + height;
                if (cut_line_bottom>bitmap_visible.getHeight()){
                    cut_line_top = bitmap_visible.getHeight()-height;
                    cut_line_bottom = bitmap_visible.getHeight();
                }
            } else {
                float height = cut_line_bottom - cut_line_top;
                float width = height / aspectRatio;
                cut_line_right = cut_line_left + width;
                if (cut_line_right>bitmap_visible.getWidth()){
                    cut_line_left = bitmap_visible.getWidth()-width;
                    cut_line_right = bitmap_visible.getWidth();
                }
            }
        }
    }

    /**
     * Calculates the scale ratio of the bitmap inside the imageview.
     * @return float scale-ratio
     */

    private Float getScaleRatio(){
        if ((image_display!=null) && (bitmap_image!=null)) {
            if ((image_display.getWidth()!=0) && (image_display.getHeight()!=0) && (bitmap_image.getWidth()!=0) && (bitmap_image.getHeight()!=0)){
                float view_width = image_display.getWidth();
                float view_height = image_display.getHeight();
                float original_image_width = bitmap_image.getWidth();
                float original_image_height = bitmap_image.getHeight();
                float x_ratio = original_image_width / view_width;
                float y_ratio = original_image_height / view_height;
                return Math.max(y_ratio, x_ratio);
            }
        }
        return 1f;
    }

    /**
     * Helper sub to get a color from resource.
     * @param color_resource is the R.id.color of the color.
     * @return int color value from a resource
     */

    @SuppressWarnings("deprecation")
    public static int GetAppColor(Context context, int color_resource){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            try {
                return context.getResources().getColor(color_resource, context.getTheme());
            } catch ( Exception e ) {
                return 0;
            }
            }
            else {
            try {
                return context.getResources().getColor(color_resource);
            } catch ( Exception e ) {
                return 0;
            }
            }
    }

    /**
     * A runnable that updates the image_display view with the image bitmap. This is only
     * a helper of the updateImageDisplay() call.
     */

    private void updateImageDisplay_runnable(){
        if (bitmap_image!=null){
            bitmap_visible = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
            image_display.setImageBitmap(bitmap_visible);
            if ((ImagepipePreferences.previewQuality(context)) && (!ImagepipePreferences.getCompressFormatFileExtension(context).equals(ImagepipePreferences.ImageFileFormat.PNG))){
                displayImageQuality();
            }
            displayExifTagsIndicator();
        }
    }

    /**
     * Updates the image_display view with the image bitmap.
     * Checks if the view is already visible. If not, the call is queued.
     */

    private void updateImageDisplay(){
        if (bitmap_image!=null){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateImageDisplay_runnable();
                }
            });
        } else {
            setEmptyDefaultScreen();
        }
    }

    public void displayExifTagsIndicator(){
        TextView allowedTagsWarning = (TextView) findViewById(R.id.warning_indicator);
        if (allowedTagsWarning!=null) {
            if (ImagepipePreferences.getAllowedTags(context).length>0) {
                if (ImagepipePreferences.allowedTagsModified(context)){
                    allowedTagsWarning.setVisibility(View.VISIBLE);
                    Animation animation = new AlphaAnimation(0.5f,1f);
                    animation.setDuration(500);
                    animation.setRepeatCount(6);
                    animation.setRepeatMode(Animation.REVERSE);
                    allowedTagsWarning.startAnimation(animation);
                    ImagepipePreferences.resetAllowedTagsModified(context);
                }
            } else {
                allowedTagsWarning.setVisibility(View.INVISIBLE);
            }
        }
    }

    private int determineCutlinePointer(float x0,float y0,float x1,float y1, int pointer){
        if (pointer==LEFT_CUTLINE){
            if (x0<=x1) {
                return 0;
            } else {
                return 1;
            }
        }
        if (pointer==RIGHT_CUTLINE){
            if (x0>=x1) {
                return 0;
            } else {
                return 1;
            }
        }
        if (pointer==TOP_CUTLINE){
            if (y0<=y1) {
                return 0;
            } else {
                return 1;
            }
        }
        if (pointer==BOTTOM_CUTLINE)
            if (y0>=y1) {
                return 0;
            } else {
                return 1;
            }
        return 0;
    }

    private void calculateCutFrameMultitouch(View v, MotionEvent me, int historyindex){
        float x_padding = (v.getWidth()-bitmap_visible.getWidth()/scale_ratio)/2;
        float y_padding = (v.getHeight()-bitmap_visible.getHeight()/scale_ratio)/2;

        int i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),LEFT_CUTLINE);
        float x_value = (me.getHistoricalX(i,historyindex) - x_padding) * scale_ratio;
        float y_value = (me.getHistoricalY(i,historyindex) - y_padding) * scale_ratio;
        float x_delta = x_value - x_pos;
        float y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_left = cut_line_left + x_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),RIGHT_CUTLINE);
        x_value = (me.getHistoricalX(i,historyindex) - x_padding) * scale_ratio;
        y_value = (me.getHistoricalY(i,historyindex) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_right = cut_line_right + x_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),TOP_CUTLINE);
        x_value = (me.getHistoricalX(i,historyindex) - x_padding) * scale_ratio;
        y_value = (me.getHistoricalY(i,historyindex) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_top = cut_line_top + y_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),BOTTOM_CUTLINE);
        x_value = (me.getHistoricalX(i,historyindex) - x_padding) * scale_ratio;
        y_value = (me.getHistoricalY(i,historyindex) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_bottom = cut_line_bottom + y_delta;

        fixCropOutOfBounds();
        applyFixedAspectRatio(x_delta,y_delta);
    }

    private void calculateCutFrameMultitouch(View v, MotionEvent me){
        float x_padding = (v.getWidth()-bitmap_visible.getWidth()/scale_ratio)/2;
        float y_padding = (v.getHeight()-bitmap_visible.getHeight()/scale_ratio)/2;

        int i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),LEFT_CUTLINE);
        float x_value = (me.getX(i) - x_padding) * scale_ratio;
        float y_value = (me.getY(i) - y_padding) * scale_ratio;
        float x_delta = x_value - x_pos;
        float y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_left = cut_line_left + x_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),RIGHT_CUTLINE);
        x_value = (me.getX(i) - x_padding) * scale_ratio;
        y_value = (me.getY(i) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_right = cut_line_right + x_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),TOP_CUTLINE);
        x_value = (me.getX(i) - x_padding) * scale_ratio;
        y_value = (me.getY(i) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_top = cut_line_top + y_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),BOTTOM_CUTLINE);
        x_value = (me.getX(i) - x_padding) * scale_ratio;
        y_value = (me.getY(i) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_bottom = cut_line_bottom + y_delta;
        fixCropOutOfBounds();
        applyFixedAspectRatio(x_delta,y_delta);
    }

    private void setPaintTool(int tool){
        if (paintTool==Tool.CROP){
            buttonCrop.setImageResource(R.mipmap.ic_crop_white_24dp);
        }
        lastPaintTool = paintTool;
        paintTool = tool;
        int cropColor = Color.TRANSPARENT;
        int blurColor = Color.TRANSPARENT;
        int textColor = Color.TRANSPARENT;
        int paintColor = Color.TRANSPARENT;
        int colorPickerColor = Color.TRANSPARENT;
        switch (paintTool){
            case Tool.CROP: cropColor = GetAppColor(context,R.color.colorAccent); break;
            case Tool.BLUR: blurColor = GetAppColor(context,R.color.colorAccent); break;
            case Tool.TEXT: textColor = GetAppColor(context,R.color.colorAccent); break;
            case Tool.PAINT: paintColor = GetAppColor(context,R.color.colorAccent); break;
            case Tool.COLORPICKER: colorPickerColor = GetAppColor(context,R.color.colorAccent);
        }
        if (paintTool==Tool.CROP){
            buttonCrop.setImageResource(R.mipmap.ic_check_white_24dp);
        }
        buttonCropFrame.setBackgroundColor(cropColor);
        buttonBlurFrame.setBackgroundColor(blurColor);
        buttonTextFrame.setBackgroundColor(textColor);
        buttonPaintFrame.setBackgroundColor(paintColor);
        buttonColorPickerFrame.setBackgroundColor(colorPickerColor);
        // remove visible cut-frames if tool is not crop
        if (tool!=Tool.CROP){
            updateImageDisplay();
        }
    }

    private void registerPaintToolButtonListeners(){
        buttonCropFrame = (FrameLayout) findViewById(R.id.tools_crop_frame);
        buttonBlurFrame = (FrameLayout) findViewById(R.id.tools_blur_frame);
        buttonTextFrame = (FrameLayout) findViewById(R.id.tools_text_frame);
        buttonPaintFrame = (FrameLayout) findViewById(R.id.tools_paint_frame);
        buttonColorFrame = (FrameLayout) findViewById(R.id.tools_color_frame);
        buttonColorPickerFrame = (FrameLayout) findViewById(R.id.tools_colorpicker_frame);
        buttonCrop  = (ImageButton) findViewById(R.id.tools_crop);
        buttonBlur  = (ImageButton) findViewById(R.id.tools_blur);
        buttonText  = (ImageButton) findViewById(R.id.tools_text);
        buttonPaint = (ImageButton) findViewById(R.id.tools_paint);
        buttonColor = (ImageButton) findViewById(R.id.tools_color);
        buttonColorPicker = (ImageButton) findViewById(R.id.tools_colorpicker);
        buttonUnDo = (ImageButton) findViewById(R.id.tools_undo);
        buttonReDo = (ImageButton) findViewById(R.id.tools_redo);
        buttonCrop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (paintTool==Tool.CROP){
                    crop();
                } else {
                    resetCutFrame();
                    setPaintTool(Tool.CROP);
                    drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                }
            }
        });
        buttonCrop.setOnLongClickListener(cropRatioListener);
        buttonBlur.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setPaintTool(Tool.BLUR);
                if (brushSize<0){
                    brushSize = 25;
                    showBrushSelector();
                }
            }
        });
        buttonBlur.setOnLongClickListener(brushSizeListener);
        buttonText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setPaintTool(Tool.TEXT);
                if (charSequence.equals("")){
                    showTextSelector();
                }
            }
        });
        buttonText.setOnLongClickListener(charSequenceListener);
        buttonPaint.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setPaintTool(Tool.PAINT);
                if (brushSize<0){
                    brushSize = 25;
                    showBrushSelector();
                }
            }
        });
        buttonPaint.setOnLongClickListener(brushSizeListener);
        buttonColor.setOnClickListener(paletteListener);
        buttonColorPicker.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setPaintTool(Tool.COLORPICKER);
            }
        });
        // disable new ColorPicker for API<21, since it leads to crashes on 4.4.4
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            buttonColor.setOnLongClickListener(colorPickerListener);
        }
        buttonColor.setColorFilter(selectedColor,PorterDuff.Mode.SRC_ATOP);
        buttonUnDo.setOnClickListener(unDoButtonListener);
        buttonReDo.setOnClickListener(reDoButtonListener);
    }

    private void setLastModifiedTimestamp(){
        imageLastModified = Calendar.getInstance().getTimeInMillis();
        image_needs_to_be_saved = true;
    }

    private int realBrushSize(){
        return brushSize;
    }

    private int realBlurSize(){
        return brushSize;
    }

    public void drawLineWithBrush(Canvas canvas, float x0, float y0, float x1,float y1, Paint paint){
        if (x0>x1){
            float x3=x0; float y3=y0; x0 = x1; y0=y1; x1=x3; y1=y3;
        }
        float deltaX = x1-x0;
        float deltaY = y1-y0;
        if (Math.abs(deltaX)>Math.abs(deltaY)){
            for (int x = Math.round(x0); x<Math.round(x1); x++){
                canvas.drawCircle(x,y0+(deltaY/deltaX)*(x-x0),realBrushSize()/2f,paint);
            }
        } else {
            if (y0>y1){
                float x3=x0; float y3=y0; x0 = x1; y0=y1; x1=x3; y1=y3;
            }
            for (int y = Math.round(y0); y<Math.round(y1); y++){
                canvas.drawCircle(x0+(deltaX/deltaY)*(y-y0),y,realBrushSize()/2f,paint);
            }
        }
    }

    public void paintBrushApply(float x, float y,final Canvas canvas){
        Paint paint = new Paint();
        paint.setColor(selectedColor);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        if ((xPaintCache!=0) && (yPaintCache!=0)){
            drawLineWithBrush(canvas,x,y,xPaintCache,yPaintCache,paint);
        } else {
            canvas.drawCircle(x,y,realBrushSize()/2f,paint);
        }
    }

    public void paintBrush(float x, float y){
        Canvas image = new Canvas(bitmap_image);
        Canvas visible = new Canvas(bitmap_visible);
        paintBrushApply(x,y,image);
        paintBrushApply(x,y,visible);
        xPaintCache = x;
        yPaintCache = y;
        image_display.setImageBitmap(bitmap_visible);
        setLastModifiedTimestamp();
    }

    public void blurBrushApply(float x, float y,final Canvas canvas, int color){
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setAntiAlias(true);
        paint.setMaskFilter(new BlurMaskFilter(realBrushSize(),BlurMaskFilter.Blur.NORMAL));
        canvas.drawCircle(x,y,realBlurSize()/2f,paint);
    }

    public int getBlurColor(final Bitmap bitmap, final int x, final int y){
        ArrayList<Integer> colors = new ArrayList<Integer>();
        try {
            colors.add(bitmap.getPixel(x,y));
            colors.add(bitmap.getPixel(x-1,y));
            colors.add(bitmap.getPixel(x+1,y));
            colors.add(bitmap.getPixel(x,y-1));
            colors.add(bitmap.getPixel(x,y+1));
        } catch (IllegalArgumentException e){
            if (colors.size()<1){
                colors.add(Color.BLACK);
            }
        }
        int red = 0;
        int green = 0;
        int blue = 0;
        for (int i=0; i<colors.size(); i++){
            red = red + Color.red(colors.get(i));
            green = green + Color.green(colors.get(i));
            blue = blue + Color.blue(colors.get(i));
        }
        red = red/(colors.size());
        green = green/(colors.size());
        blue = blue/(colors.size());
        int blurColor = Color.argb(255,red,green,blue);
        return blurColor;
    }

    public void blurBrush(float x, float y){
        Canvas image = new Canvas(bitmap_image);
        Canvas visible = new Canvas(bitmap_visible);
        int color = getBlurColor(bitmap_image,Math.round(x),Math.round(y));
        blurBrushApply(x,y,image,color);
        blurBrushApply(x,y,visible,color);
        image_display.setImageBitmap(bitmap_visible);
        setLastModifiedTimestamp();
    }

    public void putTextApply(float x, float y, final Canvas canvas, int textsize){
        Paint paint = new Paint();
        paint.setColor(selectedColor);
        paint.setAntiAlias(true);
        paint.setTextSize(textsize);
        if (textStyle_bold){
            paint.setFakeBoldText(true);
        }
        if (textStyle_italic){
            paint.setTextSkewX((float) -0.25);
        }
        if (textStyle_underline){
            paint.setUnderlineText(true);
        }
        if (textStyle_strikethrough){
            paint.setStrikeThruText(true);
        }
        canvas.drawText(String.valueOf(charSequence),x,y,paint);
    }

    public void putText(float x, float y){
        Canvas image = new Canvas(bitmap_image);
        Canvas visible = new Canvas(bitmap_visible);
        int textsize = realBrushSize();
        Paint paint = new Paint();
        // decrease font size to fit text in position
        paint.setTextSize(textsize);
        while ((textsize>6) && (paint.measureText(String.valueOf(charSequence))>bitmap_image.getWidth()-x)){
            textsize--;
            paint.setTextSize(textsize);
        }
        putTextApply(x,y,image,textsize);
        putTextApply(x,y,visible,textsize);
        image_display.setImageBitmap(bitmap_visible);
        setLastModifiedTimestamp();
    }

    public void crop(){
        if (!no_image_loaded){
            if ((cut_line_left<cut_line_right) && (cut_line_top<cut_line_bottom)) {
                try {
                    bitmap_image = Bitmap.createBitmap(bitmap_image, Math.round(cut_line_left), Math.round(cut_line_top), Math.round(cut_line_right - cut_line_left), Math.round(cut_line_bottom - cut_line_top));
                    setLastModifiedTimestamp();
                    putImageBitmapToCache(null,false);
                    updateImageDisplay();
                    image_needs_to_be_saved = true;
                    updateImagePropertiesText();
                    resetCutFrame();
                } catch (Exception e) {
                    // simply nothing to do if cut failed for some reason.
                }
            }
        }
    }

    public void rotateCutFrameRight(){
        float cut_line_bottom_new = cut_line_right;
        float cut_line_top_new = cut_line_left;
        float cut_line_left_new = bitmap_image.getWidth() - cut_line_bottom;
        float cut_line_right_new = bitmap_image.getWidth() - cut_line_top;
        cut_line_bottom = cut_line_bottom_new;
        cut_line_top = cut_line_top_new;
        cut_line_left = cut_line_left_new;
        cut_line_right = cut_line_right_new;
    }

    public void rotateRight(){
        if (rotateImage(90,false)) {
            rotateCutFrameRight();
            putImageBitmapToCache(null,false);
            updateImageDisplay();
            drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
        }
    }

    public void rotateCutFrameLeft(){
        float cut_line_bottom_new = bitmap_image.getHeight() - cut_line_left;
        float cut_line_top_new = bitmap_image.getHeight() - cut_line_right;
        float cut_line_left_new = cut_line_top;
        float cut_line_right_new = cut_line_bottom;
        cut_line_bottom = cut_line_bottom_new;
        cut_line_top = cut_line_top_new;
        cut_line_left = cut_line_left_new;
        cut_line_right = cut_line_right_new;
    }

    public void rotateLeft(){
        if (rotateImage(-90,false)) {
            putImageBitmapToCache(null,false);
            rotateCutFrameLeft();
            updateImageDisplay();
            drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
        }
    }

    public void rotateCutFrame(int surfaceRotation){
        // todo
    }

    public void setQualitybarVisibility(){
        TextView textView = (TextView) findViewById(R.id.quality_text);
        if (ImagepipePreferences.getCompressFormatFileExtension(context).equals(ImagepipePreferences.ImageFileFormat.PNG)) {
            if (seekBar!=null){
                seekBar.setVisibility(View.INVISIBLE);
            }
            if (textView!=null){
                textView.setVisibility(View.INVISIBLE);
            }
        } else {
            if (seekBar!=null){
                seekBar.setVisibility(View.VISIBLE);
            }
            if (textView!=null){
                textView.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setPaintColor(int color){
        selectedColor = color;
        ImagepipePreferences.setLastColor(context,selectedColor);
        buttonColor.setColorFilter(selectedColor,PorterDuff.Mode.SRC_ATOP);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void imageEditor(){
        Log.v("imageEditor() started.");
        TextView scale_view = (TextView) findViewById(R.id.editText_size);
        scale_view.setText(ImagepipePreferences.getScaleMax(context));
        if (seekBar==null){
            seekBar = (SeekBar) findViewById(R.id.seekBar_quality);
        }
        seekBar.setMax(ImagepipePreferences.getQualitymaxvalue(context));
        seekBar.setProgress(ImagepipePreferences.getQuality(context));
        originalImageContainer = ImagepipePreferences.getOriginalImageContainer(context);
        selectedColor = ImagepipePreferences.getLastColor(context);
        registerPaintToolButtonListeners();
        // Style the actionbar
        actionBar = getActionBar();
        if (actionBar!=null){
            actionBar.setTitle(getResources().getString(R.string.app_name));
            actionBar.setSubtitle("");
        }
        image_display = (ImageView) findViewById(R.id.pipedimage);
        // disable hardware acceleration for this view because blur is not supported with hardware acc.
        image_display.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
        image_display.setOnTouchListener(new ImageView.OnTouchListener(){
        @Override
            public boolean onTouch(View v, MotionEvent me){
               // hide floating menus if visible
                hideToolsIfVisible();
               if (bubble_visible){
                   updateImagePropertiesText();
               }
               if (no_image_loaded){
                   if (!gallery_already_called)
                       if ((me.getX()>=x1_choose_button) && (me.getX()<=x2_choose_button) &&
                               (me.getY()>=y1_choose_button) && (me.getY()<=y2_choose_button)){
                       pickImageFromGallery(SELECT_FROM_GALLERY_CALLBACK);
                       gallery_already_called = true;
                       return true;
                        }
                   return false;
               } else {
                   // it might happen that there is no bitmap_visible initialized yet because this is done in
                   // async putImageBitmapToCache. We need to abort the listener, then.
                   if (bitmap_visible==null){
                       return false;
                   }
               }
               int action = me.getActionMasked();
               int pid    = me.getActionIndex();
                scale_ratio=getScaleRatio();
                float x_padding = (v.getWidth()-bitmap_image.getWidth()/scale_ratio)/2;
                float y_padding = (v.getHeight()-bitmap_image.getHeight()/scale_ratio)/2;
                for (int i=0; i<me.getPointerCount();i++) {
                    if ((me.getX(i) > x_padding) && (me.getX(i) < v.getWidth() - x_padding) &&
                            (me.getY(i) > y_padding) && (me.getY(i) < v.getHeight() - y_padding)) {
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                    }
                }
               if (action==MotionEvent.ACTION_DOWN){
                   x_pos = (me.getX() - x_padding) * scale_ratio;
                   y_pos = (me.getY() - y_padding) * scale_ratio;
                   if (paintTool==Tool.CROP){
                       drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                   }
                   if (paintTool==Tool.PAINT){
                       paintBrush(x_pos,y_pos);
                   }
                   if (paintTool==Tool.BLUR){
                       blurBrush(x_pos,y_pos);
                   }
                   if (paintTool==Tool.TEXT){
                       putText(x_pos,y_pos);
                   }
                   if (paintTool==Tool.COLORPICKER){
                       setPaintColor(getPixelColor(x_pos,y_pos));
                   }
                   return true;
               }
            if (action==MotionEvent.ACTION_POINTER_DOWN){
                x1_pos = (me.getX() - x_padding) * scale_ratio;
                y1_pos = (me.getY() - y_padding) * scale_ratio;
                if (paintTool==Tool.CROP){
                    drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                }
                return true;
            }
            if (action==MotionEvent.ACTION_MOVE) {
                if (me.getPointerCount() == 1) {
                    if (me.getHistorySize() > 0) {
                        x_pos = (me.getHistoricalX(0) - x_padding) * scale_ratio;
                        y_pos = (me.getHistoricalY(0) - y_padding) * scale_ratio;
                        int i = 0;
                        while (i < me.getHistorySize() - 2) {
                            i = i + 1;
                            float x_pos1 = (me.getHistoricalX(i) - x_padding) * scale_ratio;
                            float y_pos1 = (me.getHistoricalY(i) - y_padding) * scale_ratio;
                            float x_delta = x_pos1 - x_pos;
                            float y_delta = y_pos1 - y_pos;
                            if (paintTool==Tool.CROP){
                                calculateCutFramePosition(x_pos1,y_pos1,x_delta, y_delta);
                                drawCutFrame(cut_line_left, cut_line_top, cut_line_right, cut_line_bottom, scale_ratio);
                            }
                            if (paintTool==Tool.PAINT){
                                paintBrush(x_pos1,y_pos1);
                            }
                            if (paintTool==Tool.BLUR){
                                blurBrush(x_pos1,y_pos1);
                            }
                            x_pos = x_pos1;
                            y_pos = y_pos1;
                        }
                    }
                    float x_pos1 = (me.getX() - x_padding) * scale_ratio;
                    float y_pos1 = (me.getY() - y_padding) * scale_ratio;
                    float x_delta = x_pos1 - x_pos;
                    float y_delta = y_pos1 - y_pos;
                    if (paintTool==Tool.CROP){
                        calculateCutFramePosition(x_pos1,y_pos1,x_delta, y_delta);
                        drawCutFrame(cut_line_left, cut_line_top, cut_line_right, cut_line_bottom, scale_ratio);
                    }
                    if (paintTool==Tool.PAINT){
                        paintBrush(x_pos1,y_pos1);
                    }
                    if (paintTool==Tool.BLUR){
                        blurBrush(x_pos1,y_pos1);
                    }
                    if (paintTool==Tool.COLORPICKER){
                        setPaintColor(getPixelColor(x_pos,y_pos));
                    }
                    x_pos = x_pos1;
                    y_pos = y_pos1;
                } else {
                    if (me.getHistorySize() > 0) {
                        x_pos = (me.getHistoricalX(0,0) - x_padding) * scale_ratio;
                        y_pos = (me.getHistoricalY(0,0) - y_padding) * scale_ratio;
                        x1_pos = (me.getHistoricalX(1,0) - x_padding) * scale_ratio;
                        y1_pos = (me.getHistoricalY(1,0) - y_padding) * scale_ratio;
                        int i = 0;
                        while (i < me.getHistorySize() - 2) {
                            i = i + 1;
                            float x_pos1 = (me.getHistoricalX(0, i) - x_padding) * scale_ratio;
                            float y_pos1 = (me.getHistoricalY(0, i) - y_padding) * scale_ratio;
                            float x1_pos1 = (me.getHistoricalX(1, i) - x_padding) * scale_ratio;
                            float y1_pos1 = (me.getHistoricalY(1, i) - y_padding) * scale_ratio;
                            float x_delta = x_pos1 - x_pos;
                            float y_delta = y_pos1 - y_pos;
                            float x1_delta = x1_pos1 - x1_pos;
                            float y1_delta = y1_pos1 - y1_pos;
                            if (paintTool==Tool.CROP){
                                calculateCutFrameMultitouch(v, me, i);
                                drawCutFrame(cut_line_left, cut_line_top, cut_line_right, cut_line_bottom, scale_ratio);
                            }
                            x_pos = x_pos1;
                            y_pos = y_pos1;
                            x1_pos = x1_pos1;
                            y1_pos = y1_pos1;
                        }
                    }
                    float x_pos1 = (me.getX(0) - x_padding) * scale_ratio;
                    float y_pos1 = (me.getY(0) - y_padding) * scale_ratio;
                    float x1_pos1 = (me.getX(1) - x_padding) * scale_ratio;
                    float y1_pos1 = (me.getY(1) - y_padding) * scale_ratio;
                    if (paintTool==Tool.CROP){
                        calculateCutFrameMultitouch(v, me);
                        drawCutFrame(cut_line_left, cut_line_top, cut_line_right, cut_line_bottom, scale_ratio);
                    }
                    x_pos = x_pos1;
                    y_pos = y_pos1;
                    x1_pos = x1_pos1;
                    y1_pos = y1_pos1;
                }
                return true;
            }
            if (action==MotionEvent.ACTION_UP){
                x_pos = (me.getX() - x_padding) * scale_ratio;
                y_pos = (me.getY() - y_padding) * scale_ratio;
                float x_pos1 = (me.getX() - x_padding) * scale_ratio;
                float y_pos1 = (me.getY() - y_padding) * scale_ratio;
                float x_delta = x_pos1 - x_pos;
                float y_delta = y_pos1 - y_pos;
                if (paintTool==Tool.CROP){
                    calculateCutFramePosition(x_pos1,y_pos1,x_delta,y_delta);
                    drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                    /*
                    if (pref.smartcrop){
                        crop();
                    }
                     */
                }
                if ((paintTool==Tool.PAINT) || (paintTool==Tool.BLUR) || (paintTool==Tool.TEXT)){
                    // as drawing step finished => save image to cache
                    putImageBitmapToCache(null,false);
                    // invalidate draw path
                    xPaintCache = 0;
                    yPaintCache = 0;
                }
                if (paintTool==Tool.COLORPICKER){
                    // jump back to last paint tool after color was picked
                    setPaintTool(lastPaintTool);
                }
                return true;
            }
            return false;
        }
        });
        seekBar = (SeekBar) findViewById(R.id.seekBar_quality);
        setQualitybarVisibility();
        seekBar.setMax(ImagepipePreferences.getQualitymaxvalue(context));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                updateImagePropertiesText();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // to do
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ImagepipePreferences.setQuality(context,seekBar.getProgress());
                displayImageQuality();
            }
        });
        progressBar = (ProgressBar) findViewById(R.id.main_progress_bar);
        Spinner resizeSpinner = (Spinner) findViewById(R.id.textView_size);
        ArrayAdapter<String> resizeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,ResizeOptions.getResizeOptions(context));
        resizeSpinner.setAdapter(resizeAdapter);
        resizeSpinner.setSelection(ImagepipePreferences.getResizeMode(context),false);
        resizeSpinner.setOnItemSelectedListener(scaleOptionsListener);
        edit_size = (EditText) findViewById(R.id.editText_size);
        edit_size.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // check the text field for a valid resulotion entry (is a valid number, not zero, not negative)
                // and write to preferences if valid
                if (isSizeEntryValid(editable.toString())){
                    try {
                        ImagepipePreferences.setScaleMax(context,Integer.parseInt(editable.toString()));
                        image_needs_to_be_saved = true;
                    } catch (NumberFormatException e){
                        // ignore
                    }
                } else {
                    ImagepipePreferences.setScaleMax(context,Integer.parseInt(ImagepipePreferences.PREF_SCALEMAX_DEFAULT));
                    image_needs_to_be_saved = true;
                }
            }
        });
    }

    private int getExifRotationDataFromMediaStore(Uri image_uri){
        String [] imageColumns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.ORIENTATION};
        Cursor c = this.getContentResolver().query(image_uri,imageColumns,null,null,null);
        if (c == null)
            return -1;
        if (c.moveToFirst()){
            try {
                int i = c.getColumnIndex(imageColumns[1]);
                if (i != -1){
                    int degrees = c.getInt(i);
                    return degrees;
                } else {
                    // orientation-data does not exist.
                    return -1;
                }
            } catch (Exception e) {
                // some other data read error from media store
                return -1;
            }
        }
        c.close();
        return -1;
    }

    private int getExifRotationFromUri(Uri image_uri){
        // try to get rotation from system MediaStore
        int degrees = getExifRotationDataFromMediaStore(image_uri);
        if (degrees !=  -1) {
            return degrees;
        }
        // try to copy stream to file instead
        File tempfile = new File(this.getCacheDir(), TEMP_FILENAME);
        if (tempfile.exists()){
            if (tempfile.length()!=0){
                try {
                    ExifInterface exifInterface = new ExifInterface(tempfile.getAbsolutePath());
                    switch (exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,0)) {
                        case ExifInterface.ORIENTATION_ROTATE_270: degrees = 270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180: degrees = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:  degrees = 90;
                            break;
                        default: degrees = 0;
                            break;
                    }
                    return degrees;
                } catch (Exception e){
                    // reading exif data from file failed, no rotation => returns 0
                }
            }
        }
        // return 0 if the temporary file in cache does not exist.
        return 0;
    }

    public static boolean deleteCacheFile(Context context){
        File tempfile = new File(context.getCacheDir(), TEMP_FILENAME);
        return tempfile.delete();
    }

    private void applyChangesOnLoadedImage(Uri source_uri, boolean updateImageInformation, boolean applyQualityLoss, boolean downscale, final Runnable finishedRunnable){
        next_quality_update_time = 0;
        if (!no_image_loaded){
            if ((downscale) && (ImagepipePreferences.autoScale(context))){
                scaleImage(originalImageContainer.originalWidth, originalImageContainer.originalHeight,false);
            } else
            if (ImagepipePreferences.forceDownscale(context)){
                scaleImage(originalImageContainer.originalWidth, originalImageContainer.originalHeight,true);
            }
            if (ImagepipePreferences.autoRotate(context)) {
                rotateImage(getExifRotationFromUri(source_uri),false);
            }
            // remove transparent pixels if applicable
            if ((ImagepipePreferences.getReplaceColor(context)!=Color.TRANSPARENT) &&
                    (ImagepipePreferences.getCompressFormat(this) != Bitmap.CompressFormat.JPEG)){
                int replaceColor = ImagepipePreferences.getReplaceColor(this);
                for (int x=0; x<bitmap_image.getWidth()-1; x++){
                    for (int y=0; y<bitmap_image.getHeight()-1; y++){
                        if (bitmap_image.getPixel(x,y)==Color.TRANSPARENT){
                            bitmap_image.setPixel(x,y,replaceColor);
                        }
                    }
                }
            }
            if (ImagepipePreferences.autoApplyFilter(context)){
                filterImage();
            }
            resetCutFrame();
            bitmap_original = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
            //clearCache();
            putImageBitmapToCache(finishedRunnable, applyQualityLoss);
            // updating image info is omitted in case of multiple pipe, because the underlying async task
            // runs in a seperate thread and may display wrong information and/or run into an empty bitmap
            // basically, it would lead to running conditions....
            if (updateImageInformation){
                updateImageDisplay();
            }
            image_needs_to_be_saved = true;
        }
    }

    private Boolean pipeTheImage(Intent intent){
        Uri source_uri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (!hasReadWriteStorageRuntimePermission()) {
            missing_permissions_task = MISSING_PERMISSIONS_TASK_pipeTheImage;
            setEmptyDefaultScreen();
            return false;
        }
        if ( source_uri != null ) {
            if (readImageBitmapFromUri(source_uri,false)) {
                // piping must run after the image has been put to cache async, otherwise it may fail.
                Runnable launchAfterImageArrivedInCache = null;
                if ((ImagepipePreferences.autopipe(context)) && (!piping_was_already_launched)) {
                    launchAfterImageArrivedInCache = new Runnable() {
                        @Override
                        public void run() {
                            piping_was_already_launched = true;
                            sendImageUriIntent();
                        }
                    };
                }
                applyChangesOnLoadedImage(source_uri,true, true, true, launchAfterImageArrivedInCache);
                } else {
                showLoadingFailedErrorToast();
                setEmptyDefaultScreen();
                }
        } else {
            setEmptyDefaultScreen();
        }
        return true;
    }

    /**
     * Extracts the uris from an arraylist of ImageContainers into an uri arraylist.
     * Returns null if the parameter is null or if the arraylist is empty.
     *
     * @param imageContainers
     * @return ArrayList<Uri>
     */

    private ArrayList<Uri> getUriArrayList(ArrayList<ImageContainer> imageContainers){
        if (imageContainers!=null) {
            if (imageContainers.size()>0) {
                ArrayList<Uri> urilist = new ArrayList<Uri>();
                for (int i=0; i<imageContainers.size(); i++){
                    if (imageContainers.get(i).uri != null){
                        urilist.add(imageContainers.get(i).uri);
                    } else {
                        // do nothing
                    }
                }
                return urilist;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private void processOneImageForMultiplePipe(final ArrayList<Uri> source_uris, final int i){
        if (i<source_uris.size()){
            if (readImageBitmapFromUri(source_uris.get(i),false)){
                final Context this_context = context;
                Runnable finishedRunnable = null;
                if (!ImagepipePreferences.autosave(this_context)){
                    finishedRunnable = new Runnable() {
                        @Override
                        public void run() {
                            // this is run AFTER the image is saved to cache.
                            // determine target file name using the uri data of the original source that has been put
                            // to orignialImageContainer by readImageBitmarFromUri(source_uris.get(i),false)
                            // copy image from cache to a temporary share folder,
                            // apply the allowed exif tags.
                            // to prevent runtime conflicts, take the fixed cache position, not the last
                            Uri shareFile = ImageWriter.saveShareTempFileFromCache(context,originalImageContainer,exifData,i);
                            final ImageContainer imageContainer = new ImageContainer(this_context,shareFile);
                            multiplePipeList.add(imageContainer);
                            savedImageContainer = imageContainer;
                            // update view in order not to look too frozen
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (bitmap_image!=null){
                                        // finally put last image to cache, store last image uri & update displays
                                        ImagepipePreferences.setSavedImageContainer(context,savedImageContainer);
                                        try {
                                            bitmap_visible = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
                                            image_display.setImageBitmap(bitmap_visible);
                                        } catch (Exception e){
                                            // do nothing
                                        }
                                    }
                                }
                            });
                            updateImagePropertiesText();
                            //
                            processOneImageForMultiplePipe(source_uris,i+1);
                        }
                    };
                }
                applyChangesOnLoadedImage(source_uris.get(i),false, true,true, finishedRunnable);
                // use traditional way for sharing using the mediastore
                if (ImagepipePreferences.autosave(this_context)){
                    final ImageContainer imageContainer = saveImage(false);
                    if (imageContainer != null) {
                        if (useScopedStorage) {
                            // no need to scan images when using scoped storage
                            multiplePipeList.add(imageContainer);
                            savedImageContainer = imageContainer;
                            processOneImageForMultiplePipe(source_uris,i+1);
                        } else {
                            if (imageContainer.file != null) {
                                FileMediaScanner fms = new FileMediaScanner(this_context,imageContainer.file,false){
                                    @Override
                                    public void onScanCompleted(String s, Uri uri){
                                        mediaScannerConnection.disconnect();
                                        imageContainer.uri = uri;
                                        imageContainer.filename = s;
                                        imageContainer.file = new File(s);
                                        imageContainer.filesize = imageContainer.file.length();
                                        multiplePipeList.add(imageContainer);
                                        savedImageContainer = imageContainer;
                                        processOneImageForMultiplePipe(source_uris,i+1);
                                    }
                                };
                            }
                        }
                    }
                }
            } else {
                showLoadingFailedErrorToast();
                // however, do not give up but try with next image
                processOneImageForMultiplePipe(source_uris,i+1);
            }
        } else {
            // here are the tasks done once the recursion finishes (all images are prepared for pipe).
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // finally clear the cache, put last image to cache, store last image uri & update displays.
                    // Exif data of original image was already saved.
                    PictureCache.clearCacheNotShare(context);
                    putImageBitmapToCache(null,false);
                    ImagepipePreferences.setSavedImageContainer(context,savedImageContainer);
                    updateImageDisplay();
                    updateImagePropertiesText();
                }
            });
            ArrayList<Uri> uriArrayList = getUriArrayList(multiplePipeList);
            Log.v("Sharing multiple, temporary uris:");
            Log.v(uriArrayList.toString());
            String[] mimeTypes = new String[1];
            mimeTypes[0]=ImageWriter.getMimeType(this);
            ClipData.Item clipItem = new ClipData.Item(uriArrayList.get(0));
            ClipData clipData = new ClipData("clipdata",mimeTypes,clipItem);
            int pos=1;
            while (pos<uriArrayList.size()) {
                clipData.addItem(new ClipData.Item(uriArrayList.get(pos)));
                pos++;
            }
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND_MULTIPLE);
            String mimeType = ImageWriter.getMimeType(this);
            Log.v("Setting mimeType: "+mimeType);
            intent.setType(mimeType);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                intent.setClipData(clipData);
            }
            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM,uriArrayList);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                // build data intent
                Intent chooserIntent = null;
                // callback only possible if SDK > 21
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    // build IntentSender for callback after an app was chosen
                    //Intent receiverIntent = new Intent(Intent.EXTRA_CHOSEN_COMPONENT);
                    Intent receiverIntent = new Intent(this,IntentReceiver.class);
                    receiverIntent.putExtra(IntentReceiver.IMAGE_COUNT_EXTRA,i);
                    PendingIntent receiverPendingIntent;
                    if (Build.VERSION.SDK_INT >= 31) {
                        receiverPendingIntent = PendingIntent.getBroadcast(this,0,receiverIntent,PendingIntent.FLAG_CANCEL_CURRENT|PendingIntent.FLAG_IMMUTABLE);
                    } else {
                        receiverPendingIntent = PendingIntent.getBroadcast(this,0,receiverIntent,PendingIntent.FLAG_CANCEL_CURRENT);
                    }
                    chooserIntent = Intent.createChooser(intent, getResources().getString(R.string.send_images_part1)+String.valueOf(i)+getResources().getString(R.string.send_images_part2),receiverPendingIntent.getIntentSender());
                } else {
                    chooserIntent = Intent.createChooser(intent, getResources().getString(R.string.send_images_part1)+String.valueOf(i)+getResources().getString(R.string.send_images_part2));
                }
                chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(chooserIntent);
            } catch (ActivityNotFoundException e){
                displayNoActivityFoundToast(ERROR_NO_BULK_IMAGE_APP);
                Log.v("No suitable app found to share multiple images: "+e.getMessage());
                Log.printStackTrace(e);
            }
        }
    }

    private void sendGetResultIntentSingle(ArrayList<Uri> source_uris){
        // todo: check for read/write permissions
        if (readImageBitmapFromUri(source_uris.get(0),false)) {
            if (ImagepipePreferences.autopipe(context)){
                applyChangesOnLoadedImage(source_uris.get(0),true, true,true,launchAfterImageArrivedInCache);
            } else {
                applyChangesOnLoadedImage(source_uris.get(0),true, true,true,null);
            }
        } else {
            showLoadingFailedErrorToast();
            setEmptyDefaultScreen();
        }
    }

    private void buildSendGetMultiple(final ArrayList<Uri> source_uris, final int i){
        if (i<source_uris.size()){
            if (readImageBitmapFromUri(source_uris.get(i),false)) {
                Runnable finishedRunnable = new Runnable() {
                    @Override
                    public void run() {
                        Uri uri = PictureCache.getFileUri(context,i);
                        final ImageContainer imageContainer = new ImageContainer(context,uri);
                        multiplePipeList.add(imageContainer);
                        savedImageContainer = imageContainer;
                        // update view in order not to look too frozen
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (bitmap_image!=null){
                                    // finally put last image to cache, store last image uri & update displays
                                    ImagepipePreferences.setSavedImageContainer(context,savedImageContainer);
                                    try {
                                        bitmap_visible = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
                                        image_display.setImageBitmap(bitmap_visible);
                                    } catch (Exception e){
                                        // do nothing
                                    }
                                }
                            }
                        });
                        updateImagePropertiesText();
                        buildSendGetMultiple(source_uris,i+1);
                    }
                };
                applyChangesOnLoadedImage(source_uris.get(i),false, true,true,finishedRunnable);
            }
        } else {
            ArrayList<Uri> resultUris = getUriArrayList(multiplePipeList);
            // multi-return
            Intent resultIntent = new Intent();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                String[] mimeTypes = new String[1];
                mimeTypes[0]=ImageWriter.getMimeType(context);
                // this adds the uri from the FileProvider to the clipdata
                ClipData.Item clipItem = new ClipData.Item(PictureCache.getFileUri(context,0));
                ClipData clipData = new ClipData("clipdata",mimeTypes,clipItem);
                for (int pos=1; pos<resultUris.size(); pos++){
                    clipItem = new ClipData.Item(resultUris.get(pos));
                    clipData.addItem(clipItem);
                }
                resultIntent.setClipData(clipData);
            }
            resultIntent.setAction(Intent.ACTION_SEND);
            resultIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM,resultUris);
            resultIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            setResult(Activity.RESULT_OK,resultIntent);
            finish();
        }
    }

    private void displayNoActivityFoundToast(int error){
        switch (error) {
            case ERROR_NO_IMAGE_APP: Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.no_image_app_error),Toast.LENGTH_LONG).show(); break;
            case ERROR_NO_BULK_IMAGE_APP: Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.no_bulk_image_app_error),Toast.LENGTH_LONG).show(); break;
        }
    }

    private ArrayList<Uri> getMultipleUriArrayList(Intent intent){
        ArrayList<Uri> source_uris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        // this code takes into account the picker result for multiple images
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            ClipData clipData = intent.getClipData();
            if (clipData!=null){
                if (clipData.getItemCount()>0){
                    source_uris = new ArrayList<Uri>();
                    for (int i=0; i<clipData.getItemCount(); i++){
                        source_uris.add(clipData.getItemAt(i).getUri());
                    }
                }
            }
        }
        if (source_uris==null){
            source_uris = new ArrayList<Uri>();
            Uri uri = intent.getData();
            if (uri!=null){
                source_uris.add(uri);
            }
        }
        return source_uris;
    }

    private Boolean pipeMultipleImages(Intent intent){
        if (!hasReadWriteStorageRuntimePermission()) {
            call_intent = intent;
            missing_permissions_task = MISSING_PERMISSIONS_TASK_bulkpipe;
            setEmptyDefaultScreen();
            return null;
        }
        ArrayList<Uri> source_uris = getMultipleUriArrayList(intent);
        if (source_uris != null) {
            multiplePipeList = new ArrayList<ImageContainer>();
            ImageWriter.clearShareTempDir(context);
            processOneImageForMultiplePipe(source_uris,0);
        } else {
            setEmptyDefaultScreen();
        }
        return true;
    }

    private void reloadNoShrink() {
        if (loadingImageIsRunning) {
            return;
        }
        loadingImageIsRunning = true;
        if (!no_image_loaded) {
            // try to restore from uri
            if (readImageBitmapFromUri(originalImageContainer.uri,true)) {
                clearCache();
                // clear image cache and restore original to first position, this is done in applyChangesOnLoadedImage
                applyChangesOnLoadedImage(originalImageContainer.uri, false, false, false, null);
                updateImagePropertiesText();
                initOriginalBitmap();
                bitmap_visible = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
                putImageBitmapToCache(null,false);
                image_needs_to_be_saved = true;
                resetCutFrame();
            }
        } else {
            showLoadingFailedErrorToast();
        }
    }

    private void reloadImage(){
        if (loadingImageIsRunning){
            return;
        }
        loadingImageIsRunning = true;
        if (!no_image_loaded) {
            // try to restore from uri
            if (readImageBitmapFromUri(originalImageContainer.uri,false)){
                clearCache();
                // clear image cache and restore original to first position, this is done in applyChangesOnLoadedImage
                applyChangesOnLoadedImage(originalImageContainer.uri,false, false,true,null);
                image_needs_to_be_saved = true;
                updateImagePropertiesText();
                resetCutFrame();
                initOriginalBitmap();
                bitmap_visible = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
                image_needs_to_be_saved = true;
            } else {
                // if loading bitmap fails, try to restore from 1st cache position
                Bitmap bitmap = PictureCache.restoreFromPictureCache(context,0);
                if (bitmap!=null){
                    // if restored from cache, restore exif data from preferences.
                    // The exif data was written when reading the image from uri.
                    // exifInterface = PictureCache.getExifInterface(context,0);
                    // ExifData exifData = ImagepipePreferences.readExifData(context);
                    /*
                    if (exifData!=null) {
                        exifData.fillExifInterfaceWithData(exifInterface,false);
                    }
                     */
                    bitmap_original = bitmap;
                    bitmap_image = bitmap.copy(Bitmap.Config.ARGB_8888,true);
                    bitmap_visible = bitmap.copy(Bitmap.Config.ARGB_8888,true);
                    PictureCache.clearPictureCache(context);
                    cachePosition = 0;
                    updateImageDisplay();
                    updateImagePropertiesText();
                    image_needs_to_be_saved = true;
                    resetCutFrame();
                    image_needs_to_be_saved = true; // x!
                } else {
                    // try to restore from bitmap in memory
                    if (bitmap_original!=null){
                        bitmap_image = bitmap_original.copy(Bitmap.Config.ARGB_8888,true);
                        bitmap_visible = bitmap_original.copy(Bitmap.Config.ARGB_8888,true);
                        image_needs_to_be_saved = true; // x!
                        clearCache();
                        putImageBitmapToCache(null,false);
                        updateImageDisplay();
                        updateImagePropertiesText();
                    } else {
                        showLoadingFailedErrorToast();
                    }
                }
            }
        }
    }

    private int getResizeTextFieldValue(){
        String scaleValueText = ImagepipePreferences.PREF_SCALEMAX_DEFAULT;
        int maxsize;
        boolean textFieldChanged = false;
        // try to get the scale value directly from the text field
        if (edit_size != null) {
            try {
                maxsize = Integer.parseInt(ImagepipePreferences.getScaleMax(context));
                scaleValueText = ImagepipePreferences.getScaleMax(context);
            } catch (NumberFormatException e) {
                // textfield is invalid, needs a refresh
                textFieldChanged = true;
            }
        } else {
            scaleValueText = ImagepipePreferences.getScaleMax(context);
        }
        // if this fails, we use the preference default entry (assigned above as default)
        try {
            maxsize = Integer.parseInt(scaleValueText);
        } catch (NumberFormatException e){
            // if the preference entry is corrupt, we set the default
            maxsize = 1024;
            textFieldChanged = true;
        }
        // we finally check for zero
        if (maxsize==0){
            maxsize = 1024;
            textFieldChanged = true;
        }
        // we eliminate negative values
        if (maxsize<0){
            maxsize = Math.abs(maxsize);
            textFieldChanged = true;
        }
        // finally update EditText if value force-changed
        if (textFieldChanged){
            if (edit_size!=null){
                edit_size.setText(String.valueOf(maxsize));
            }
        }
        return maxsize;
    }

    private Point getForceImagesizeResolution(){
        Display d = getWindowManager().getDefaultDisplay();
        Point po = new Point();
        d.getSize(po);
        return po;
    }

    private int getForceImagesize(){
        Point point = getForceImagesizeResolution();
        return Math.max(point.x,point.y);
    }

    private Point getTargetImageResolution(final int width, final int height, boolean forceScaleOnly){
        int target_height = height;
        int target_width = width;
        int maxsize = getResizeTextFieldValue();
        if (!forceScaleOnly){
            int resizeMode = ImagepipePreferences.getResizeMode(context);
            if (resizeMode == ResizeOptions.MAX){
                if (height>width){
                    resizeMode = ResizeOptions.HEIGHT;
                } else {
                    resizeMode = ResizeOptions.WIDTH;
                }
            }
            if (resizeMode == ResizeOptions.MIN){
                if (height>width){
                    resizeMode = ResizeOptions.WIDTH;
                } else {
                    resizeMode = ResizeOptions.HEIGHT;
                }
            }
            if (resizeMode==ResizeOptions.HEIGHT){
                target_height = maxsize;
                target_width = Math.round((float) target_height / (float) height * (float) width);
            }
            if (resizeMode==ResizeOptions.WIDTH){
                target_width = maxsize;
                target_height = Math.round((float) target_width / (float) width * (float) height);
            }
            if (resizeMode==ResizeOptions.PERCENT){
                target_width = Math.round((float) width * (maxsize/100f));
                target_height = Math.round((float) height * (maxsize/100f));
            }
        }
        // check for a necessary force-resize to the screen dimensions
        if (ImagepipePreferences.forceDownscale(context)){
            Point point = getForceImagesizeResolution();
            int maxHeight = point.y;
            int maxWidth = point.x;
            int maxDiameter = maxHeight;
            if (maxWidth > maxDiameter){
                maxDiameter = maxWidth;
            }
            if (target_height>maxDiameter){
                target_height = maxDiameter;
                target_width = Math.round((float) target_height / (float) height * (float) width);
            }
            if (target_width>maxDiameter){
                target_width = maxDiameter;
                target_height = Math.round((float) target_width / (float) width * (float) height);
            }
        }
        return new Point(target_width,target_height);
    }

    private boolean scaleImage(int originalWidth, int originalHeight, boolean forceScaleOnly){
        if ((!no_image_loaded) && (!(forceScaleOnly && !ImagepipePreferences.forceDownscale(context)))){
            Point point = getTargetImageResolution(originalWidth,originalHeight,forceScaleOnly);
            int target_width = point.x;
            int target_height = point.y;
            imageScaleFactor = (float) target_width / (float) bitmap_image.getWidth();
            try {
                // perform operation on a *new* bitmap to avoid nullifying bitmap_image when out of memory
                Bitmap bitmapTemp = Bitmap.createScaledBitmap(bitmap_image, target_width, target_height, ImagepipePreferences.isBilinearFilteringEnabled(context));
                if (bitmapTemp!=null){
                    bitmap_image = bitmapTemp.copy(Bitmap.Config.ARGB_8888,true);
                }
                image_needs_to_be_saved = true;
            } catch (OutOfMemoryError e){
                return false;
            }
        }
        return true;
    }

    private Boolean rotateImage(float degree, boolean cropAfterRotation){
        if (degree == 0){
            return true;
        }
        if (!no_image_loaded){
            Matrix matrix = new Matrix();
            matrix.postRotate(degree);
            try {
                // perform bitmap operation on a *new* bitmap to avoid nullifying bitmap_image when out of memory
                Bitmap bitmapTemp = Bitmap.createBitmap(bitmap_image,0,0,bitmap_image.getWidth(),bitmap_image.getHeight(),matrix,true);
                if (bitmapTemp!=null){
                    bitmap_image = bitmapTemp.copy(Bitmap.Config.ARGB_8888,true);
                    if (cropAfterRotation) {
                    /*
                    Canvas canvas = new Canvas(bitmap_image);
                    Paint paint = new Paint();
                    paint.setColor(Color.YELLOW);
                    paint.setStyle(Paint.Style.STROKE);
                    int x = (bitmap_image.getWidth()-originalWidth);
                    int y = (bitmap_image.getHeight()-originalHeight);
                    canvas.drawRect(x,y,bitmap_image.getWidth()-x,bitmap_image.getHeight()-y,paint);
                     */
                        float gradient = ((float) bitmap_image.getHeight()) / ((float) bitmap_image.getWidth());
                        int x = 0;
                        int y = 0;
                        while ((bitmap_image.getPixel(x, y) == 0) && (x<bitmap_image.getWidth()/2)) {
                            x++;
                            y = Math.round(gradient * x);
                        }
                        int topLeftX = x;
                        int topLeftY = y;
                        //canvas.drawCircle(x,y,25,paint);
                        x = 0;
                        y = bitmap_image.getHeight() - 1;
                        while ((bitmap_image.getPixel(x, y) == 0) && (x<bitmap_image.getWidth()/2)) {
                            x++;
                            y = bitmap_image.getHeight() - Math.round(gradient * x);
                        }
                        int bottomLeftX = x;
                        int bottomLeftY = y;
                    /*
                    canvas.drawCircle(x,y,25,paint);
                    canvas.drawLine(0,0,bitmap_image.getWidth(),bitmap_image.getHeight(),paint);
                    canvas.drawLine(0,bitmap_image.getHeight(),bitmap_image.getWidth(),0,paint);
                    paint.setColor(Color.CYAN);
                    canvas.drawRect(topLeftX,topLeftY,bitmap_image.getWidth()-topLeftX,bitmap_image.getHeight()-topLeftY,paint);
                    paint.setColor(Color.BLUE);
                    canvas.drawRect(bottomLeftX,bottomLeftY,bitmap_image.getWidth()-bottomLeftX,bitmap_image.getHeight()-bottomLeftY,paint);
                     */
                        float area1 = Math.abs((bitmap_image.getWidth() - topLeftX * 2) * (bitmap_image.getHeight() - topLeftY * 2));
                        float area2 = Math.abs((bitmap_image.getWidth() - bottomLeftX * 2) * (bitmap_image.getHeight() - bottomLeftY * 2));
                        //paint.setColor(Color.WHITE);
                        //paint.setStyle(Paint.Style.STROKE);
                        if (area1 < area2) {
                            //canvas.drawRect(topLeftX,topLeftY,bitmap_image.getWidth()-topLeftX,bitmap_image.getHeight()-topLeftY,paint);
                            bitmap_image = Bitmap.createBitmap(bitmap_image, topLeftX, topLeftY, bitmap_image.getWidth() - topLeftX * 2, bitmap_image.getHeight() - topLeftY * 2);

                        } else {
                            //canvas.drawRect(bottomLeftX,bottomLeftY,bitmap_image.getWidth()-bottomLeftX,bitmap_image.getHeight()-bottomLeftY,paint);
                            //canvas.drawCircle(bottomLeftX,bitmap_image.getHeight()-bottomLeftY,10,paint);
                            bitmap_image = Bitmap.createBitmap(bitmap_image, bottomLeftX, bitmap_image.getHeight() - bottomLeftY, bitmap_image.getWidth() - bottomLeftX * 2, bitmap_image.getHeight() - (bitmap_image.getHeight() - bottomLeftY) * 2);

                        }
                    }
                }
                updateImageDisplay();
                image_needs_to_be_saved = true;
                updateImagePropertiesText();
            } catch (OutOfMemoryError e){
                return false;
            }
        }
        return true;
    }

    private class processJpegInBackground extends AsyncTask<Integer,Void,Boolean>{
        ByteArrayOutputStream out_stream;
        long launchTime;
        protected Boolean doInBackground(Integer... compression){
            launchTime = Calendar.getInstance().getTimeInMillis();
            if (bitmap_image != null){
                try {
                    out_stream = new ByteArrayOutputStream();
                } catch (OutOfMemoryError e) {
                    return false;
                }
                bitmap_visible = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
                try {
                    bitmap_visible.compress(ImagepipePreferences.getCompressFormat(context,compression[0]), compression[0], out_stream);
                } catch (OutOfMemoryError e){
                    return false;
                }
                try {
                    out_stream.close();
                } catch (Exception e){
                    // nothing to do
                }
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean jpegresult) {
            super.onPostExecute(jpegresult);
            if (jpegresult){
                ByteArrayInputStream byis;
                try {
                     byis = new ByteArrayInputStream(out_stream.toByteArray());
                } catch (OutOfMemoryError e){
                    jpeg_ecoded_filesize = 0;
                    return;
                }
                try {
                    // do this only if no changes on image occurred since launch, otherwise ignore result
                    if (imageLastModified<launchTime){
                        Bitmap bitmap_immutable = BitmapFactory.decodeStream(byis);
                        bitmap_visible = bitmap_immutable.copy(Bitmap.Config.ARGB_8888,true);
                        image_display.setImageBitmap(bitmap_visible);
                    }
                } catch (OutOfMemoryError e){
                    jpeg_ecoded_filesize = 0;
                    return;
                }
                try {
                    byis.close();
                } catch (Exception e){
                    // nothing to do
                }
                jpeg_ecoded_filesize = out_stream.size();
                drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                next_quality_update_time = new Date().getTime() + NEXT_UPDATE_TIME_DELAY;
                updateImagePropertiesText();
            }
            quality_update_is_dispatched = false;
        }
    }

    private void displayImageQuality(){
        if (!no_image_loaded) {
            if (new Date().getTime() >= next_quality_update_time) {
                new processJpegInBackground().execute(ImagepipePreferences.getQuality(context));
            } else {
                if (!quality_update_is_dispatched) {
                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            displayImageQuality();
                        }
                    }, next_quality_update_time - new Date().getTime());
                    quality_update_is_dispatched = true;
                }
            }
        }
    }

    private boolean hasReadWriteStorageRuntimePermission(){
        if (useScopedStorage){
            if (android.os.Build.VERSION.SDK_INT >= 26) {
                if (android.os.Build.VERSION.SDK_INT >= 33) {
                    // request only read external storage
                    if (this.checkSelfPermission(Manifest.permission.READ_MEDIA_IMAGES) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_MEDIA_IMAGES}, PERMISSION_CALLBACK);
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    // request read external storage
                    if (this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CALLBACK);
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        } else {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
            // handle runtime permissions only if android is >= Marshmellow api 23
            if (this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},PERMISSION_CALLBACK);
                    // at this moment, we request the permission and leave with a negative result. The user needs to try saving again after permission was granted.
                    return false;
                }
                else
                {
                    // permission is granted, ok
                    return true;
                }
            }
        }
    // before api 23, permissions are always granted, so everything is ok
            return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int permRequestCode, String perms[], int[] grantRes){
        if (permRequestCode == PERMISSION_CALLBACK){
            int i;
            for (i=0; i<grantRes.length; i++){
                if (android.os.Build.VERSION.SDK_INT >= 33) {
                    if (perms[i].equals(Manifest.permission.READ_MEDIA_IMAGES)){
                        if (grantRes[i]==PackageManager.PERMISSION_GRANTED){
                            hasReadStoragePermission=true;
                        } else {
                            hasReadStoragePermission=false;
                        }
                    }
                }
                if (perms[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)){
                    if (grantRes[i]==PackageManager.PERMISSION_GRANTED){
                        hasReadStoragePermission=true;
                    } else {
                        hasReadStoragePermission=false;
                    }
                }
                if (perms[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    if (grantRes[i]==PackageManager.PERMISSION_GRANTED){
                        hasWriteStoragePermission=true;
                    } else {
                        hasWriteStoragePermission=false;
                    }
                }
            }
            if ((useScopedStorage && hasReadStoragePermission) || (hasReadStoragePermission && hasWriteStoragePermission)) {
                if (missing_permissions_task == MISSING_PERMISSIONS_TASK_sendImageUriIntent) {
                    missing_permissions_task = 0;
                    sendImageUriIntent();
                }
                if (missing_permissions_task ==  MISSING_PERMISSIONS_TASK_pipeTheImage){
                    missing_permissions_task = 0;
                    pipeTheImage(call_intent);
                }
                if (missing_permissions_task ==  MISSING_PERMISSIONS_TASK_bulkpipe){
                    missing_permissions_task = 0;
                    pipeMultipleImages(call_intent);
                }
                if (missing_permissions_task ==  MISSING_PERMISSIONS_TASK_saveImage){
                    missing_permissions_task = 0;
                    saveAndScanImage(true);
                }
            } else {
                if (this.shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        this.shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    showPermissionsRationale();
                }
            }

        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void showPermissionsRationale(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (useScopedStorage){
            builder.setView(R.layout.dialogpermissionrationale2);
        } else {
            builder.setView(R.layout.dialogpermissionrationale);
        }
        builder.setTitle(R.string.permissions_title);
        builder.setCancelable(true);
        builder.setNeutralButton(R.string.permissions_ok_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void sendImageUriIntent() {
        if (!ImagepipePreferences.autosave(context)){
            shareImage(originalImageContainer);
        } else {
            // traditional way to share using the media store
            if (!hasReadWriteStorageRuntimePermission()) {
                missing_permissions_task = MISSING_PERMISSIONS_TASK_sendImageUriIntent;
                // break to exit sending here; it will be resumed through the permission callback if necessary
                return;
            }
            if (image_needs_to_be_saved) {
                ImageContainer imageContainer = saveImage(false);
                if (imageContainer != null){
                    // when using scoped storage, file is already in the mediastore and needs not to be scanned.
                    if (useScopedStorage){
                        // store current uri data, because scanning is not necessary
                        savedImageContainer = new ImageContainer(imageContainer);
                        shareImage(imageContainer);
                    } else {
                        // scan file when not using scoped storage
                        if (imageContainer.file != null){
                            new FileMediaScanner(getApplicationContext(),imageContainer.file,true);
                        }
                    }
                } else {
                }
            } else {
                if (savedImageContainer != null) {
                    if (savedImageContainer.uri != null) {
                        shareImage(savedImageContainer);
                    }
                }
            }

        }
    }

    private ImageContainer saveImage(Boolean display_save_result){
        if (no_image_loaded){
            Toast.makeText(this, R.string.toast_load_image_first, Toast.LENGTH_LONG).show();
            return null;
        }
        ImageContainer targetImageContainer;
        if ((image_needs_to_be_saved) || (savedImageContainer==null)){
            if (useScopedStorage) {
                targetImageContainer = ImageWriter.toMediaStore(context,originalImageContainer);
            } else {
                targetImageContainer = ImageWriter.toFile(context,originalImageContainer);
            }
            int compression_rate = ImagepipePreferences.getQuality(context);
            if (!ImageWriter.saveBitmapToUri(context,bitmap_image,targetImageContainer.uri,compression_rate,exifData)){
                Toast.makeText(this, R.string.toast_error_save_failed +" scoped storage: "+useScopedStorage, Toast.LENGTH_LONG).show();
                return null;
            }
            image_needs_to_be_saved = false;
            if (display_save_result){
                Toast.makeText(this, getResources().getString(R.string.toast_save_successful),Toast.LENGTH_LONG).show();
            }
        } else {
            if (display_save_result) {
                Toast.makeText(this, getResources().getString(R.string.toast_save_not_necessary), Toast.LENGTH_LONG).show();
            }
            return savedImageContainer;
        }
        return targetImageContainer;
    }

    private void saveAndScanImage(Boolean display_save_result){
        if (!hasReadWriteStorageRuntimePermission()) {
            missing_permissions_task = MISSING_PERMISSIONS_TASK_saveImage;
        } else {
            if ( (!ImagepipePreferences.warningPermanentlyDisabled(context)) &&
                    (!ImagepipePreferences.warningTemporarilyDisabled(context)) &&
                    (ImagepipePreferences.keepSomeExifTags(context))){
                showExifWarning(null,
                        getResources().getString(R.string.info_back),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                saveAndScan(true);
                                dialogInterface.dismiss();
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ImagepipePreferences.setPrefAllowedTags(context,"");
                                image_needs_to_be_saved = true; // need to save again without the exif tags!
                                updateImageDisplay();
                                saveAndScan(true);
                                dialogInterface.dismiss();
                            }
                        });
            } else {
                saveAndScan(true);
            }
        }
    }

    private void saveAndScan(Boolean display_save_result){
        //ImageContainer imageContainer = saveImage(display_save_result);
        savedImageContainer = saveImage(display_save_result);
        if (savedImageContainer != null){
            if (savedImageContainer.file != null){
                // scan image, update lastImageContainer and do not launch share
                FileMediaScanner fms = new FileMediaScanner(getApplicationContext(),savedImageContainer.file,false);
            }
        }
    }

    private void shareImage(ImageContainer imageContainer){
        Log.v("Building the single share intent:");
        Intent i = new Intent();
        i.setAction(Intent.ACTION_SEND);
        // build the intent from the FileContentProvider from cache without the media store
        if (!ImagepipePreferences.autosave(this)){
            ImageWriter.clearShareTempDir(context);
            Uri uri = ImageWriter.saveShareTempFileFromLastCachePosition(context,imageContainer,exifData);
            i.setDataAndType(uri,ImageWriter.getMimeType(this));
            i.putExtra(Intent.EXTRA_STREAM, uri);
            i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (uri!=null){
                Log.v("Put uri and set flags from local cache folder: "+uri.toString());
            }
        } else {
            // build the intent in the classic way from the media store
            i.setDataAndType(imageContainer.uri, ImageWriter.getMimeType(this));
            i.putExtra(Intent.EXTRA_STREAM, imageContainer.uri);
            i.setType(ImageWriter.getMimeType(this));
            Log.v("Put uri from media store: "+imageContainer.uri.toString());
        }
        try {
            Intent receiverIntent = new Intent(this,IntentReceiver.class);
            PendingIntent receiverPendingIntent;
            if (Build.VERSION.SDK_INT >= 31) {
                receiverPendingIntent = PendingIntent.getBroadcast(this,0,receiverIntent,PendingIntent.FLAG_CANCEL_CURRENT|PendingIntent.FLAG_IMMUTABLE);
            } else {
                receiverPendingIntent = PendingIntent.getBroadcast(this,0,receiverIntent,PendingIntent.FLAG_CANCEL_CURRENT);
            }
            Log.v("Starting activity to create chooser...");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                startActivity(Intent.createChooser(i, getResources().getString(R.string.send_image),receiverPendingIntent.getIntentSender()));
            } else {
                startActivity(Intent.createChooser(i, getResources().getString(R.string.send_image)));
            }
        } catch (ActivityNotFoundException e){
            Log.v("No app found that can share images! Chooser cannot be created.");
            displayNoActivityFoundToast(ERROR_NO_IMAGE_APP);
        }
    }

    private class FileMediaScanner implements MediaScannerConnection.MediaScannerConnectionClient{

        private File file;
        private Boolean share;
        public MediaScannerConnection mediaScannerConnection;

        public FileMediaScanner(Context c, File f, Boolean s){
            share = s;
            file = f;
            mediaScannerConnection = new MediaScannerConnection(context,this);
            mediaScannerConnection.connect();
        }

        @Override
        public void onMediaScannerConnected() {
           mediaScannerConnection.scanFile(file.getAbsolutePath(),null);
        }

        @Override
        public void onScanCompleted(String s, Uri uri) {
            mediaScannerConnection.disconnect();
            // lastImageContainer needs to be updated with the content-uri. Otherwise, sharing will fail on
            // API > 24.
            savedImageContainer = new ImageContainer(context,uri);
            // it is save to generate a file, because this will never be called when using scoped storage
            savedImageContainer.file = new File(s);
            if (share) {
                shareImage(savedImageContainer);
            }
        }
    }

    private String getKilobyteString(long l){
        l = l / 1000;
        BidiHelper bidiHelper = new BidiHelper(context);
        if (bidiHelper.isRTL()){
            return getResources().getString(R.string.kilobyte)+" "+String.valueOf(l);
        }
        return String.valueOf(l)+" kB";
    }

    private void updateImagePropertiesText(){
        String message = "";
        BidiHelper bidiHelper = new BidiHelper(context);
        final TextView infotext = (TextView) findViewById(R.id.imageproperties_text);
        if ((!no_image_loaded) && (bitmap_image!=null)){
            String filesize ="";
            String shrinkRatio="";
            if ((ImagepipePreferences.previewQuality(context)) && (originalImageContainer != null)){
                filesize = getResources().getString(R.string.size)+" "+getKilobyteString(jpeg_ecoded_filesize);
                if ((originalImageContainer.filesize != 0) && (jpeg_ecoded_filesize != 0)){
                    if (!bidiHelper.isRTL()){
                        shrinkRatio = " ("+String.valueOf((jpeg_ecoded_filesize *100)/ originalImageContainer.filesize)+"%)";
                    } else {
                        shrinkRatio = " (%"+String.valueOf((jpeg_ecoded_filesize *100)/ originalImageContainer.filesize)+")";
                    }
                } else shrinkRatio = "";
            } else filesize = "";
              String sizeinfo = filesize + shrinkRatio;
              message = getResources().getString(R.string.width)+" "+String.valueOf(bitmap_image.getWidth())+" "+getResources().getString(R.string.height)+" "+String.valueOf(bitmap_image.getHeight())+" "+getResources().getString(R.string.quality)+" "+String.valueOf(ImagepipePreferences.getQuality(context))+" "+sizeinfo+debug;
            if ((actionBar!=null) && (originalImageContainer!=null)){
                if (originalImageContainer.filename!=null){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            actionBar.setSubtitle(originalImageContainer.filename);
                        }
                    });
                }
            }
        } else {
            // keep message empty, nothing to do
            if (actionBar!=null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        actionBar.setSubtitle("");
                    }
                });
            }
        }
        final String s = message;
                runOnUiThread(new Runnable() {
            @Override
            public void run() {
                infotext.setText(s);
            }
        });
    }

    private void initOriginalBitmap(){
        if (bitmap_image!=null){
            try {
                bitmap_original = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
            } catch (Exception e){
                // out of memory; keep bitmap_original empty
                bitmap_original = null;
            }
        }
    }

    private void setEmptyDefaultScreen() {
        // clear cache because there is no image.
        clearCache();
        View main_view = getWindow().getDecorView().getRootView();
        main_view.post(new Runnable(){
            @Override
            public void run(){
                Bitmap add_icon_blue = BitmapFactory.decodeResource(getResources(),R.mipmap.ic_choose_blue,null);
                int x = image_display.getWidth();
                int y = image_display.getHeight();
                if ((x!=0) && (y!=0)){
                    Bitmap original = Bitmap.createBitmap(x, y, Bitmap.Config.ARGB_8888);
                    Canvas no_image_canvas = new Canvas();
                    no_image_canvas.setBitmap(original);
                    x1_choose_button = (x-add_icon_blue.getWidth())/2;
                    y1_choose_button = (y-add_icon_blue.getHeight())/2;
                    x2_choose_button = (x+add_icon_blue.getWidth())/2;
                    y2_choose_button = (y+add_icon_blue.getHeight())/2;
                    Paint paint = new Paint();
                    paint.setAntiAlias(true);
                    no_image_canvas.drawBitmap(add_icon_blue,x1_choose_button,y1_choose_button,paint);
                    paint.setColor(GetAppColor(context,R.color.primaryTextColor));
                    int text_size = y / 10;
                    paint.setTextSize(text_size);
                    while (paint.measureText(getResources().getString(R.string.empty_imageview_text))>x-x/20) {
                        text_size = text_size - 1;
                        paint.setTextSize(text_size);
                    }
                    no_image_canvas.drawText(getResources().getString(R.string.empty_imageview_text), x/40, (y-add_icon_blue.getHeight())/2 - text_size*2,paint);
                    if (original != null) {
                        bitmap_image = original.copy(Bitmap.Config.ARGB_8888,true);
                        updateImageDisplay();
                        no_image_loaded = true;
                        resetCutFrame();
                    }
                    showNewCanvasButton();
                } else {
                    // do nothing
                }
            }});
    }

    private void resetCutFrame(){
        cut_line_bottom = bitmap_image.getHeight();
        cut_line_left = 0;
        cut_line_right = bitmap_image.getWidth();
        cut_line_top = 0;
        scale_ratio = getScaleRatio();
    }

    private void pickImageFromGallery(int callback){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
         if (android.os.Build.VERSION.SDK_INT >= 18){
             intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
         }
         try {
             Intent chooserIntent = Intent.createChooser(intent,getResources().getString(R.string.empty_imageview_text));
             startActivityForResult(chooserIntent,callback);
         } catch (ActivityNotFoundException e){
             displayNoActivityFoundToast(ERROR_NO_IMAGE_APP);
         }
    }

    private boolean readImageBitmapFromUri(Uri source_uri, boolean noShrink){
        long original_image_filesize = 0;
        int originalWidth = 0;
        int originalHeight = 0;
        try {
            // free memory
            bitmap_original = null; bitmap_image = null; bitmap_visible = null;
            BufferedInputStream image_inputstream = new BufferedInputStream(getContentResolver().openInputStream(source_uri));
            File tempfile = new File(getApplicationContext().getCacheDir(),TEMP_FILENAME);
            BufferedOutputStream fileOutputStream = new BufferedOutputStream(new FileOutputStream(tempfile));
            byte [] bytebuffer = new byte[1024];
            int i = 0;
            while ((i = image_inputstream.read(bytebuffer)) != -1){
                fileOutputStream.write(bytebuffer,0,i);
                original_image_filesize = original_image_filesize + i;
            }
            fileOutputStream.flush(); fileOutputStream.close();
            image_inputstream.close();
            ExifInterface exifInterface = new ExifInterface(tempfile.getAbsolutePath());
            // save exif data to be restored later if image is restored from cache
            exifData = ImagepipePreferences.saveExifData(context,exifInterface);
            // determine sampleSize
            int neededSampleSize = 1;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            FileInputStream fileInputStream = new FileInputStream(tempfile);
            BitmapFactory.decodeStream(fileInputStream,null,options);
            fileInputStream.close();
            originalWidth = options.outWidth;
            originalHeight = options.outHeight;
            Point point;
            if ((ImagepipePreferences.autoScale(context)) &&(!noShrink)) {
                point = getTargetImageResolution(options.outWidth, options.outHeight, false);
            } else {
                point = getTargetImageResolution(options.outWidth, options.outHeight, true);
            }
            int maxWidth = point.x;
            int maxHeight = point.y;
            while ((options.outWidth/neededSampleSize > maxWidth) && (options.outHeight/neededSampleSize > maxHeight)){
                neededSampleSize = neededSampleSize * 2;
            }
            neededSampleSize = neededSampleSize / 2;
            BitmapFactory.Options optionsDecode = new BitmapFactory.Options();
            optionsDecode.inMutable=true;
            optionsDecode.inSampleSize = neededSampleSize;
            if (ImagepipePreferences.isBilinearFilteringEnabled(context)){
                optionsDecode.inPreferQualityOverSpeed = true;
            }
            fileInputStream = new FileInputStream(tempfile);
            bitmap_image = BitmapFactory.decodeStream(fileInputStream,null,optionsDecode);
            // if BitmapFactory.decodeStream fails, try to use ImageDecoder.decode
            if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) && (bitmap_image==null)) {
                /* The ImageDecoder cannot handle streams, therefore the image needs to be read to ram
                 * before decoding. To avoid OutOfMemory errors, the reading of the stream is cancelled
                 * if the stream data exceeds 25% of the "low memory state" threshold for the device. This
                 * should be more than sufficient to load normal images.
                 */
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int bufferSize;
                long streamSize = 0;
                ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                if (activityManager!=null){
                    ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                    activityManager.getMemoryInfo(memoryInfo);
                    long maxStreamSize = Math.round(memoryInfo.threshold/4f);
                    while (((bufferSize=fileInputStream.read(buffer,0,buffer.length)) != -1) && (streamSize<maxStreamSize)){
                        byteArrayOutputStream.write(buffer,0,bufferSize);
                        streamSize = streamSize + bufferSize;
                    }
                    byteArrayOutputStream.flush();
                    if (streamSize<maxStreamSize){
                        ImageDecoder.Source source = ImageDecoder.createSource(getContentResolver(),source_uri);
                        bitmap_image = ImageDecoder.decodeBitmap(source);
                    }
                }
                byteArrayOutputStream.close();
            }
            fileInputStream.close();
        }
        catch (Exception e){
            return false;
        }
        // catch the case that the file was read correctly, but BitmapFactory.decodeStream returned a null
        // value because the image stream is in an unknown format and/or corrupt.
        if (bitmap_image==null){
            return false;
        }
        // save original source & filesize
        originalImageContainer = new ImageContainer(getApplicationContext(),source_uri,original_image_filesize);
        originalImageContainer.originalWidth = originalWidth;
        originalImageContainer.originalHeight = originalHeight;
        ImagepipePreferences.setOriginalImageContainer(context,originalImageContainer);
        no_image_loaded = false;
        image_needs_to_be_saved = true;
        loadingImageIsRunning = false;
        return true;
    }

    public void showLoadingFailedErrorToast(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, R.string.toast_error_image_not_loaded, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int rc, int rescode, Intent call_intent){
        Log.v("onActivityResult action: "+rc+" resultCode: "+rescode);
        if (rc==SELECT_FROM_GALLERY_GET_CONTENT_CALLBACK){
            final ArrayList<Uri> source_uris = getMultipleUriArrayList(call_intent);
            if (source_uris!=null){
                if (source_uris.size()>0){
                        if (source_uris.size()==1){
                            sendGetResultIntentSingle(source_uris);
                        } else {
                            multiplePipeList = new ArrayList<ImageContainer>();
                            buildSendGetMultiple(source_uris,0);
                        }
                } else {
                    setResult(Activity.RESULT_CANCELED); finish();
                }
            } else {
                setResult(Activity.RESULT_CANCELED); finish();
            }
        }
        if ((rc==SELECT_FROM_GALLERY_CALLBACK) && (rescode==RESULT_OK)){
            Uri i_uri = call_intent.getData();
            if (android.os.Build.VERSION.SDK_INT >= 16) {
                ClipData clipData = call_intent.getClipData();
                if (clipData!=null){
                    // lauch multipipe
                    if (clipData.getItemCount()>1){
                        pipeMultipleImages(call_intent);
                    } else {
                        // if clipdata contains only one uri, it is a simple pipe. If intent getData was null,
                        // get the uri from the clipData
                        if (i_uri==null){
                            ArrayList<Uri> source_uris = getMultipleUriArrayList(call_intent);
                            if (source_uris.size()==1){
                                i_uri = source_uris.get(0);
                            }
                        }
                    }
                }
            }
            if (i_uri != null) {
                image_display.setImageBitmap(null);
                clearCache();
                if (!readImageBitmapFromUri(i_uri,false)){
                    showLoadingFailedErrorToast();
                }
                gallery_already_called = false;
                applyChangesOnLoadedImage(i_uri,true,false, true,null);
                initOriginalBitmap();
            } else {
                // do nothing here; if no uri obtained, simply do nothing
            }
        } else {
            // this means the gallery did not return us an image
            gallery_already_called = false;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle save_bundle){
        super.onSaveInstanceState(save_bundle);
        Log.v("onsavedInstanceState");
        save_bundle.putFloat(SIS_CUT_LINE_TOP,cut_line_top);
        save_bundle.putFloat(SIS_CUT_LINE_BOTTOM,cut_line_bottom);
        save_bundle.putFloat(SIS_CUT_LINE_LEFT,cut_line_left);
        save_bundle.putFloat(SIS_CUT_LINE_RIGHT,cut_line_right);
        save_bundle.putFloat(SIS_SCALE_RATIO,scale_ratio);
        save_bundle.putBoolean(SIS_IMAGE_NEEDS_TO_BE_SAVED,image_needs_to_be_saved);
        if (savedImageContainer != null){
            if (savedImageContainer.uri!=null) {
                save_bundle.putParcelable(SIS_SAVEDIMAGE_URI,savedImageContainer.uri);
            }
        }
        if (originalImageContainer != null){
            save_bundle.putParcelable(SIS_ORIGINALIMAGE_URI,originalImageContainer.uri);
            save_bundle.putLong(SIS_ORIGINAL_FILESIZE,originalImageContainer.filesize);
        }
        save_bundle.putBoolean(SIS_NO_IMAGE_LOADED,no_image_loaded);
        save_bundle.putLong(SIS_JPEG_FILESIZE,jpeg_ecoded_filesize);
        save_bundle.putBoolean(SIS_PIPING_ALREADY_LAUNCHED,piping_was_already_launched);
        save_bundle.putInt(SIS_CACHE_POSITION,cachePosition);
        save_bundle.putInt(SIS_PAINTTOOL,paintTool);
        save_bundle.putInt(SIS_BRUSHSIZE,brushSize);
        save_bundle.putInt(SIS_SELECTEDCOLOR,selectedColor);
        save_bundle.putCharSequence(SIS_CHARSEQUENCE,charSequence);
        save_bundle.putBoolean(SIS_TEXTSTYLE_BOLD,textStyle_bold);
        save_bundle.putBoolean(SIS_TEXTSTYLE_ITALIC,textStyle_italic);
        save_bundle.putBoolean(SIS_TEXTSTYLE_UNDERLINE,textStyle_underline);
        save_bundle.putBoolean(SIS_TEXTSTYLE_STRIKETHROUGH,textStyle_strikethrough);
        save_bundle.putString(SIS_PREF_NUMBERING,ImagepipePreferences.getNumbering(context));
    }

    private void getSavedInstanceStateCutLine(Bundle bundle){
        if (bitmap_image!=null){
            cut_line_top=bundle.getFloat(SIS_CUT_LINE_TOP,0);
            cut_line_bottom=bundle.getFloat(SIS_CUT_LINE_BOTTOM, bitmap_image.getHeight());
            cut_line_left=bundle.getFloat(SIS_CUT_LINE_LEFT,0);
            cut_line_right=bundle.getFloat(SIS_CUT_LINE_RIGHT,bitmap_image.getWidth());
        }
        scale_ratio=bundle.getFloat(SIS_SCALE_RATIO,getScaleRatio());
    }

/*
    private void readPreferences(){
        pref = new ImagepipePreferences(this);
        pref.readPreferences();
        TextView scale_view = (TextView) findViewById(R.id.editText_size);
        scale_view.setText(pref.scalemax);
        if (seekBar==null){
            seekBar = (SeekBar) findViewById(R.id.seekBar_quality);
        }
        seekBar.setMax(ImagepipePreferences.getQualitymaxvalue(context));
        seekBar.setProgress(pref.quality);
        originalImageContainer = ImagepipePreferences.getOriginalImageContainer(context);
    }

 */
    private boolean isSizeEntryValid(String s){
        int scaleValue = 1024;
        boolean result = false;
        try {
            // try to parse the scale value from the text field
            scaleValue = Integer.parseInt(s);
            result = true;
        } catch (NumberFormatException e){
            // when parsing fails, do nothing; the fallback value is already assigned above
        } finally {
            // check for negative values
            if (scaleValue<1){
                scaleValue = Math.abs(scaleValue);
                result = false;
            }
            // check for zero
            if (scaleValue==0){
                scaleValue = 1024;
                result = false;
            }
        }
        return result;
    }

    private void setSpannableToTextView(TextView textView, String text, Object span){
        SpannableString spannableString = new SpannableString(text);
        spannableString.setSpan(span, 0, spannableString.length(),0);
        textView.setText(spannableString);
    }
    public void showIntroDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View aboutDialogView = getLayoutInflater().inflate(R.layout.aboutdialog,null);
        builder.setView(aboutDialogView);
        TextView translationsCredits = aboutDialogView.findViewById(R.id.text_intro_credits4);
        // read and insert translation credits
        InputStream inputStream = getResources().openRawResource(R.raw.translations);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line=getApplicationContext().getString(R.string.intro_credits4_text)+System.getProperty("line.separator");
        stringBuilder.append(line);
        try {
            while ((line=bufferedReader.readLine())!=null){
                stringBuilder.append(line);
                stringBuilder.append(System.getProperty("line.separator").toString());
            }
        } catch (IOException e){
            // do nothing
        }
        translationsCredits.setText(stringBuilder.toString());
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setCancelable(true);
        builder.setNeutralButton(R.string.intro_button_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        String versioning = BuildConfig.VERSION_NAME + " (build "+BuildConfig.VERSION_CODE+")";
        TextView heading = (TextView) aboutDialogView.findViewById(R.id.text_intro_headtext);
        heading.setText(getResources().getString(R.string.intro_headtext_text)+System.getProperty("line.separator")+"version "+versioning);
        setSpannableToTextView((TextView) aboutDialogView.findViewById(R.id.text_intro_credits1),getResources().getString(R.string.intro_credits1_text), new UnderlineSpan());
        setSpannableToTextView((TextView) aboutDialogView.findViewById(R.id.text_intro_license1),getResources().getString(R.string.intro_license1_text), new UnderlineSpan());
    }

    public void showExifDialog(){
        if (exifData!=null){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.exif_tags));
            builder.setCancelable(true);
            builder.setMessage(exifData.getListString());
            builder.setNeutralButton(R.string.permissions_ok_button, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    private static boolean cropToolVisible = false;
    private static boolean textToolVisible = false;
    private static boolean brushToolVisible = false;
    private static boolean paletteToolVisible = false;
    private static boolean hintBubbleVisible = false;
    private static boolean filterToolVisible = false;

    private void hideBrushSelector(){
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        RelativeLayout brush_selector = (RelativeLayout) findViewById(R.id.brushview_maincontainer);
        main_layout.removeView(brush_selector);
        brushToolVisible = false;
    }

    private void setPreviewBrushBitmapToColor(ImageView imageView){
        Bitmap bitmapIcon_immutable = BitmapFactory.decodeResource(getResources(),R.drawable.symbol_circle);
        Bitmap bitmapIcon = bitmapIcon_immutable.copy(Bitmap.Config.ARGB_8888,true);
        for (int y=0; y<bitmapIcon.getHeight(); y++){
            for (int x=0; x<bitmapIcon.getWidth(); x++){
                if (bitmapIcon.getPixel(x,y)!=Color.TRANSPARENT){
                    bitmapIcon.setPixel(x,y,selectedColor);
                }
            }
        }
        imageView.setImageBitmap(bitmapIcon);
    }

    private void previewBrushSize(final ImageView imageView){
        // get metrics of main image view
        ViewGroup.LayoutParams lp = imageView.getLayoutParams();
        float scale = getScaleRatio();
        lp.width = Math.round(brushSize/scale);
        lp.height =Math.round(brushSize/scale);
        imageView.setLayoutParams(lp);
    }

    private void hideCropSelector(){
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        RelativeLayout crop_selector = (RelativeLayout) findViewById(R.id.cropview_maincontainer);
        // close soft keyboard
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(main_layout.getWindowToken(),0);
        main_layout.removeView(crop_selector);
        cropToolVisible = false;
    }

    public void showCropSelector(){
        // close tool views if visible to avoid overlapping
        hideToolsIfVisible();
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        View cropView = getLayoutInflater().inflate(R.layout.view_crop,main_layout,true);
        final Button button = (Button) cropView.findViewById(R.id.cropview_button);
        final EditText editText = (EditText) cropView.findViewById(R.id.cropratio_text);
        Float aspectRatio = ImagepipePreferences.getAspectRatioFloat(context);
        if (aspectRatio!=null){
            editText.setText(ImagepipePreferences.getAspectRatioString(context));
        }
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String result = editText.getText().toString();
                ImagepipePreferences.applyAspectRatioString(context,result);
                hideCropSelector();
            }
        });
        cropToolVisible = true;
    }

    public void showBrushSelector(){
        // close tool views if visible to avoid overlapping
        hideToolsIfVisible();
        // add view
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        View selectorView = getLayoutInflater().inflate(R.layout.view_brushsize,main_layout,true);
        Button button = (Button) selectorView.findViewById(R.id.brushview_button);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hideBrushSelector();
            }
        });
        final ImageView brushPreview = (ImageView) selectorView.findViewById(R.id.brushview_sizepreview);
        setPreviewBrushBitmapToColor(brushPreview);
        previewBrushSize(brushPreview);
        SeekBar seekBar = (SeekBar) selectorView.findViewById(R.id.seekBar_brushsize);
        seekBar.setProgress(brushSize);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                brushSize = seekBar.getProgress();
                previewBrushSize(brushPreview);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // do nothing
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                hideBrushSelector();
            }
        });
        brushToolVisible = true;
    }

    public void showTextSelector(){
        // close tool views if visible to avoid overlapping
        hideToolsIfVisible();
        // add view
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        View selectorView = getLayoutInflater().inflate(R.layout.view_charsequence,main_layout,true);
        Button button = selectorView.findViewById(R.id.charsequenceview_button);
        EditText editText = selectorView.findViewById(R.id.charsequenceview_text);
        CheckBox checkBold = (CheckBox) findViewById(R.id.charsequenceview_ckeck_bold);
        CheckBox checkItalic = (CheckBox) findViewById(R.id.charsequenceview_ckeck_italic);
        CheckBox checkUnderline = (CheckBox) findViewById(R.id.charsequenceview_ckeck_underline);
        CheckBox checkStrikethrough = (CheckBox) findViewById(R.id.charsequenceview_ckeck_strikethrough);
        if (!charSequence.equals("")){
            editText.setText(charSequence);
        }
        checkBold.setChecked(textStyle_bold);
        checkItalic.setChecked(textStyle_italic);
        checkUnderline.setChecked(textStyle_underline);
        checkStrikethrough.setChecked(textStyle_strikethrough);
        checkBold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                textStyle_bold = b;
            }
        });
        checkItalic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                textStyle_italic = b;
            }
        });
        checkUnderline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                textStyle_underline = b;
            }
        });
        checkStrikethrough.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                textStyle_strikethrough = b;
            }
        });
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hideTextSelector();
            }
        });
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // do nothing
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // do nothing
            }
            @Override
            public void afterTextChanged(Editable editable) {
                charSequence = editable;
            }
        });
        textToolVisible = true;
    }

    private void hideTextSelector(){
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        RelativeLayout text_selector = (RelativeLayout) findViewById(R.id.charsequenceview_maincontainer);
        EditText editText = (EditText) findViewById(R.id.charsequenceview_text);
        if (editText!=null){
            charSequence = editText.getText();
        }
        // close soft keyboard
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (editText != null) {
            inputMethodManager.hideSoftInputFromWindow(main_layout.getWindowToken(),0);
        }
        main_layout.removeView(text_selector);
        textToolVisible = false;
    }


    public void showColorPicker(){
        // close tool views if visible to avoid overlapping
        hideToolsIfVisible();
        ColorPicker colorPicker = new ColorPicker(this,selectedColor, getResources().getString(R.string.pickColor),R.mipmap.ic_palette_white_24dp,ColorPicker.FLAG_NONE);
        colorPicker.setOnColorPickedListener(new ColorPicker.OnColorPickedListener() {
            @Override
            public void onColorSelected(int color) {
                setPaintColor(color);
            }

            @Override
            public void onNoColorSelected() {
                // do nothing
            }
        });
        colorPicker.show();
        paletteToolVisible = true;
    }

    public void showPalette(){
        // close tool views if visible to avoid overlapping
        hideToolsIfVisible();
        // add view
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        View paletteView = getLayoutInflater().inflate(R.layout.view_colorpicker,main_layout,true);
        for (int i=0; i<ColorPicker.COLORPALETTE_OLD.length; i++){
            int id = getResources().getIdentifier("brushview_button"+i,"id",this.getPackageName());
            ImageButton imageButton = (ImageButton) findViewById(id);
            imageButton.setOnClickListener(colorButtonListener);
        }
        paletteToolVisible = true;
    }

    public void hidePalette(){
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        if (main_layout!=null){
            RelativeLayout palette_view = (RelativeLayout) findViewById(R.id.colorpicker_maincontainer);
            main_layout.removeView(palette_view);
        }
        paletteToolVisible = false;
    }

    public void colorButtonPressed(View view){
        String name = getResources().getResourceName(view.getId());
        int charPos = name.indexOf("button");
        String numberString = name.substring(charPos+6);
        int position = Integer.parseInt(numberString);
        selectedColor = ColorPicker.COLORPALETTE_OLD[position];
        ImagepipePreferences.setLastColor(context,selectedColor);
        ImagepipePreferences.addColorHistory(context,selectedColor);
        buttonColor.setColorFilter(selectedColor,PorterDuff.Mode.SRC_ATOP);
        hidePalette();
    }

    public void hideFilterSelector(){
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        RelativeLayout filter_selector = (RelativeLayout) findViewById(R.id.filters_maincontainer);
        main_layout.removeView(filter_selector);
        filterToolVisible = false;
    }

    private void previewFilterInSelector(Bitmap source, ImageView imageView, int filter){
        if (source!=null){
            if ((filter==ImageFilters.FilerType.DITHER1) || (filter==ImageFilters.FilerType.DITHER2) || (filter==ImageFilters.FilerType.DITHER3)
                || (filter==ImageFilters.FilerType.DITHER4) || (filter==ImageFilters.FilerType.DITHER5) || (filter==ImageFilters.FilerType.HALFTONE)
                || (filter==ImageFilters.FilerType.RETRO1) || (filter==ImageFilters.FilerType.RETRO2) || (filter==ImageFilters.FilerType.RETRO3)){
                Bitmap previewBitmap = Bitmap.createScaledBitmap(source,source.getWidth()/4,source.getHeight()/4,true);
                previewBitmap = ImageFilters.applyColorFilter(null,previewBitmap,filter);
                imageView.setImageBitmap(previewBitmap);
            } else {
                Bitmap previewBitmap = ImageFilters.applyColorFilter(null,source,filter);
                imageView.setImageBitmap(previewBitmap);
            }
        }
    }

    public void showFilterSelector() {
        // close tool views if visible to avoid overlapping
        hideToolsIfVisible();
        // add view
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        View selectorView = getLayoutInflater().inflate(R.layout.view_filters, main_layout, true);
        RelativeLayout maincontainer = (RelativeLayout) selectorView.findViewById(R.id.filters_maincontainer);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,(int) Math.round(image_display.getHeight()));
        maincontainer.setLayoutParams(lp1);
        int target_width = displayMetrics.widthPixels/3;
        Bitmap thumbnail = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
        int min_diameter = thumbnail.getWidth();
        if (thumbnail.getHeight()<min_diameter){
            min_diameter = thumbnail.getHeight();
        }
        int x_center = thumbnail.getWidth()/2;
        int y_center = thumbnail.getHeight()/2;
        thumbnail = Bitmap.createBitmap(thumbnail,x_center-min_diameter/2+1,y_center-min_diameter/2+1,min_diameter-1,min_diameter-1);
        if (thumbnail.getWidth()>target_width){
            thumbnail = Bitmap.createScaledBitmap(thumbnail,target_width,target_width,ImagepipePreferences.isBilinearFilteringEnabled(context));
        }
        RelativeLayout filters_rl00 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl00);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image00), ImageFilters.FilerType.NONE);
        filters_rl00.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.NONE);
                hideFilterSelector();
            }
        });
        RelativeLayout filters_rl01 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl01);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image01), ImageFilters.FilerType.GREYSCALE);
        filters_rl01.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.GREYSCALE);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl02 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl02);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image02), ImageFilters.FilerType.SEPIA);
        filters_rl02.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.SEPIA);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl03 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl03);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image03), ImageFilters.FilerType.SEPIALIGHT);
        filters_rl03.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.SEPIALIGHT);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl04 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl04);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image04), ImageFilters.FilerType.SEPIA1960);
        filters_rl04.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.SEPIA1960);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl05 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl05);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image05), ImageFilters.FilerType.SEPIACOLD);
        filters_rl05.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.SEPIACOLD);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl06 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl06);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image06), ImageFilters.FilerType.INVERT);
        filters_rl06.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.INVERT);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl07 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl07);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image07), ImageFilters.FilerType.MYSTIFY);
        filters_rl07.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.MYSTIFY);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl08 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl08);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image08), ImageFilters.FilerType.DAWN);
        filters_rl08.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.DAWN);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl09 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl09);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image09), ImageFilters.FilerType.DITHER1);
        filters_rl09.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.DITHER1);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl10 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl10);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image10), ImageFilters.FilerType.DITHER2);
        filters_rl10.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.DITHER2);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl11 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl11);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image11), ImageFilters.FilerType.DITHER3);
        filters_rl11.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.DITHER3);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl12 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl12);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image12), ImageFilters.FilerType.DITHER4);
        filters_rl12.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.DITHER4);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl13 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl13);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image13), ImageFilters.FilerType.DITHER5);
        filters_rl13.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.DITHER5);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl14 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl14);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image14), ImageFilters.FilerType.HALFTONE);
        filters_rl14.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.HALFTONE);
                hideFilterSelector();
                filterImage();
            }
        });

        RelativeLayout filters_rl15 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl15);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image15), ImageFilters.FilerType.RETRO1);
        filters_rl15.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.RETRO1);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl16 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl16);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image16), ImageFilters.FilerType.RETRO2);
        filters_rl16.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.RETRO2);
                hideFilterSelector();
                filterImage();
            }
        });
        RelativeLayout filters_rl17 = (RelativeLayout) selectorView.findViewById(R.id.filters_rl17);
        previewFilterInSelector(thumbnail,(ImageView) selectorView.findViewById(R.id.filters_image17), ImageFilters.FilerType.RETRO3);
        filters_rl17.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagepipePreferences.setFilter(context,ImageFilters.FilerType.RETRO3);
                hideFilterSelector();
                filterImage();
            }
        });

        filterToolVisible = true;
    }

        private String getHintText(){
        if (ImagepipePreferences.getBubbleCounter(context)==0){
            return getResources().getString(R.string.hint1_line1) + System.getProperty("line.separator") + System.getProperty("line.separator") + getResources().getString(R.string.hint1);
        } else {
            if (ImagepipePreferences.getBubbleCounter(context)==1){
                return getResources().getString(R.string.hint2);
            } else
                return getResources().getString(R.string.hint3);
        }
    }

    public void showHintBubble(){
        // close tool views if visible to avoid overlapping
        hideToolsIfVisible();
        // add view
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        View paletteView = getLayoutInflater().inflate(R.layout.view_hint,main_layout,true);
        TextView text_hint = (TextView) findViewById(R.id.hintview_text);
        text_hint.setText(getHintText());
        Button button_hint = (Button) findViewById(R.id.hintview_button);
        button_hint.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hideHintBubble();
            }
        });
        CheckBox checkBox = (CheckBox) findViewById(R.id.hintview_checkbox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ImagepipePreferences.setShouldShowHints(context,!b);
            }
        });
        hintBubbleVisible = true;
        ImagepipePreferences.incrementBubbleCounter(context);
        if (ImagepipePreferences.getBubbleCounter(context)>HINT_COUNT){
            ImagepipePreferences.setBubbleCounter(context,0);
        }
    }

    public void hideHintBubble(){
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        RelativeLayout hint_view = (RelativeLayout) findViewById(R.id.hintview_maincontainer);
        main_layout.removeView(hint_view);
        hintBubbleVisible = false;
    }

    public void displayHintBubble(){
        Handler bubble_handler = new Handler();
        bubble_handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showHintBubble();
            }
        }, BUBBLE_RUN_DELAY);
    }

    private void checkForHintBubbleDisplay(){
        if ((no_image_loaded) && (ImagepipePreferences.shouldShowHints(context))) {
            displayHintBubble();
        }
    }

    public void hideToolsIfVisible(){
        if (cropToolVisible){
            hideCropSelector();
        }
        if (textToolVisible){
            hideTextSelector();
        }
        if (brushToolVisible){
            hideBrushSelector();
        }
        if (paletteToolVisible){
            hidePalette();
        }
        if (hintBubbleVisible){
            hideHintBubble();
        }
        if (filterToolVisible){
            hideFilterSelector();
        }
    }

    public boolean readImageBitmapFromCache(int position){
        Bitmap bitmap = PictureCache.restoreFromPictureCache(context,position);
        if (bitmap!=null){
            bitmap_image = bitmap.copy(Bitmap.Config.ARGB_8888,true);
            bitmap_visible = bitmap.copy(Bitmap.Config.ARGB_8888,true);
            updateImageDisplay();
            if (bitmap_original==null){
                bitmap_original = PictureCache.restoreFromPictureCache(context,0);
                // if no exif data is present, restore it from a copy in the preferences
                if (exifData==null){
                    exifData = ImagepipePreferences.readExifData(context);
                }
            }
            ImagepipePreferences.resetSavedImageContainer(context);
            return true;
        }
        return false;
    }

    public void readLastImageFromCacheAndRestoreCache(){
        // empty cache returns cachePosition = 0,
        // all bitmaps may be null if cache is empty
        cachePosition = PictureCache.getLastUsedCachePosition(context);
        bitmap_original = PictureCache.restoreFromPictureCache(context,0);
        // restore exif data from preferences
        exifData = ImagepipePreferences.readExifData(context);
        bitmap_image = PictureCache.restoreFromPictureCache(context,cachePosition);
        savedImageContainer = ImagepipePreferences.getSavedImageContainer(context);
        bitmap_visible = bitmap_image;
        if ((bitmap_image==null) && (bitmap_original!=null)){
            bitmap_image = bitmap_original.copy(Bitmap.Config.ARGB_8888,true);
            bitmap_visible = bitmap_original.copy(Bitmap.Config.ARGB_8888,true);
        }
        if (bitmap_image==null){
            no_image_loaded = true;
            savedImageContainer = null;
        } else {
            no_image_loaded = false;
            image_needs_to_be_saved = true;
        }
    }

    public void putImageBitmapToCache(final Runnable finishedRunnable, boolean applyQualityLoss){
        if (bitmap_image!=null){
            Runnable runnable = finishedRunnable;
            // this only applies to lossless file format.
            // after putting to cache, the target file size is calculated taking the cache image.
            // for .jpeg & .webp this value is calculated by the previews.
            // The stuff needs to be in a runnable because putting images to cache is done async!
            if (ImagepipePreferences.getImageFileFormat(context).equals(ImagepipePreferences.ImageFileFormat.PNG)){
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                jpeg_ecoded_filesize = PictureCache.getCachedFileSize(context,cachePosition);
                                updateImagePropertiesText();
                                // this is an optional runnable that runs after image was saved to cache and
                                // the display updated.
                                if (finishedRunnable!=null){
                                    finishedRunnable.run();
                                }
                            }
                        });
                    }
                };
            }
            PictureCache.clearPictureCache(context,cachePosition+1);
            cachePosition = PictureCache.saveToPictureCache(context,bitmap_image,applyQualityLoss, runnable);
            hideNewCanvasButton();
        }
    }

    public void clearCache(){
        Log.v("clearing picture cache");
        PictureCache.clearPictureCache(context);
        ImagepipePreferences.resetOriginalImageContainer(context);
        cachePosition = 0;
    }

    public void unDoLastAction(){
        if (cachePosition>0){
            cachePosition--;
            Bitmap bitmap = PictureCache.restoreFromPictureCache(context,cachePosition);
            PictureCache.touchPicture(context,cachePosition);
            bitmap_image = bitmap.copy(Bitmap.Config.ARGB_8888,true);
            bitmap_visible = bitmap.copy(Bitmap.Config.ARGB_8888,true);
            image_display.setImageBitmap(bitmap_visible);
            resetCutFrame();
            setLastModifiedTimestamp();
            updateImageDisplay();
        }
    }

    public void reDoAction(){
        if (cachePosition<PictureCache.getPictureCacheSize(context)-1){
            cachePosition++;
            Bitmap bitmap = PictureCache.restoreFromPictureCache(context,cachePosition);
            PictureCache.touchPicture(context,cachePosition);
            bitmap_image = bitmap.copy(Bitmap.Config.ARGB_8888,true);
            bitmap_visible = bitmap.copy(Bitmap.Config.ARGB_8888,true);
            image_display.setImageBitmap(bitmap_visible);
            resetCutFrame();
            setLastModifiedTimestamp();
            updateImageDisplay();
        }
    }

    public void createNewCanvas(boolean useImageViewSize) {
        savedImageContainer = null;
        ImagepipePreferences.resetSavedImageContainer(context);
        originalImageContainer = null;
        image_needs_to_be_saved = true;
        no_image_loaded = false;
        ImagepipePreferences.resetSavedImageContainer(context);
        Bitmap bitmap;
        int newWidth = image_display.getWidth();
        int newHeight = image_display.getHeight();
        if (!useImageViewSize){
            int size = getResizeTextFieldValue();
            // force image size to maximum screen resolution if applicable
            if ((ImagepipePreferences.forceDownscale(context)) && (size>getForceImagesize())){
                size = getForceImagesize();
            }
            newHeight = size; newWidth = size;
        }
        try {
            bitmap = Bitmap.createBitmap(newWidth,newHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError e){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context,getResources().getString(R.string.memory_error),Toast.LENGTH_LONG).show();
                }
            });
            return;
        }
        bitmap.eraseColor(selectedColor);
        bitmap_image = bitmap.copy(Bitmap.Config.ARGB_8888,true);
        bitmap_original = bitmap.copy(Bitmap.Config.ARGB_8888,false);
        bitmap_visible = bitmap.copy(Bitmap.Config.ARGB_8888,true);
        image_display.setImageBitmap(bitmap_visible);
        clearCache();
        putImageBitmapToCache(null,false);
        if (newCanvas!=null){
            newCanvas.setVisibility(View.INVISIBLE);
        }
        updateImageDisplay();
    }

    public void showNewCanvasButton(){
        if (newCanvas==null){
            newCanvas = (ImageButton) findViewById(R.id.new_canvas);
        }
        if (newCanvas!=null){
            newCanvas.setVisibility(View.VISIBLE);
            newCanvas.setOnClickListener(newCanvasListener);
            newCanvas.setOnLongClickListener(newCanvasLongListener);
        }
    }

    public void hideNewCanvasButton(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (newCanvas==null){
                    newCanvas = (ImageButton) findViewById(R.id.new_canvas);
                }
                if (newCanvas!=null){
                    if (newCanvas.getVisibility() != View.INVISIBLE){
                        newCanvas.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
    }

    final static int FLIP_HORIZONTALLY=0;
    final static int FLIP_VERTICALLY=1;

    public boolean flipImage(int direction){
        if (!no_image_loaded){
            Matrix matrix = new Matrix();
            if (direction==FLIP_HORIZONTALLY){
                matrix.preScale(-1,1);
                float i = cut_line_right; cut_line_right = bitmap_image.getWidth() - cut_line_left; cut_line_left = bitmap_image.getWidth() - i;
            } else {
                float i = cut_line_top; cut_line_top = bitmap_image.getHeight() - cut_line_bottom; cut_line_bottom = bitmap_image.getHeight() - i;
                matrix.preScale(1,-1);
            }
            try {
                // perform operation on a *new* bitmap to avoid nullifying bitmap_image when out of memory
                Bitmap bitmapTemp = Bitmap.createBitmap(bitmap_image,0,0,bitmap_image.getWidth(),bitmap_image.getHeight(),matrix,true);
                if (bitmapTemp!=null){
                    bitmap_image = bitmapTemp.copy(Bitmap.Config.ARGB_8888,true);
                }
                putImageBitmapToCache(null,false);
                updateImageDisplay();
                updateImagePropertiesText();
            } catch (OutOfMemoryError e){
                return false;
            }
        }
        return true;
    }

    private void updateImageView(final ImageView imageView, final Bitmap bitmap, int progress){
        int targetDegrees = 180 - progress;
        Matrix matrix = new Matrix();
        matrix.postRotate(targetDegrees);
        Bitmap previewBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);
        Canvas canvas = new Canvas(previewBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.YELLOW);
        paint.setStyle(Paint.Style.STROKE);
        imageView.setImageBitmap(previewBitmap);
    }

    public int rotateImageDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View rotateImageDialogView = getLayoutInflater().inflate(R.layout.rotatedialog,null);
        Display display = getWindowManager().getDefaultDisplay();
        int rotateImagePreviewMaxSize = display.getWidth();
        if (display.getHeight()<display.getWidth()){
            rotateImagePreviewMaxSize = display.getHeight();
        }
        rotateImagePreviewMaxSize = rotateImagePreviewMaxSize / 2;
        // default: height > width
        int previewHeight  = rotateImagePreviewMaxSize;
        int previewWidth   = (bitmap_image.getWidth()*rotateImagePreviewMaxSize)/bitmap_image.getHeight();
        if (bitmap_image.getWidth() > bitmap_image.getHeight()){
            previewWidth   = rotateImagePreviewMaxSize;
            previewHeight  = (bitmap_image.getHeight()*rotateImagePreviewMaxSize)/bitmap_image.getWidth();
        }
        final Bitmap rotationStartBitmap = Bitmap.createScaledBitmap(bitmap_image,previewWidth,previewHeight,true);
        builder.setView(rotateImageDialogView);
        builder.setTitle(context.getResources().getString(R.string.rotate));
        builder.setIcon(R.mipmap.ic_rotate_left_white_24dp);
        final SeekBar roationSeekBar = (SeekBar) rotateImageDialogView.findViewById(R.id.rotate_seekbar);
        builder.setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (rotateImage(180 - roationSeekBar.getProgress(),ImagepipePreferences.cropAfterRotation(context))){
                    putImageBitmapToCache(null,false);
                }
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        final ImageView previewImageView = (ImageView) rotateImageDialogView.findViewById(R.id.rotate_preview);
        final TextView degreesTextView = (TextView) rotateImageDialogView.findViewById(R.id.rotate_text_degrees);
        final CheckBox cropCheckBox = (CheckBox) rotateImageDialogView.findViewById(R.id.rotate_crop_checkbox);
        cropCheckBox.setChecked(ImagepipePreferences.cropAfterRotation(context));
        cropCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean crop) {
                ImagepipePreferences.setCropAfterRotation(context,crop);
            }
        });
        roationSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                degreesTextView.setText(Integer.toString(180-progress)+"°");
                updateImageView(previewImageView,rotationStartBitmap,progress);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        builder.show();
        previewImageView.getLayoutParams().width = rotateImagePreviewMaxSize;
        previewImageView.getLayoutParams().height = rotateImagePreviewMaxSize;
        // in landscape mode, make the view smaller
        if (display.getHeight()<display.getWidth()){
            previewImageView.getLayoutParams().width = rotateImagePreviewMaxSize/2;
            previewImageView.getLayoutParams().height = rotateImagePreviewMaxSize/2;
        }
        previewImageView.requestLayout();
        updateImageView(previewImageView,rotationStartBitmap,roationSeekBar.getProgress());
        return 0;

    }

    public void filterImage() {
        if (!no_image_loaded) {
                if ((ImagepipePreferences.getFilter(context)==ImageFilters.FilerType.DITHER1) ||
                        (ImagepipePreferences.getFilter(context)==ImageFilters.FilerType.DITHER2) ||
                        (ImagepipePreferences.getFilter(context)==ImageFilters.FilerType.DITHER3) ||
                        (ImagepipePreferences.getFilter(context)==ImageFilters.FilerType.DITHER4) ||
                        (ImagepipePreferences.getFilter(context)==ImageFilters.FilerType.DITHER5) ||
                        (ImagepipePreferences.getFilter(context)==ImageFilters.FilerType.RETRO1) ||
                        (ImagepipePreferences.getFilter(context)==ImageFilters.FilerType.RETRO2) ||
                        (ImagepipePreferences.getFilter(context)==ImageFilters.FilerType.RETRO3) ||
                        (ImagepipePreferences.getFilter(context)==ImageFilters.FilerType.HALFTONE)){
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setMax(100);
                    progressBar.setProgress(0);
                }
                ExecutorService executor = Executors.newSingleThreadExecutor();
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Bitmap bitmapTemp = ImageFilters.applyColorFilter(context,bitmap_image,ImagepipePreferences.getFilter(context));
                            // update only if a known filter was applied
                            if (bitmapTemp!=null){
                                bitmap_image = bitmapTemp.copy(Bitmap.Config.ARGB_8888,true);
                                bitmapTemp=null; // free memory
                                putImageBitmapToCache(null, false);
                                updateImageDisplay();
                                updateImagePropertiesText();
                            }
                    } catch (Exception e) {
                        // do nothing, probably out of memory.
                    } finally {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            });
                        }
                }});
        }
    }

    private void showExifWarning(final Bundle savedInstanceState,
                                 final String neutralLabel,
                                 final DialogInterface.OnClickListener neutralOnClickListener,
                                 final DialogInterface.OnClickListener positiveOnClickListener,
                                 final DialogInterface.OnClickListener negativeOnClickListener){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View warningView = getLayoutInflater().inflate(R.layout.exifwarning,null);
        builder.setView(warningView);
        builder.setIcon(R.mipmap.ic_info_outline_white_24dp);
        builder.setTitle(R.string.keepingtags);
        TextView exifElementsList = (TextView) warningView.findViewById(R.id.exifwarning_list);
        CheckBox exifWarningDisableCheckbox = (CheckBox) warningView.findViewById(R.id.exifwarning_checkbox);
        exifWarningDisableCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ImagepipePreferences.setTemporarilyDisableWarning(context,b);
            }
        });
        int displayHeight = getWindowManager().getDefaultDisplay().getHeight();
        ScrollView scrollView = (ScrollView) warningView.findViewById(R.id.exifwarning_scrollview);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,(int) Math.round(displayHeight/4.5));
        scrollView.setLayoutParams(llp);
        ExifData exifData = new ExifData(context,null);
        exifElementsList.setText(exifData.getAllowedListString());
        builder.setPositiveButton(getResources().getString(R.string.permissions_ok_button), positiveOnClickListener);
        builder.setNeutralButton(neutralLabel, neutralOnClickListener);
        builder.setNegativeButton(getResources().getString(R.string.reset), negativeOnClickListener);
        builder.show();
    }

    private void clearImageData(boolean updateViews){
        // clear image cache and last information about image in preferences
        clearCache();
        // clear cache from image rotation
        deleteCacheFile(context);
        no_image_loaded = true;
        bitmap_image = null;
        bitmap_visible = null;
        bitmap_original = null;
        savedImageContainer = null;
        if (updateViews){
            updateImageDisplay();
            updateImagePropertiesText();
        }
        ImagepipePreferences.resetExifData(context);
        exifData = null;
    }

    public static String getCurrentTimestamp(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss:SSS");
        long time = Calendar.getInstance().getTimeInMillis();
        String result = simpleDateFormat.format(new Date(time));
        return result;
    }

}
