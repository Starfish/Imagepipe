/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.Context;
import android.graphics.*;

public class Trim {

    /*
     Touch logic of trimming:
     touch on frame or outside frame resizes the frame:
        - swipe away        = enlarge
        - swipe to middle   = shrink
     touch inside the frame:
        - moves it while pointer down
     */

    private final static float CornerPadding = 10f;
    private final static float CornerLength  = 50f;

    public Trim(){
    }

    public static void drawCutFrame(final Context context, final Bitmap bitmap, float x1, float y1, float x2, float y2, float scale_ratio){
        float strokeWidthBold   = scale_ratio*5;
        float strokeWidthMedium = scale_ratio*4f;
        float strokeWidthThin   = scale_ratio*3f;
        final Paint paintBold = new Paint();
        final boolean antiAlias = true;
        paintBold.setColor(ImageReceiver.GetAppColor(context,R.color.secondaryLightColor));
        paintBold.setAntiAlias(antiAlias);
        paintBold.setStrokeWidth(strokeWidthBold);
        paintBold.setStyle(Paint.Style.STROKE);
        paintBold.setAlpha(255);
        final Paint paintMedium = new Paint();
        paintMedium.setColor(ImageReceiver.GetAppColor(context,R.color.secondaryLightColor));
        paintMedium.setAntiAlias(antiAlias);
        paintMedium.setStrokeWidth(strokeWidthMedium);
        paintMedium.setStyle(Paint.Style.STROKE);
        paintMedium.setAlpha(255);
        final Paint paintThin = new Paint();
        paintThin.setColor(ImageReceiver.GetAppColor(context,R.color.secondaryColor));
        paintThin.setPathEffect(new DashPathEffect(new float[]{3,3},0));
        paintThin.setAntiAlias(antiAlias);
        paintThin.setStrokeWidth(strokeWidthThin);
        paintThin.setStyle(Paint.Style.STROKE);
        paintThin.setAlpha(255);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawRect(x1+strokeWidthBold/2f,y1+strokeWidthBold/2f,x2-strokeWidthBold/2f,y2-strokeWidthBold/2f,paintBold);
        if (( (x2-x1) > (CornerPadding*2+CornerLength*2) ) && ( (y2-y1) > (CornerPadding*2+CornerLength*2) ) ){
            // left-top corner
            canvas.drawLine(x1+CornerPadding,y1+CornerPadding, x1+CornerPadding+CornerLength, y1+CornerPadding,paintMedium);
            canvas.drawLine(x1+CornerPadding,y1+CornerPadding,x1+CornerPadding,y1+CornerPadding+CornerLength,paintMedium);
            // right-top corner
            canvas.drawLine(x2-CornerPadding,y1+CornerPadding, x2-CornerPadding-CornerLength, y1+CornerPadding,paintMedium);
            canvas.drawLine(x2-CornerPadding,y1+CornerPadding,x2-CornerPadding,y1+CornerPadding+CornerLength,paintMedium);
            // left-bottom corner
            canvas.drawLine(x1+CornerPadding,y2-CornerPadding, x1+CornerPadding+CornerLength, y2-CornerPadding,paintMedium);
            canvas.drawLine(x1+CornerPadding,y2-CornerPadding,x1+CornerPadding,y2-CornerPadding-CornerLength,paintMedium);
            // right-bottom corner
            canvas.drawLine(x2-CornerPadding,y2-CornerPadding, x2-CornerPadding-CornerLength, y2-CornerPadding,paintMedium);
            canvas.drawLine(x2-CornerPadding,y2-CornerPadding,x2-CornerPadding,y2-CornerPadding-CornerLength,paintMedium);
        }
        if (((y2-y1) > CornerLength*3 + CornerPadding*2) && (x2-x1>CornerPadding*3)){
            float middleY = y1 + (y2-y1)/2;
            canvas.drawLine(x1+CornerPadding,middleY-CornerLength/2,x1+CornerPadding,middleY+CornerLength/2,paintMedium);
            canvas.drawLine(x2-CornerPadding,middleY-CornerLength/2,x2-CornerPadding,middleY+CornerLength/2,paintMedium);
        }
        if (((x2-x1) > CornerLength*3 + CornerPadding*2) && (y2-y1>CornerPadding*3)){
            float middleX = x1 + (x2-x1)/2;
            canvas.drawLine(middleX-CornerLength/2,y1+CornerPadding,middleX+CornerLength/2,y1+CornerPadding,paintMedium);
            canvas.drawLine(middleX-CornerLength/2,y2-CornerPadding,middleX+CornerLength/2,y2-CornerPadding,paintMedium);
        }
        // 9 tiles
        float tileWidthX = (x2-x1)/3f;
        canvas.drawLine(x1+tileWidthX,y1,x1+tileWidthX,y2,paintThin);
        canvas.drawLine(x1+tileWidthX*2,y1,x1+tileWidthX*2,y2,paintThin);
        float tileWidthY = (y2-y1)/3f;
        canvas.drawLine(x1,y1+tileWidthY,x2,y1+tileWidthY,paintThin);
        canvas.drawLine(x1,y1+tileWidthY*2,x2,y1+tileWidthY*2,paintThin);
    }

    public static void applyAlpha(final Bitmap bitmap, float cut_line_left, float cut_line_top, float cut_line_right, float cut_line_bottom){
        // alpha out areas around the target
        final Paint fadePaint = new Paint();
        fadePaint.setAlpha(125);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawRect(0,0,bitmap.getWidth(),cut_line_top,fadePaint);
        canvas.drawRect(0,cut_line_bottom,bitmap.getWidth(),bitmap.getHeight(),fadePaint);
        canvas.drawRect(0,cut_line_top,cut_line_left,cut_line_bottom,fadePaint);
        canvas.drawRect(cut_line_right,cut_line_top,bitmap.getWidth(),cut_line_bottom,fadePaint);
    }


}
