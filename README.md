Imagepipe
=========

This app reduces image size by changing the resolution and quality of the image. It also removes exif data before sending the image. The modified image is saved in a separate folder in jpeg, png or webp format. The original image remains unchanged.

Imagepipe will receive a send intent for images, modify the image and send the changed image onward. Therefore, it takes you only one touch to pipe the image before sending!

Imagepipe also supports sharing and processing multiple pictures at once. 
 
The image can also be edited with Imagepipe.

The app is light wight (small apk) and also supports legacy devices with low screen resolutions down to QVGA (320 x 240 pixels). 

If you like to learn what Imagepipe can do, [visit the wiki](https://codeberg.org/Starfish/Imagepipe/wiki).

Screenshots
-----------

![Screenshot #1](fastlane/metadata/android/en-US/images/phoneScreenshots/1.png)
![Screenshot #2](fastlane/metadata/android/en-US/images/phoneScreenshots/2.png)
![Screenshot #3](fastlane/metadata/android/en-US/images/phoneScreenshots/3.png)

How to get the app
==================

From the main F-Droid repository (recommended)
----------------------------------------------

Imagepipe is available on the F-Droid main repository.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
alt="Get it on F-Droid"
height="80">](https://f-droid.org/packages/de.kaffeemitkoffein.imagepipe)

From the author's custom F-Droid repository
--------------------------------------------

Alternatively, the app is also available in the author's custom repository.

You need to add the following repo to your f-droid app:

Repo: [https://kaffeemitkoffein.de/fdroid/repo](https://kaffeemitkoffein.de/fdroid/repo)

Fingerprint: 2F 27 5C B8 37 35 FE 79 75 44 9C E8 00 CA 84 07 B5 01 9D 5F 7B 6A BC B3 0F 36 51 C5 20 A3 72 61

### How to add this repo:

<img style="height: 10em;" src="https://www.kaffeemitkoffein.de/fdroid/repo/index.png" alt="Get it on F-Droid">

1. Launch the f-droid app
2. Select "Settings"
3. Select "Repositories" from the list
4. Select "+NEW REPOSITORY"
5. Scan the code above or add the following repository address: "https://kaffeemitkoffein.de/fdroid/repo"
6. Optional: enter the fingerprint of this repo (see above)
7. Select "Add"

### Now, you can install this app via f-droid as soon as the app updated the repo data:

1. In the main view, update the f-droid repos by swiping down
2. Look for the Imagepipe app in f-droid and install it

Direct download of the binary (APK)
-----------------------------------

You can also directly download the apk file via this link:

<https://kaffeemitkoffein.de/fdroid/repo/Imagepipe.apk>

### APKs for testing and older versions

New binaries for testing and older versions can be also downloaded directly from the developer:

<https://kaffeemitkoffein.de/nextcloud/index.php/s/4gXTjNW4zExBKoK>

License
=======

 Imagepipe

 Copyright (c) 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 Pawel Dube

 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 Imagepipe is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.

Credits
=======

 The Material Design icons are Copyright (c) Google Inc., licensed 
 under the Apache License Version 2.0.
 
 This app uses gradle and the gradle wrapper, Copyright (c) Gradle Inc.,
 licensed under the Apache 2.0 license.

Translations:
-------------

* Arabic: alhassanaraouf, Allan Nordhøy, butterflyoffire, gnuhead-chieb, Saldef, SomeTr
* Bengali: Allan Nordhøy, gnuhead-chieb, MonsoonFire, Orwell Not
* Catalan: Allan Nordhøy, gnuhead-chieb, Ícar N.S., juxuanu, mondstern, Sheila
* Chinese: Allan Nordhøy, gnuhead-chieb, hamburger1024, ivellilukewarm, melanotosjaunty, Outbreak2096, sr093906, yzqzss
* Czech: Fjuro
* Dutch: Allan Nordhøy, eUgEntOptIc44, gnuhead-chieb, Jean-Luc Tibaux, mondstern, yarmo
* English: Allan Nordhøy, gnuhead-chieb
* Estonian: Priit Jõerüüt
* French: Allan Nordhøy, eUgEntOptIc44, gnuhead-chieb, Jean-Luc Tibaux, J. Lavoie, lejun, mondstern, twann
* German: Allan Nordhøy, Benny, eUgEntOptIc44, fnetX, gnuhead-chieb, Hartmut Goebel, Jean-Luc Tibaux, jfa, mondstern, nautilusx, ncc1988, fossdd, J. Lavoie, pixelcode, surrim
* Indonesian: Allan Nordhøy, gnuhead-chieb, liimee, Matthew Justch, mondstern
* Italian: Allan Nordhøy, eUgEntOptIc44, Giovanni, mondstern, silkevicious, verhyppo
* Japanese: Allan Nordhøy, gnuhead-chieb
* Norwegian Bokmål: Allan Nordhøy, gnuhead-chieb, mondstern
* Odia: SubhamJena
* Portuguese: Allan Nordhøy, Barrette4138, Cavernosa, Cloudstar, gnuhead-chieb, inkhorn, mezysinc, mondstern
* Punjabi (Pakistan): bgo-eiu
* Romanian: 2Baki
* Russian: 0ko, 0que, Allan Nordhøy, arslee07, Barrette4138, Степан, gnuhead-chieb, mondstern, Rikishi, thejenja, Timur
* Sinhala: Allan Nordhøy, gnuhead-chieb, Hatsune Miku
* Swedish: tygyh
* Turkish: abfreeman, Allan Nordhøy, aral, erdem, metezd
* Ukrainian: Allan Nordhøy, Andrij Mizyk, gnuhead-chieb, maxch, mondstern, SomeTr, winnie_ua
* Vietnamese: Allan Nordhøy, bruh, gnuhead-chieb, Mingu113, mondstern, Quang Trung

Privacy
=======

For the privacy statement, see [here](https://codeberg.org/Starfish/Imagepipe/wiki/Privacy-Policy).
 
Contributing
============

Translations
------------

You are welcome to [help translate Imagepipe to other languages](https://translate.codeberg.org/engage/imagepipe/):

[<img src="https://translate.codeberg.org/widgets/imagepipe/-/multi-blue.svg">](https://translate.codeberg.org/engage/imagepipe/)
 
Issues & Suggesions
--------------------

 Please use, whenever possible, the issue tracker at codeberg.org for reporting issues and/or suggestions:

 <https://codeberg.org/Starfish/Imagepipe/issues>

 For suggestions and bug reports, please contact the author:
 bugreport (at) imagepipe.de


